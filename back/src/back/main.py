from back.app.app import app, celery_app

# see utils/gunicorn_config.py
if __name__ == "__main__":
    app.run(debug=True, port=8080)  # host="0.0.0.0",


# Run using
# python main.py
# or
# gunicorn -c utils/gunicorn_config.py app:app
# This allows having more than 1 thread

# Start celery with
# celery -A main.celery_app worker -P threads -l info

# To init database run
# python init_db.py
