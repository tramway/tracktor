from datetime import datetime
from back.app.app import db, sm
import sqlalchemy as sa

job_files = db.Table('job_file_link',
                     db.Column('job_id', db.Integer, db.ForeignKey(
                         'job.id'), primary_key=True),
                     db.Column('file_id', db.Integer, db.ForeignKey(
                         'file.id'), primary_key=True)
                     )


class Job(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    create_time = db.Column(db.DateTime, nullable=False,
                            default=datetime.utcnow)
    submit_time = db.Column(db.DateTime, nullable=True)
    start_time = db.Column(db.DateTime, nullable=True)
    end_time = db.Column(db.DateTime, nullable=True)
    author = db.Column(db.String(255), nullable=True)
    name = db.Column(db.String(255), nullable=True)
    description = db.Column(db.Text, nullable=True)
    public = db.Column(db.Boolean, nullable=False, default=False)
    is3D = db.Column(db.Boolean, nullable=False, default=False)
    secret_key = db.Column(db.String(255), nullable=False)

    JUST_CREATED = -1
    QUEUED = 0
    PROCESSING = 1
    SUCCESS = 2
    FAILED = 3
    ABORTED = 4
    STATUSES = {JUST_CREATED: "Just created",
                QUEUED: "In the queue",
                PROCESSING: "Processing",
                SUCCESS: "Terminated with success",
                FAILED: "Failed",
                ABORTED: "Aborted"}

    status = db.Column(db.Integer, default=JUST_CREATED)

    session_id = db.Column(db.String(255), db.ForeignKey(
        'session_entry.session_id', ondelete="CASCADE"), nullable=False)
    session_rel = db.relationship(sm,
                                  backref=db.backref('jobs', lazy=True, cascade="delete"))

    files = db.relationship('File', secondary=job_files, lazy='subquery',
                            backref=db.backref('jobs', lazy=True))

    def __repr__(self):
        return "<Job %r, %s>" % (self.id, self.STATUSES[self.status])


class File(db.Model):
    """
    Files are uploaded in the /data/sessions/xxx/ folder
    """
    id = db.Column(db.Integer, primary_key=True)

    session_id = db.Column(db.String(255), db.ForeignKey(
        'session_entry.session_id', ondelete="CASCADE"), nullable=False)
    session_rel = db.relationship(sm,
                                  backref=db.backref('files', lazy=True, cascade="delete"))

    upload_time = db.Column(db.DateTime, nullable=False,
                            default=datetime.utcnow)
    size = db.Column(db.Integer, nullable=True)
    is3D = db.Column(db.Boolean, nullable=False, default=False)
    path = db.Column(db.String(255), nullable=False, unique=True)

    # This is the hask key of the file upon upload.
    hash_key = db.Column(db.String(255), nullable=False)

    # This can be edited by the user in the main index table
    name = db.Column(db.String(255), nullable=False)

    #scale = db.Column(db.Float, nullable=False)
    #exposition = db.Column(db.Float, nullable=False)

    def __repr__(self):
        return "<File %r, %s>" % (self.id, self.path)


class SessionPropertyType(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(255), nullable=False)

    session_id = db.Column(db.String(255), db.ForeignKey(
        'session_entry.session_id', ondelete="CASCADE"), nullable=False)
    session_rel = db.relationship(sm,
                                  backref=db.backref('property_types', lazy=True, cascade="delete"))
    unique = sa.UniqueConstraint(name, session_id)


class FileProperty(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    value = db.Column(db.String(255), default="")
    property_type_id = db.Column(db.Integer, db.ForeignKey(
        'session_property_type.id'), nullable=False)

    file_id = db.Column(db.Integer, db.ForeignKey(
        'file.id'), nullable=False)
    file_rel = db.relationship(File,
                               backref=db.backref('properties', lazy=True, cascade="all, delete-orphan"))
