from typing import Dict
from flask import jsonify
import os
from environs import Env
import numpy as np
import pandas as pd
import json
import hashlib

env = Env()  # get environment variables from the .env file
env.read_env()  # path="../app/.env"


def session_folder_for_session(s):
    return os.path.join(env("UPLOAD_DIR"), "sessions", str(s.id))


def get_job_folder(job_id: int) -> str:
    """Returns the path of a job's results folder

    Args:
        job_id (int): _description_

    Returns:
        str: _description_
    """
    job_folder = os.path.join(env("UPLOAD_DIR"), "jobs", str(job_id))
    if not os.path.exists(job_folder):
        os.mkdir(job_folder)
    return job_folder

def get_hash_key(file_path: str):
    with open(file_path, "rb") as f:
        f.seek(0, 2)
        file_size = f.tell()
        if file_size < (1024 * 1024):
            f.seek(0)
            data = f.read()
            sha256 = hashlib.sha256()
            sha256.update(data)
            hash_key = sha256.hexdigest()
        else:
            # Go back to the start of the file
            f.seek(0)
            # Read the first half megabyte of the file
            first_half_mb = f.read(512 * 1024)
            # Go to the end of the file
            f.seek(-512 * 1024, 2)
            # Read the last half megabyte of the file
            last_half_mb = f.read(512 * 1024)
            # Create a new SHA-256 hash object
            sha256 = hashlib.sha256()
            # Update the hash object with the first half megabyte
            sha256.update(first_half_mb)
            # Update the hash object with the last half megabyte
            sha256.update(last_half_mb)
            # Get the hexadecimal representation of the hash
            hash_key = sha256.hexdigest()
    return hash_key