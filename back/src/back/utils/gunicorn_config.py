# then in the Gunicorn config file:
from prometheus_flask_exporter.multiprocess import GunicornPrometheusMetrics
import os
from environs import Env
import logging

env = Env()  # get environment variables from the .env file
env.read_env()

bind = "0.0.0.0:8080"
workers = 2
timeout = 200

#def when_ready(server):
#    GunicornPrometheusMetrics.start_http_server_when_ready(8000)
#    port=8081,host="http://dev.localhost"

#def child_exit(server, worker):
#    GunicornInternalPrometheusMetrics.mark_process_dead_on_child_exit(worker.pid)

def when_ready(server):
    logging.info("Starting prometheus server")
    GunicornPrometheusMetrics.start_http_server_when_ready(8081)

def child_exit(server, worker):
    GunicornPrometheusMetrics.mark_process_dead_on_child_exit(worker.pid)