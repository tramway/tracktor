#from celery import Celery
from environs import Env
#from werkzeug.utils import secure_filename
import os
#import numpy as np
import flask
#from flask import session
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_session import Session #, SqlAlchemySessionInterface
from datetime import timedelta
from back.utils.celery_app import create_celery_app
from flask_cors import CORS
import logging
from flask.json import JSONEncoder
from prometheus_flask_exporter.multiprocess import GunicornPrometheusMetrics
from prometheus_flask_exporter import PrometheusMetrics
from prometheus_client import Enum, Histogram
import os

env = Env()  # get environment variables from the .env file
env.read_env()

if not os.path.exists(os.environ["PROMETHEUS_MULTIPROC_DIR"]):
    os.mkdir(os.environ["PROMETHEUS_MULTIPROC_DIR"])

app = flask.Flask(__name__)
# https://flask-cors.readthedocs.io/en/latest/
# J'ai rajouté l'option ressources, je ne sais pas bien si ça change quelque chose...
CORS(app,
     supports_credentials=True,
     resources={"*": {"origins": "%s/*" % os.getenv("FRONT_ADDRESS")}})

class FloatEncoder(JSONEncoder):
	def default(self, obj):
		if isinstance(obj, float):
			return "%.5f" % obj
		return super().default(obj)

app.json_encoder = FloatEncoder

app.config['CELERY_BROKER_URL'] = os.getenv("CELERY_BROKER_URL")
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('SQLITE_DB')
app.config['SQLALCHEMY_ECHO'] = False  # True -> Vraiment beaucoup de logs
app.config['SQLITE_DB'] = os.getenv('SQLITE_DB')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SESSION_TYPE'] = 'sqlalchemy'
app.config['SESSION_SQLALCHEMY_TABLE'] = 'session_entry'
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(seconds=3600*48)
app.config['CELERY_REDIS_USE_SSL'] = False
app.config["SESSION_COOKIE_SAMESITE"] = "Lax"
app.config['MAX_CONTENT_LENGTH'] = 50 * 1000 * 1000  # Max upload size = 50mb

# DB STUFF
db = SQLAlchemy(app)
db.init_app(app)
migrate = Migrate(app, db)
app.config['SESSION_SQLALCHEMY'] = db
db_session = Session(app)
sm = app.session_interface.sql_session_model

# CELERY STUFF
celery_app = create_celery_app(_app=app)

# Prometheus
#metrics = GunicornInternalPrometheusMetrics(app=app,path="/api/metrics")
#metrics = PrometheusMetrics(app)
metrics = GunicornPrometheusMetrics(app,path="/metrics")
metrics.info('app_info', 'Application info', version='1.0.3')
metrics.init_app(app)

job_status = Enum("job_status","Statuses of jobs", states=["created","submitted","succeeded","failed","failed_at_submission"])
n_files_per_job = Histogram("files_per_job","Number of files per job")

logging.debug("Done creating app")

if __name__ == '__main__':
    app.run(debug=False, port=8080)
