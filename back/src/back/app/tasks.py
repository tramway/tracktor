from datetime import datetime
from glob import glob
import os
import numpy as np
import pandas as pd
import json
import zipfile
import logging
import shutil
from typing import Dict, List, Tuple
from environs import Env
import traceback
from logging import FileHandler, Logger
from itertools import combinations
import warnings

from back.app.app import celery_app, db, sm, job_status
from back.utils.routes_utils import get_job_folder, session_folder_for_session
from back.utils.db_models import Job

from palm_tools import TrackSets
from palm_tools.post_processing.ts_post_processing import PostProcessingStepSeries
from palm_tools.post_processing.gratin import GratinAllLengths, GratinParameters
from palm_tools.post_processing import StepsAndDiffusion
from palm_tools.analysis.mmd_analysis import MMDInterUnitAnalysis, MMDInterGroupAnalysis
from palm_tools.analysis.mds_analysis import MDSAnalysis
from palm_tools.analysis.analysis_tools.mmd import witness_function, keep_one_length_per_traj


env = Env()  # get environment variables from the .env file
env.read_env()
assert env("FRONT_ADDRESS")

# colonnes obligatoires qui ne doivent pas être prises en compte dans les comparaisons
FILE_PROPERTY_COLUMNS = ["scale", "DT", "file_path", "file_id"]
N_LATENT_GRID_POINTS = 30


def get_file_path_in_job(job_id: int, file_name: str):
    return os.path.join(get_job_folder(job_id), "uploaded_data", file_name)


def get_valid_factors_combinations(hierarchy: Dict[str, int]) -> List:
    factors_combinations = []
    for max_level in sorted(set(hierarchy.values())):
        mandatory_factors = [f for f, level in hierarchy.items() if level < max_level]
        optional_factors = [f for f, level in hierarchy.items() if level == max_level]
        for i in range(len(optional_factors)):
            for opt_f in combinations(optional_factors, i+1):
                factors_combinations.append(mandatory_factors + list(opt_f))
    factors_combinations = factors_combinations + [["file"]]
    return factors_combinations


def name_factors_list(factors: List[str]) -> str:
    return "_".join(factors)


def describe_group(g: Dict, hierarchy: Dict[str, int]) -> str:
    components = []
    if len(g) == 0:
        return "All"

    # Trier par hierarchie
    h_level = []

    for key, value in g.items():
        if type(value) == int:
            components.append("%s = %d" % (key, value))
        else:
            components.append(value)
        h_level.append(hierarchy[key])
    components = [x for _, x in sorted(zip(h_level, components))]
    return "|".join(components)


def describe_comp(comp: Tuple[Dict], hierarchy: Dict[str, int]) -> str:
    return " VS ".join([describe_group(g, hierarchy) for g in comp])


def sort_groups(groups: List[Dict], hierarchy: Dict[str, int]) -> List[Dict]:
    descriptions = [describe_group(g, hierarchy) for g in groups]
    sorted_groups = [(index, g) for _, g, index in sorted(
        zip(descriptions, groups, range(len(groups))))]
    return sorted_groups


def df_to_plotly_scatter_dict(latent_df: pd.DataFrame,
                              cols_to_include: List[str],
                              coord_col_names=["U_1", "U_2"],
                              ):
    ldict = {}
    for l in cols_to_include:
        if latent_df[l].dtype.kind in 'fc':
            logging.debug("Column %s is numeric" % l)
            ldict[l] = [{
                "name": str(l),
                "text": latent_df["file"].tolist() if "file" in latent_df.columns else None,
                "x": latent_df[coord_col_names[0]].to_list(),
                "y": latent_df[coord_col_names[1]].to_list(),
                "hoverinfo":"text",
                "mode": "markers",
                "showlegend": False,
                "marker":{
                    "color": latent_df[l].tolist(),
                    "showscale": True,
                    "colorbar":{
                        "title": l
                    }
                },
                "type": "scatter"
            }]
        else:
            logging.debug("Column %s is categorical" % l)
            ldict[l] = []
            for g, df in latent_df.groupby(l):
                if l == "file":
                    l = l.split("/")[-1]
                ldict[l].append(
                    {
                        "name": str(g),
                        "x": df[coord_col_names[0]].to_list(),
                        "y": df[coord_col_names[1]].to_list(),
                        "hoverinfo": "name",
                        "showlegend": False,
                        "mode": "markers",
                        "type": "scatter",
                    }
                )
    return ldict


def group_values_by_factors(traj_level_df, cols_to_group_by=[], values=[]) -> Dict:
    ldict = {}
    for v in values:
        ldict[v] = {}
    for col in cols_to_group_by:
        for g, df in traj_level_df.groupby(col):
            name = g
            for v in values:
                if col not in ldict[v]:
                    ldict[v][col] = []
                # TODO: changer dans le front la façon dont c'est lu
                ldict[v][col].append(
                    {
                        "label": name,
                        "values": df[v].to_list(),
                    }
                )
    return ldict


def group_values_by_several_factors(traj_level_df, cols_to_group_by=[], values=[]) -> Dict:
    ldict = {"label": [], "values": {}}
    for v in values:
        ldict["values"][v] = []
    for g, df in traj_level_df.groupby(cols_to_group_by):
        if len(cols_to_group_by) > 1:
            name = " - ".join([str(f) for f in g])
        else:
            name = g
        ldict["label"].append(name)
        for v in values:
            ldict["values"][v].append(df[v].tolist())
    return ldict


def df_to_trajectory_counts(df, cols_to_group_by):
    """
    Compte le nombre de trajectoires par facteur.
    - au total
    - par fichier
    """

    total_counts = []
    detailed_counts = []

    if "file" not in cols_to_group_by:
        gb = df.groupby(cols_to_group_by + ["file"])
        df_file = gb.agg(
            {"traj_ID": "nunique"}).reset_index().rename(columns={"traj_ID": "n_trajs"})
        length_of_file = gb[["frame"]].max() - gb[["frame"]].min()
        length_of_file = length_of_file.rename(columns={"frame": "n_frames"})
        df_file = df_file.merge(length_of_file, left_on=cols_to_group_by +
                                ["file"], right_index=True, how="left").reset_index()

        for val, s_df_ in df_file.groupby(cols_to_group_by):
            group_name = val
            if len(cols_to_group_by) > 1:
                group_name = " - ".join(group_name)
            trajs_per_frame = (s_df_["n_trajs"] / s_df_["n_frames"])
            total_counts.append({
                "x": [group_name],
                "y": [int(s_df_["n_trajs"].sum())],
                "y_per_hundred_frames": [100*trajs_per_frame.mean()],
                "n_files": [int(s_df_["file"].nunique())]
            })

            detailed_counts.append({
                "x": [group_name for _ in range(s_df_.shape[0])],
                "y": [int(i) for i in s_df_["n_trajs"].tolist()],
                "y_per_hundred_frames": [100*f for f in trajs_per_frame.tolist()],
                "text": s_df_["file"].tolist()
            })
    else:
        cols_to_group_by = [c for c in cols_to_group_by if c != "file"] + ["file"]
        gb = df.groupby(cols_to_group_by)
        s = gb[["traj_ID"]].nunique().rename(columns={"traj_ID": "n_trajs"})
        n_frames = (gb[["frame"]].max() - gb[["frame"]].min()
                    ).rename(columns={"frame": "n_frames"})
        s = s.merge(n_frames, left_index=True, right_index=True, how="left")

        for group, row in s.iterrows():
            group_name = group
            if len(cols_to_group_by) > 1:
                group_name = " - ".join(group_name)
            total_counts.append({
                "x": [group_name],
                "y": [int(row["n_trajs"])],
                "y_per_hundred_frames": [100*row["n_trajs"] / row["n_frames"]]
            })
        detailed_counts = [{
            "x": ["all files" for i in range(s.shape[0])],
            "y": [int(i) for i in s["n_trajs"].tolist()],
            "y_per_hundred_frames": [100*f for f in (s["n_trajs"] / s["n_frames"]).tolist()],
            "text": s.index.tolist()
        }]

    return total_counts, detailed_counts


def remove_redundant_columns(df):
    col_hash = {}
    initial_columns = list(df.columns)
    new_columns = []
    if "file" in df:
        new_columns.append("file")
    for c in initial_columns:
        if c == "file":
            continue
        col_hash[c] = hash(pd.factorize(df[c])[0].data.tobytes())
    hash_values = set(col_hash.values())
    for val in hash_values:
        matching_cols = [c for c in col_hash if col_hash[c] == val]
        new_col = "/".join(matching_cols)
        df[new_col] = df[matching_cols].astype(str).agg('/'.join, axis=1)
        if df[new_col].nunique() > 1:
            new_columns.append(new_col)
    return df[new_columns]


def save_default_gratin_model(dim: int = 2):
    from gratin.standard import train_model

    default_model_folder = os.path.join(
        env("GRATIN_MODELS_FOLDER"), "%dD" % dim, "default")
    default_model_path = os.path.join(
        env("GRATIN_MODELS_FOLDER"), "%dD" % dim, "default", "model.ckpt")
    if os.path.exists(default_model_path):
        return  # Prevent two trainings from being run simultaneously
    if not os.path.exists(default_model_folder):
        os.makedirs(default_model_folder)
        logging.critical("Created folder %s" % default_model_path)
    logging.critical("Training new gratin model")
    model, encoder = train_model(
        # indicate an empty folder where to store the model once trained
        export_path=default_model_folder,
        num_workers=0,  # number of workers used to simulate trajectories during the training phase
        # time separating two successive position recordings in your trajectories (exposure time of the camera)
        dim=dim,
        time_delta_range=(0.005, 5.),
        log_diffusion_range=(
            -2.2,
            1.2,
        ),  # log-diffusion is drawn following a truncated centered gaussian in this range
        length_range=(7, 80),  # length is drawn in a log-uniform way in this interval
        noise_range=(
            0.015,
            0.05,
        ),  # localization uncertainty, in micrometers (one value per trajectory)
        max_n_epochs=50  # Maximum epochs on which to run the training.
    )

MAX_TRAJECTORIES = 5000

def do_computation(EXPORT_FOLDER, TRACKS_FILES, INDEX_DF, RUN_NAME, logger: Logger,is3D: bool):

    initial_format = "%(asctime)s : \t %(message)s"

    logging_handler = logger.handlers[-1]
    assert isinstance(logging_handler, FileHandler)
    fmt = "[GETTING EMBEDDINGS (1/6)] " + initial_format
    logging_handler.setFormatter(logging.Formatter(fmt, datefmt='%m-%d %H:%M'))

    logger.info("%d Files : %s" % (len(TRACKS_FILES),TRACKS_FILES))

    tss = TrackSets.from_files(
        TRACKS_FILES, root_folder=EXPORT_FOLDER, index_df=INDEX_DF, is3D=is3D)

    dim = len(tss[0].coord_cols)

    available_models = glob(os.path.join(env("GRATIN_MODELS_FOLDER"), "%dD" % dim, "*"))
    if len(available_models) == 0:
        save_default_gratin_model()
        available_models = glob(os.path.join(
            env("GRATIN_MODELS_FOLDER"), "%dD" % dim, "*"))
        assert len(available_models) > 0

    # Steps of the process
    gt = GratinAllLengths(params=GratinParameters(
        path=available_models[0]), logger=logger)
    sd = StepsAndDiffusion(logger=logger)
    pps = PostProcessingStepSeries(processing_steps=[gt, sd], logger=logger)

    pps.process(tss)

    # save traditional indicators (alpha, best model, log_diffusivity, length)
    factors_list = INDEX_DF.columns.to_list()

    locs_df = []
    for ts in tss:
        locs = ts.locs[ts.coord_cols + ["n", "frame",
                                        "t", "n_points", "log_D", "est_sigma"]]
        trajs = ts.trajs_df.sort_values("L", ascending=False).groupby("n")[
            ["alpha", "best_model", "U_1", "U_2"]].first()
        locs = locs.merge(trajs, left_on="n", right_index=True, how="left")
        file_path = ts.origin_file
        for col in factors_list:
            locs[col] = INDEX_DF[INDEX_DF["file"] == file_path][col].values[0]
        locs["file"] = file_path
        locs_df.append(locs)

    fmt = "[SAVING EMBEDDINGS (2/6)] " + initial_format
    logging_handler.setFormatter(logging.Formatter(fmt, datefmt='%m-%d %H:%M'))

    # locs_df contient toutes les positions + informations
    locs_df = pd.concat(locs_df, axis=0)
    locs_df.to_csv(
        os.path.join(
            EXPORT_FOLDER,
            "variables_and_trajectories.csv"
        )
    )
    logger.info("Saved localization-level files")

    # traj_level_df synthétise au niveau de la trajectoire
    traj_level_df = locs_df.groupby(["file", "n"])[
        ["n_points", "log_D", "est_sigma", "alpha", "best_model", "U_1", "U_2"] + [c for c in factors_list if c != "file"]].first().reset_index()
    traj_level_df.to_csv(
        os.path.join(
            EXPORT_FOLDER,
            "variables_by_trajectory.csv"
        )
    )
    logger.info("Saved trajectory-level files")

    # Ensuite on groupe en fonction des différents critères, et on sauve ça dans un JSON qui contient des listes avec une valeur par trajectoire
    grouped_dict = group_values_by_factors(traj_level_df, cols_to_group_by=factors_list, values=[
        "alpha", "best_model", "log_D", "est_sigma", "n_points"])
    grouped_dict["factors_list"] = factors_list
    with open(os.path.join(EXPORT_FOLDER, "values_grouped_by_factors.json"), "w") as outfile:
        json.dump(grouped_dict, outfile)

    logger.info("Saved factor-level information")

    fmt = "[COMPUTING MAPS (3/6)] " + initial_format
    logging_handler.setFormatter(logging.Formatter(fmt, datefmt='%m-%d %H:%M'))

    hierarchy = json.load(open(os.path.join(EXPORT_FOLDER, "hierarchy.json"), "r"))
    assert all([col in hierarchy for col in INDEX_DF.columns if col != "file"])

    valid_factors = get_valid_factors_combinations(hierarchy)
    mds_unit_dict = {}
    for i, factors in enumerate(valid_factors):
        factors_run_name = name_factors_list(factors)

        fmt = ("[COMPUTING MAPS (3/6) : %s (%d / %d)] " %
               (factors_run_name, i + 1, len(valid_factors))) + initial_format
        logging_handler.setFormatter(logging.Formatter(fmt, datefmt='%m-%d %H:%M'))

        logger.info("Starting to map according to %s" % (" / ".join(factors)))

        mmd = MMDInterUnitAnalysis(track_sets=tss,
                                   group_by_keys=factors,
                                   # it is possible to delimit units by a column of the trajectories files (if they have additional information)
                                   # or to consider a cartesian product of two columns.
                                   # example of valid values : ["file","time_bin"], ["file","organelle"], ["organelle"] (spanned across files)
                                   use_high_dimension=False,
                                   unbiased=False,
                                   n_processes=4,
                                   n_bootstraps_D_sigma=20,
                                   n_max_trajs_per_unit=MAX_TRAJECTORIES,  # For speed, we never consider more than 500 trajectories per unit (now 5k)
                                   n_min_trajs_per_unit=10,  # For accuracy, we discard units with less than 150 trajectories (now 10)
                                   run_name=factors_run_name,
                                   logger=logger)

        if len(mmd.groups) > 2:  # Il faut au moins trois unités
            logger.info("Found %d groups, computing map" % len(mmd.groups))
            mmd.process(force_recompute=True)
            mmd._save_dict()
            # print("SAVED RESULTS : " + ' '.join(glob(os.path.join(mmd.output_path, "*"))))

            mds_unit = MDSAnalysis(
                track_sets=tss, run_name=factors_run_name, mmd_name=factors_run_name, logger=logger)
            mds_unit.process(force_recompute=True)
            mds_unit._save_dict()
            # save MDS results
            mds_df = mds_unit._unit_info
            # X1 and X2 are the coordinates of the MDS

            # Clean file names
            mds_df["file"] = mds_df["file"].str.split(
                "/", expand=True)[5:].astype(str).agg("/".join, axis=1)

            mds_unit_dict[factors_run_name] = df_to_plotly_scatter_dict(mds_df,
                                                                        cols_to_include=factors_list,
                                                                        coord_col_names=["X_1", "X_2"])

        else:
            logger.info("Only two groups, skipping map")

    with open(os.path.join(EXPORT_FOLDER, "mds_unit_dict.json"), "w") as outfile:
        json.dump(mds_unit_dict, outfile)

    fmt = "[COMPUTING P-VALUES (4/6)] " + initial_format
    logging_handler.setFormatter(logging.Formatter(fmt, datefmt='%m-%d %H:%M'))

    logger.info("Starting to compare files, grouped by conditions")

    mmd = MMDInterGroupAnalysis(track_sets=tss,
                                unit_key=["file"],
                                null_mode="mix",
                                group_by_keys=[
                                    col for col in INDEX_DF.columns if col != "file"],
                                # it is possible to delimit units by a column of the trajectories files (if they have additional information)
                                # or to consider a cartesian product of two columns.
                                # example of valid values : ["file","time_bin"], ["file","organelle"], ["organelle"] (spanned across files)
                                use_high_dimension=False,
                                n_max_bootstraps=5000,
                                unbiased=False,
                                n_processes=4,
                                n_max_trajs_per_unit=MAX_TRAJECTORIES,  # For speed, we never consider more than 500 trajectories per unit (now 5k)
                                n_min_trajs_per_unit=10,  # For accuracy, we discard units with less than 150 trajectories (now 10)
                                hierarchy=hierarchy,
                                run_name=RUN_NAME,
                                logger=logger)

    logger.info("%d GROUPS : " % len(mmd.groups))
    for g in mmd.groups:
        logger.info(describe_group(g, hierarchy))

    logger.info("%d COMPARISONS : " % len(mmd.comparisons))
    for c in mmd.comparisons:
        logger.info(describe_comp(c, hierarchy))

    mmd.process(force_recompute=True)
    mmd._save_dict()

    try:
        latent_df = mmd.latent_df.sort_values(
            "L", ascending=False).groupby(["file", "n"]).first().reset_index()
        # TODO: add to latent_df the columns computed by the steps & diffusion step ?

        latent_df["file"] = latent_df["file"].str.split(
            "/", expand=True)[5:].astype(str).agg("/".join, axis=1)

        latent_dict = df_to_plotly_scatter_dict(
            latent_df.sample(min(latent_df.shape[0], MAX_TRAJECTORIES), replace=False), cols_to_include=factors_list + ["alpha", "best_model", "log_D", "est_sigma"])
        with open(os.path.join(EXPORT_FOLDER, "latent_dict.json"), "w") as outfile:
            json.dump(latent_dict, outfile)
    except Exception as e:
        raise e

    fmt = "[SAVING DENSITIES (5/6)] " + initial_format
    logging_handler.setFormatter(logging.Formatter(fmt, datefmt='%m-%d %H:%M'))

    witness_function_dict = {}
    stats_dict = {}
    densities_dict = {}

    n_grid_points = N_LATENT_GRID_POINTS
    n_bootstraps_criticality = 100

    for i, c in enumerate(mmd.comparisons):
        c0_idx = mmd.groups.index(c[0])
        c1_idx = mmd.groups.index(c[1])

        w1s = []
        w2s = []

        for _ in range(n_bootstraps_criticality):

            H1 = mmd.sample_latent_vecs_for_cond(c[0])
            H2 = mmd.sample_latent_vecs_for_cond(c[1])
            N = int(min(H1.traj_ID.nunique(), H2.traj_ID.nunique()) / 2.5)
            H1, H2 = keep_one_length_per_traj([H1, H2], cols_to_keep=mmd.latent_cols)
            H1 = H1.sample(N)
            H2 = H2.sample(N)
            _, _, _, w1, w2 = witness_function(
                H1.values, H2.values, lims=mmd.lims, n_points=n_grid_points)

            w1s.append(w1)
            w2s.append(w2)

        w1 = np.stack(w1s, axis=0)
        w2 = np.stack(w2s, axis=0)
        w = w1 - w2
        assert np.all(~np.isnan(w))
        mean_w = np.mean(w, axis=0)
        amplitude = mean_w**2
        var = np.var(w, axis=0)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            stat = amplitude / var
        stat[var == 0] = 0.
        stat[np.isnan(stat)] = 0.0
        assert np.all(~np.isinf(stat))
        assert np.all(~np.isnan(stat))
        w1 = np.mean(w1, axis=0)
        w2 = np.mean(w2, axis=0)
        w = np.mean(w, axis=0)

        # Update densities and witness_function dicts
        witness_function_dict[i] = w.tolist()
        stats_dict[i] = stat.tolist()

        if c0_idx in densities_dict:
            densities_dict[c0_idx].append(w1)
        else:
            densities_dict[c0_idx] = [w1]
        if c1_idx in densities_dict:
            densities_dict[c1_idx].append(w2)
        else:
            densities_dict[c1_idx] = [w2]

        logger.info("Saved densities for comparison %d / %d" %
                    (i+1, len(mmd.comparisons)))

    # Average over all comparisons in which the condition is implied
    # NB: densities might vary because of the adjustment by length
    for condition in densities_dict:
        densities_dict[condition] = np.mean(
            np.stack(densities_dict[condition], axis=-1), axis=-1).tolist()

    with open(os.path.join(EXPORT_FOLDER, "witness_function_dict.json"), "w") as outfile:
        json.dump(witness_function_dict, outfile)
    with open(os.path.join(EXPORT_FOLDER, "stats_dict.json"), "w") as outfile:
        json.dump(stats_dict, outfile)
    with open(os.path.join(EXPORT_FOLDER, "densities_dict.json"), "w") as outfile:
        json.dump(densities_dict, outfile)

    # create zip results file

    fmt = "[CREATING EXPORT ARCHIVE (6/6)] " + initial_format
    logging_handler.setFormatter(logging.Formatter(fmt, datefmt='%m-%d %H:%M'))

    filenames = {os.path.join(EXPORT_FOLDER, "MDS", RUN_NAME, "unit_info.csv"): "info_and_MDS_by_units.csv",
                 os.path.join(EXPORT_FOLDER, "MMD_inter_groups",
                              RUN_NAME, "latent_df.csv"): "latent_vectors_by_group.csv",
                 os.path.join(EXPORT_FOLDER, "variables_by_trajectory.csv"): "variables_by_trajectory.csv",
                 os.path.join(EXPORT_FOLDER, "output.log"): "output.log",
                 os.path.join(EXPORT_FOLDER, "MMD_inter_units", RUN_NAME, "latent_df.csv"): "latent_vectors_by_unit.csv"}

    all_filenames = [f for f in filenames]
    for f in all_filenames:
        if not os.path.exists(f):
            del filenames[f]

    with zipfile.ZipFile(os.path.join(EXPORT_FOLDER, "results_archives.zip"), mode="w") as archive:
        for filename, target in filenames.items():
            archive.write(filename, target)


@celery_app.task()
def remove_old_data():
    #print("Removing old data")
    # Select expired sessions
    old_sessions = sm.query.filter(sm.expiry < datetime.utcnow()).all()
    old_session_ids = [s.session_id for s in old_sessions]
    # Select jobs of these expired sessions
    old_jobs = Job.query.filter(Job.session_id.in_(
        old_session_ids)).filter_by(public=False).all()
    for j in old_jobs:
        job_folder = get_job_folder(j.id)
        if (j.public == True) or int(j.public) > 0:
            continue
        if os.path.exists(job_folder):
            print("Removing job %s" % job_folder)
            shutil.rmtree(job_folder)
        db.session.delete(j)
    db.session.commit()

    for s in old_sessions:
        session_folder = session_folder_for_session(s)
        if os.path.exists(session_folder):
            print("Removing %s" % session_folder)
            shutil.rmtree(session_folder)
    # db.session.delete(s)
    # db.session.commit()


def setup_logger(job_id):
    logger = logging.getLogger("job_%d" % int(job_id))
    job_folder = get_job_folder(job_id)
    fh = FileHandler(os.path.join(job_folder, "output.log"))
    fh.setLevel(logging.DEBUG)
    logger.setLevel(logging.INFO)
    logger.addHandler(fh)
    return logger, fh


class JobLogger:
    def __init__(self, job_id):
        self.logger, self.fh = setup_logger(job_id=int(job_id))

    def __enter__(self):
        # temporarily redirect logging output to the job-specific logger
        return self.logger

    def __exit__(self, exc_type, exc_value, traceback):
        # restore the original handlers for the root logger
        self.logger.removeHandler(self.fh)


@celery_app.task()
def do_job(job_id, session_folder):

    #logger = get_task_logger(__name__)

    with JobLogger(job_id) as logger:

        try:
            job = Job.query.filter_by(id=job_id).first_or_404(
                "Could not find job with ID %s" % job_id)
            job.status = Job.PROCESSING
            job.start_time = datetime.utcnow()
            db.session.commit()
            # time.sleep(5)
            # Possible de faire ça avec un rotating file ? Pour éviter que ça soit trop gros

            job_folder = get_job_folder(job_id)

            INDEX_DF = pd.read_csv(os.path.join(
                job_folder, "final_index.csv"), keep_default_na=False, index_col=0)

            logger.info("Starting computations")

            do_computation(EXPORT_FOLDER=job_folder,
                           TRACKS_FILES=INDEX_DF["file"].tolist(),
                           INDEX_DF=INDEX_DF,
                           RUN_NAME="example",
                           logger=logger,
                           is3D=job.is3D,
                           )

            logger.info("Done !")
            # Maintenant il faut lire le fichier depuis l'interface
            # logger.removeHandler(fh)
            job.status = Job.SUCCESS
            job_status.state("succeeded")

        except BaseException as ex:
            db.session.rollback()
            job.status = Job.FAILED
            logger.error("Caught error : %s" % ex)
            logger.error(ex, exc_info=True)
            job_status.state("failed")
            # traceback.print_exception(type(ex), ex, ex.__traceback__)
        finally:
            job.end_time = datetime.utcnow()
            db.session.commit()

    return "did job"
