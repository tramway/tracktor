from __future__ import annotations
from typing import List
from flask import jsonify, request, session
import numpy as np
import os
from sklearn.decomposition import PCA
from environs import Env
from itertools import combinations
from back.app.tasks import \
    df_to_trajectory_counts, \
    do_job, \
    describe_group, \
    witness_function, \
    keep_one_length_per_traj, \
    group_values_by_several_factors, \
    remove_redundant_columns,  \
    sort_groups, \
    get_valid_factors_combinations, \
    name_factors_list, \
    get_file_path_in_job, \
    FILE_PROPERTY_COLUMNS, \
    N_LATENT_GRID_POINTS, \
    MAX_TRAJECTORIES
from back.app.app import app, metrics, db, sm, job_status, n_files_per_job
from back.utils.db_models import Job, File, FileProperty, SessionPropertyType
from back.utils.routes_utils import session_folder_for_session, get_job_folder, get_hash_key
import pandas as pd
import json
import datetime
import shutil
from flask import send_file
import logging
from palm_tools.utils.read_file import check_file_and_read
from matplotlib.colors import Normalize, LogNorm, to_hex, TwoSlopeNorm
from matplotlib.cm import viridis, coolwarm, tab10
from palm_tools import TrackSets
from palm_tools.analysis.mmd_analysis import MMDInterUnitAnalysis, MMDInterGroupAnalysis
from palm_tools.analysis.mds_analysis import MDSAnalysis
from palm_tools.analysis.analysis_tools.mmd import get_latent_grid
from sqlalchemy.exc import IntegrityError

env = Env()  # get environment variables from the .env file
env.read_env()

ACL_ORIGIN = 'Access-Control-Allow-Origin'
ACL_METHODS = 'Access-Control-Allow-Methods'
ACL_ALLOWED_HEADERS = 'Access-Control-Allow-Headers'

OPTIONS_METHOD = 'OPTIONS'
ALLOWED_ORIGINS = env("FRONT_ADDRESS")
ALLOWED_METHODS = 'GET, POST, PUT, DELETE, OPTIONS'
ALLOWED_HEADERS = 'Authorization, DNT, X-CustomHeader, Keep-Alive, User-Agent, ' \
                  'X-Requested-With, If-Modified-Since, Cache-Control, Content-Type, ' \
                  'Credentials'

ALL_MODELS = {"OU": 0,
              "fBM": 1, "CTRW": 2, "sBM": 3, "LW": 4}


def _ensure_session_is_in_db():
    """
    Ensure that the current session has been recorded in the database
    """
    app.session_interface.save_session(
        app=app, session=session, response=app.make_response("dummy response"))


def _get_session():
    """Returns the row of the session table corresponding to the actual session

    Returns:
        _type_: _description_
    """
    _ensure_session_is_in_db()
    return sm.query.filter(sm.session_id.contains(session.sid)).first_or_404("Could not find session with ID containing %s" % session.sid)


def get_session_jobs():
    """Returns all jobs linked with one session (Instances of 'Job' class)

    Returns:
        _type_: _description_
    """

    s = _get_session()
    return s.jobs + Job.query.filter_by(public=True, status=Job.SUCCESS).all()


def get_active_job() -> int:
    """
    Returns the ID of the active job, which is stored in the session cookie.
    If it already is in the cookie, then the function simply reads this ID and returns it
    If there is no "active_job" key in the cookie, the function looks for candidate jobs in the database
    (i.e. jobs associated with the current session and not already canceled or submitted).
    If there is no such candidate, then we create a such Job, store it in the DB and return its ID.

    Returns:
        int: ID of the session's active job
    """
    # If there's already an active job stored, return it
    if "active_job" in session:
        return session["active_job"]
    # Otherwise, look if there's a good candidate (there shoudn't be more than 1)
    jobs = get_session_jobs()
    for job in jobs:
        if job.status < Job.QUEUED:  # The job.status is an integer
            if "active_job" not in session:
                session["active_job"] = job.id
            else:
                print(
                    "Strange that there are two candidate jobs, aborting all but the first")
                job.status = Job.ABORTED
                db.session.commit()
    # If we've found the good candidate, return it
    if "active_job" in session:
        return session["active_job"]
    else:
        return new_active_job()


def new_active_job() -> int:

    jobs = get_session_jobs()
    for job in jobs:
        if job.status < Job.QUEUED:  # The job.status is an integer
            job.status = Job.ABORTED
    db.session.commit()
    # create a new job and store it as the active one
    session_id = "session:%s" % session.sid
    secret_key = "".join(np.random.choice(
        ["A", "B", "C", "D", "E", "F", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"], size=40, replace=True))
    logging.critical("Secret key = %s" % secret_key)
    job = Job(session_id=session_id, secret_key=secret_key)
    db.session.add(job)
    db.session.commit()
    job_folder = get_job_folder(job.id)
    session["active_job"] = job.id
    job_status.state("created")
    return session["active_job"]


def get_session_folder() -> str:
    """
    Returns the path of the folder where the session's data is stored.
    If it doesn't exist yet, it's created, and an empty index file is added

    Returns:
        str: folder where a session's data is stored
    """

    s = _get_session()
    session_folder = session_folder_for_session(s)
    if not os.path.exists(session_folder):
        os.mkdir(session_folder)

    index_path = os.path.join(session_folder, "index.csv")
    if not os.path.exists(index_path):
        empty_index_df = pd.DataFrame.from_dict({
            "file_name": [],
            "file": [],
            "include": [],
        })
        empty_index_df.to_csv(index_path, index=False)

    return session_folder


def get_index_table(job_id: int = None, is3D: bool = False):
    """returns index table associated to the active session

    """
    if job_id is None:
        s = _get_session()

        prop_list_query = db.session.query(
            SessionPropertyType.id.label("property_type_id"),
            SessionPropertyType.name,
            File.id.label("file_id"),
            File.name.label("file_name"),
            File.path.label("file_path"),
            FileProperty.value,
            FileProperty.id.label("property_id")).\
            filter(SessionPropertyType.session_id == s.session_id).\
            filter(File.id == FileProperty.file_id).\
            filter(File.is3D == is3D).\
            filter(FileProperty.property_type_id == SessionPropertyType.id)

        file_props_df = pd.read_sql(prop_list_query.statement, db.engine)
        index_df = pd.pivot_table(file_props_df, values="value",
                                  index="file_id", columns="name", aggfunc="first")  # .rename(columns={"path": "file_name"})
        index_df = index_df.merge(file_props_df.loc[~file_props_df.file_name.isnull()].groupby("file_id")[
                                  ["file_name", "file_path"]].first(), left_index=True, right_index=True, how="left").reset_index()

        if "blank_column_123" in index_df.columns:
            del index_df["blank_column_123"]

        index_df.fillna("", inplace=True)

    else:
        job = Job.query.filter_by(id=job_id).first_or_404(
            "Job with ID %s does not exist" % job_id)
        session_id = "session:%s" % session.sid
        if (not job.session_id == session_id) and (not job.public):
            raise BaseException(
                "This job is not linked to this session and isn't public !")

        job_folder = get_job_folder(job.id)
        index_df = pd.read_csv(os.path.join(
            job_folder, "index.csv"), keep_default_na=False)

    return index_df


@app.route('/api/publish_job/<job_secret_key>', methods=['GET'])
def publish_job(job_secret_key):
    job = Job.query.filter_by(secret_key=job_secret_key).first_or_404(
        "No job found with this secret key")
    job.public = True
    db.session.commit()
    return jsonify(success=True)


@app.route('/api/unpublish_job/<job_secret_key>', methods=['GET'])
def unpublish_job(job_secret_key):
    job = Job.query.filter_by(secret_key=job_secret_key).first_or_404(
        "No job found with this secret key")
    job.public = False
    db.session.commit()
    return jsonify(success=True)


@app.route('/api/job_info/<job_id>', methods=["GET"])
def job_info(job_id):
    job_id = int(job_id)

    # Look for a job with this ID
    job = Job.query.filter_by(id=job_id).first_or_404(
        "Job with ID %s does not exist, is not over, or hasn't run well" % job_id)
    session_id = "session:%s" % session.sid

    if not job.public and not job.session_id == session_id:
        print("QUERIED JOB SESSION : ", job.session_id)
        print("CURRENT SESSION : ", session_id)
        raise BaseException("This job is not linked to this session !")

    return jsonify(
        name=job.name,
        status=job.status,
        public=job.public,
        submit=job.submit_time,
        started=job.start_time,
        is3D=job.is3D,
        end_time=job.end_time)


@app.route("/api/get_job_factors/<job_id>", methods=["GET"])
def get_job_factors(job_id):
    return jsonify({"factors": _get_job_factors(int(job_id))})


@app.route("/api/get_densities", methods=["POST"])
def get_densities():
    job_id = int(json.loads(request.form["job_id"]))

    assert check_that_job_can_be_accessed(job_id)

    factors = _get_job_factors(job_id)
    if "factors" not in request.form:
        return jsonify({"factors": factors})

    factors_request = json.loads(request.form["factors"])

    tss, mmd_group, mmd_unit, mds = get_job_objects(job_id)

    densities = {}
    factors_no_file = [f for f in factors_request if f != "file"]
    if len(factors_no_file) == 0:
        groups_to_try = mmd_group.groups + [{}]
    else:
        groups_to_try = mmd_group.groups

    latent_vecs = []
    for g in groups_to_try:
        if len(g) > 0 and len(g) <= len(factors_no_file) and all([f in g for f in factors_no_file[:len(g)]]):

            H1 = mmd_group.sample_latent_vecs_for_cond(g)
            latent_vecs.append({
                "H": H1,
                "level": len(g),
                "name": describe_group(g, mmd_group.hierarchy),
                "tags": [v for k, v in g.items()]})
        elif len(g) == len(factors_no_file) and "file" in factors_request:
            for u in mmd_group.get_units_for_condition(g):
                H1 = mmd_group.latent_vecs[u]
                f_name: str = mmd_group.units[str(u)]["file"]
                f_name = f_name.replace(get_job_folder(job_id) + "/uploaded_data/", "")
                latent_vecs.append({
                    "H": H1,
                    "level": len(g) + 1,
                    "name": f_name,
                    "tags": [v for k, v in g.items()],
                    "file": f_name
                })

    hs = keep_one_length_per_traj(
        [lv["H"] for lv in latent_vecs], cols_to_keep=mmd_group.latent_cols)
    for (h, lv) in zip(hs, latent_vecs):
        xx, yy, _, w1, _ = witness_function(
            h.values, h.values, lims=mmd_group.lims, n_points=30)
        name = lv["name"]
        densities[name] = {"density": w1.tolist(), "level": lv["level"],
                           "tags": lv["tags"]}
        if "file" in lv:
            densities[name]["file"] = lv["file"]

    x = xx.tolist() if len(densities) > 0 else []
    y = yy.tolist() if len(densities) > 0 else []
    return jsonify({"densities": densities, "x": x, "y": y})


@app.route('/api/get_job_columns', methods=["POST"])
def get_job_columns():
    """
    Returns the list of columns that will be preserved in the INDEX_DF table with the selectedRows
    of the index table from the upload page.
    To be used to pass columns to the JobDialog, in which the user enters the hierarchy.
    """
    selected_rows = json.loads(request.form["selected_rows"])
    valid_columns = json.loads(request.form["colnames"])
    selected_rows_df = pd.DataFrame(selected_rows)
    columns_to_ignore = ["DT", "file_id", "file_name", "file_path", "scale"]
    selected_rows_df = selected_rows_df[[
        c for c in selected_rows_df.columns if c not in columns_to_ignore and c in valid_columns]]
    return jsonify({"columns": remove_redundant_columns(selected_rows_df).columns.tolist()})


def _get_job_factors(job_id: int):
    """
    Returns an ordered list with all valid combinations of factors for this job
    """
    job_folder = get_job_folder(job_id)
    hierarchy = json.load(open(os.path.join(job_folder, "hierarchy.json"), "r"))

    factors_combinations = get_valid_factors_combinations(hierarchy)
    return factors_combinations

@app.route("/api/daily_monitoring_stats", methods=["GET"])
def daily_monitoring_stats():

    today = datetime.date.today()
    start_of_day = datetime.datetime.combine(today, datetime.datetime.min.time())
    end_of_day = datetime.datetime.combine(today, datetime.datetime.max.time())

    all_jobs = db.session.query(Job).filter(
        Job.create_time >= start_of_day,
        Job.create_time <= end_of_day
    ).all()

    success_jobs = db.session.query(Job).filter(
        Job.submit_time >= start_of_day,
        Job.submit_time <= end_of_day,
        Job.status == Job.SUCCESS
    ).all()

    failed_jobs = db.session.query(Job).filter(
        Job.submit_time >= start_of_day,
        Job.submit_time <= end_of_day,
        Job.status == Job.FAILED
    ).all()

    files = db.session.query(File).filter(
        File.upload_time >= start_of_day,
        File.upload_time <= end_of_day
    ).all()

    return jsonify({
        "created_jobs":len(all_jobs),
        "success_jobs":len(success_jobs),
        "failed_jobs":len(failed_jobs),
        "uploaded_files":len(files),
    })



@app.route("/api/factors_points_data/<job_id>", methods=["POST"])
def factors_points_data(job_id):
    job_id = int(job_id)
    check_that_job_can_be_accessed(job_id)
    tss, mmd_group, mmd_units, mds = get_job_objects(job_id)
    job_folder = get_job_folder(job_id)

    points = json.loads(request.form["points"])
    factors_request = json.loads(request.form["factors"])
    zone_radius = 1.5

    cols_to_keep = mmd_group.group_by_keys + \
        ["file"] + ["alpha", "best_model", "log_D", "est_sigma", "n_points"]

    results = {}

    # Maintenant, pour le graphe du taux d'occupation
    # On groupe les trajectoires par facteur (avec autant de trajectoires par unité au sein d'un groupe)
    # On égalise les longueurs entre les groupes
    # On regarde la fraction des trajectoires du groupe qui sont proches de chaque point

    if "file" in factors_request:
        latent_vecs = []
        all_units = mmd_group.get_units_for_condition({})
        #logging.warn("%d units to concat" % len(all_units))
        for u in all_units:
            latent_vecs.append(mmd_group.latent_vecs[u])
        latent_vecs = keep_one_length_per_traj(hs=latent_vecs)

    else:
        groups = [g for g in mmd_group.groups if all(
            [key in g for key in factors_request]) and len(g) == len(factors_request)]
        #logging.warn("%d groups to return" % len(groups))
        latent_vecs = keep_one_length_per_traj(hs=[
            mmd_group._concat_equal_length_latent_dfs(
                units=mmd_group.get_units_for_condition(g),
                n_samples_per_unit=min([mmd_group.latent_vecs[u].n.nunique()
                                       for u in mmd_group.get_units_for_condition(g)])
            ) for g in groups])

    latent_vecs = pd.concat(latent_vecs, axis=0)
    latent_vecs = pd.merge(latent_vecs,
                           mmd_group.latent_df[["traj_ID", "L"] + cols_to_keep],
                           left_index=True,
                           right_on=["traj_ID", "L"],
                           how="left")
    for i, point in enumerate(points):
        latent_vecs["distance_%d" % i] = np.linalg.norm(
            latent_vecs[["U_1", "U_2"]].values - np.array([[point["x"], point["y"]]]), axis=1)
    latent_vecs["min_dist"] = latent_vecs[["distance_%d" %
                                           i for i in range(len(points))]].min(axis=1)
    latent_vecs["zone"] = -1
    for i, point in enumerate(points):
        zone = (latent_vecs["distance_%d" % i] < zone_radius) & (
            latent_vecs["distance_%d" % i] == latent_vecs["min_dist"])
        latent_vecs.loc[zone, "zone"] = i
        latent_vecs.loc[zone, "point_label"] = point["longlabel"] #"P" + (latent_vecs["zone"]+1).astype(str)

    results["region_proportion"] = {}
    for i, point in enumerate(points):
        results["region_proportion"]["P%d" % (i+1)] = {"group": [], "proportion": []}
        for group, df in latent_vecs.groupby(factors_request):
            group_dict = {}
            if isinstance(group, str):
                group = [group]
            for key, value in zip(factors_request, group):
                group_dict[key] = value
            label = describe_group(group_dict, mmd_group.hierarchy) if "file" not in factors_request else group[0].replace(
                job_folder+"/uploaded_data/", "")
            results["region_proportion"]["P%d" % (i+1)]["group"].append(label)
            prop = (df["zone"] == i).mean()
            results["region_proportion"]["P%d" % (i+1)]["proportion"].append(prop)

    return jsonify(results)


@app.route("/api/points_data/<job_id>", methods=["POST"])
def points_data(job_id):
    job_id = int(job_id)
    check_that_job_can_be_accessed(job_id)
    tss, mmd_group, mmd_units, mds = get_job_objects(job_id)
    job_folder = get_job_folder(job_id)

    points = json.loads(request.form["points"])
    zone_radius = 1.5

    cols_to_keep = mmd_group.group_by_keys + \
        ["file"] + ["alpha", "best_model", "log_D", "est_sigma", "n_points"]

    results = {}

    # D'abord, pour les métriques par point, on garde autant de trajectoires de chaque fichier

    all_units = mmd_group.get_units_for_condition({})
    min_n = min([mmd_group.latent_vecs[u].n.nunique() for u in all_units])
    hs, units = mmd_group._concat_equal_length_latent_dfs(
        units=all_units,
        n_samples_per_unit=min_n, return_units=True)
    hs["unit"] = units
    latent_vecs = pd.merge(hs.set_index(["traj_ID", "L"]),
                           mmd_group.latent_df[["traj_ID", "L"] + cols_to_keep],
                           left_index=True,
                           right_on=["traj_ID", "L"],
                           how="left")

    for i, point in enumerate(points):
        latent_vecs["distance_%d" % i] = np.linalg.norm(
            latent_vecs[["U_1", "U_2"]].values - np.array([[point["x"], point["y"]]]), axis=1)
    latent_vecs["min_dist"] = latent_vecs[["distance_%d" %
                                           i for i in range(len(points))]].min(axis=1)
    latent_vecs["zone"] = -1
    latent_vecs["point_label"] = ""
    for i, point in enumerate(points):
        zone = (latent_vecs["distance_%d" % i] < zone_radius) & (
            latent_vecs["distance_%d" % i] == latent_vecs["min_dist"])
        latent_vecs.loc[zone, "zone"] = point['index']
        latent_vecs.loc[zone, "point_label"] = point["label"]

    # Et on égalise les longueurs entre chaque région
    latent_vecs = pd.concat(
        keep_one_length_per_traj(
            hs=[df for zone,
                df in latent_vecs.loc[latent_vecs.zone >= 0].groupby("zone")]
        ),
        axis=0)

    results["metrics_dicts"] = group_values_by_several_factors(
        latent_vecs,
        cols_to_group_by=["point_label"],
        values=["alpha", "best_model", "log_D"])

    # Example trajectories

    results["trajectories"] = {"labels": [], "trajectories": []}
    mean_log_D = np.quantile(mmd_group.latent_df.groupby(
        "traj_ID")[["log_D"]].mean().iloc[:, 0], 0.8)
    mean_D = np.power(10., mean_log_D)
    DT = pd.read_csv(os.path.join(job_folder, "index.csv"))["DT"].max()
    mean_step = np.sqrt(2*mean_D*DT)
    logging.critical(mmd_group.latent_df.columns)
    mean_traj_length = mmd_group.latent_df.groupby(
        "traj_ID")["L"].median().median()
    scale = 1.5*mean_step*np.sqrt(mean_traj_length)

    coord_cols = mmd_group.tracksets.coord_cols
    dim = len(coord_cols)
    # TODO: adapter ici à la 3D
    # TODO: ensuite, faire les différents onglets de la partie résultats

    for _, point in enumerate(points):
        trajs = mmd_group.find_closest_trajectories(
            # ici, x, y c'est les coordonnées dans l'espace latent, donc toujours en 2D
            np.array([point[c] for c in ["x", "y"]], dtype=float), n_trajs=9, max_dist=zone_radius)
        L = 3
        for i in range(len(trajs)):
            traj = trajs[i]
            traj = traj - np.mean(traj, axis=0)
            traj[:, :2] = PCA().fit_transform(traj[:, :2])
            if dim == 2:
                traj = traj @ np.array([[1.0, 1.0], [-1.0, 1.0], ]) / np.sqrt(2)
            else:
                traj = traj @ np.array([[1.0, 1.0, 0.],
                                       [-1.0, 1.0, 0.], [0., 0., 1.]]) / np.sqrt(3)
            trajs[i] = traj
            trajs[i][:, 0] += (i % L)*scale
            trajs[i][:, 1] += (i // L)*scale
        results["trajectories"]["labels"].append(point["label"])

        results["trajectories"]["trajectories"].append(
            [[t[:, d].tolist() for d in range(dim)] for t in trajs])
    results["scale"] = scale

    return jsonify(results)


@app.route('/api/job_results/<job_id>/<type>', methods=["GET", "POST"])
def job_results(job_id, type):
    """
    Returns results read from a job's folder.
    The job is identified by its ID. If it isn't yet over, this returns a 404 error.

    Args:
        job_id (int): ID of the job in which we're interested
        fake (bool): Whether to return fake data or actual data

    Returns:
        _type_: JSON-ized results
    """
    job_id = int(job_id)

    # Look for a job with this ID
    job = Job.query.filter_by(id=job_id, status=Job.SUCCESS).first_or_404(
        "Job with ID %s does not exist, is not over, or hasn't run well" % job_id)
    session_id = "session:%s" % session.sid
    if not job.public and not job.session_id == session_id:
        print("QUERIED JOB SESSION : ", job.session_id)
        print("CURRENT SESSION : ", session_id)
        raise BaseException("This job is not linked to this session !")

    job_id = job.id
    job_folder = get_job_folder(job_id)

    #print("Job %d is done" % job_id)
    #print("Reading in its folder : %s" % job_folder)

    #tss, mmd_group, mmd_unit, mds = get_job_objects(job_id)

    if type == "trad":
        # Trad comme traditionnel
        # with open(os.path.join(job_folder, "values_grouped_by_factors.json"), 'r') as f:
        #    trad_dict = json.load(f)
        results = {}

        results["factors"] = _get_job_factors(job_id)
        if "factors" in request.form:
            factors_to_group_by = json.loads(request.form["factors"])
            if len(factors_to_group_by) > 0:
                traj_level_df = pd.read_csv(os.path.join(
                    job_folder, "variables_by_trajectory.csv"), keep_default_na=False)
                traj_level_df["file"] = traj_level_df["file"].str.replace(
                    job_folder + "/uploaded_data/", "")
                traj_level_df[factors_to_group_by] = traj_level_df[factors_to_group_by].astype(
                    str)

                results["factor_dict"] = group_values_by_several_factors(
                    traj_level_df,
                    cols_to_group_by=factors_to_group_by,
                    values=["alpha", "best_model", "log_D", "est_sigma", "n_points"])
                total_counts, detailed_counts = get_trajectory_counts(
                    job_id, factors_to_group_by)
                results["total_counts"] = total_counts
                results["detailed_counts"] = detailed_counts

        return jsonify(results)

    elif type == "latent":
        with open(os.path.join(job_folder, "latent_dict.json"), 'r') as f:
            latent_dict = json.load(f)
        return jsonify(latent_dict)

    elif type == "mds_conditions":
        tss, mmd_group, mmd_unit, mds = get_job_objects(job_id)
        if "factors" in request.form:
            request_factors: List[str] = json.loads(request.form["factors"])
            # .to_dict(orient="records")

            hierarchy = dict(mmd_group.hierarchy)
            hierarchy["file"] = max(hierarchy.values()) + 1

            color_options = []
            request_max_level = max([hierarchy[rf] for rf in request_factors])
            for factors in _get_job_factors(job_id):
                max_level = max([hierarchy[f] for f in factors])
                if max_level >= request_max_level:
                    if all([f in request_factors for f in factors]):
                        color_options.append(factors)
                else:
                    color_options.append(factors)

            f_name = name_factors_list(request_factors)
            mds_: MDSAnalysis = mds[f_name]

            columns = request_factors
            for c in color_options:
                columns = columns + c
            columns = list(set(columns))

            mds_df = mds_.unit_info[columns + ["X_1", "X_2"]].copy()
            mds_df[columns] = mds_df[columns].astype(str)
            if "file" in columns:
                mds_df["file"] = mds_df["file"].str.split(
                    "/", expand=True).iloc[:, 5:].astype(str).agg("/".join, axis=1)

            mds_dicts = []

            for f in color_options:
                color_mds_dict = {}
                for group, df in mds_df.groupby(f):
                    group_dict = {}
                    group_ = [group] if isinstance(group, str) else group
                    for factor, value in zip(f, group_):
                        group_dict[factor] = value
                    group_name = describe_group(group_dict, hierarchy)
                    df_ = df.copy()
                    name_cols = [c for c in df.columns if c not in f + ["X_1", "X_2"]]
                    if len(name_cols) == 0:
                        name_cols = f
                    df_["name"] = df[name_cols].astype(str).agg(" / ".join, axis=1)
                    color_mds_dict[group_name] = df_.to_dict(orient="records")
                mds_dicts.append(color_mds_dict)

            latent_vignettes_size = np.sqrt(mds_df[["X_1", "X_2"]].var(
                axis=0).mean()) / np.sqrt(mds_df.shape[0])

            hs = []
            # D'abord on collecte tous les vecteurs latents, avec plusieurs longueurs par trajectoire
            for i, (g, df) in enumerate(mds_df.groupby(request_factors)):
                if "file" in request_factors:
                    units = [unit_key for unit_key, unit in mmd_group.units.items(
                    ) if unit["file"].replace(job_folder + "/uploaded_data/", "") == g]
                    assert len(units) == 1, "%d units found" % len(units)
                    u = int(units[0])
                    hs.append(mmd_group.latent_vecs[u])
                else:
                    group = {}
                    for key, value in zip(request_factors, g):
                        group[key] = value
                    hs.append(mmd_group.sample_latent_vecs_for_cond(group))
            # Puis on ne garde qu'une seule longueur par trajectoire, pour égaliser les histogrammes de longueurs
            hs = keep_one_length_per_traj(hs, cols_to_keep=mmd_group.latent_cols)

            # Et on calcule les densités à partir de ces sélections.
            densities = []
            for h, (g, df) in zip(hs, mds_df.groupby(request_factors)):
                xx, yy, _, w1, _ = witness_function(
                    h.values, h.values, lims=mmd_group.lims, n_points=20)
                x_offset = df["X_1"].iloc[0] - latent_vignettes_size/2
                y_offset = df["X_2"].iloc[0] - latent_vignettes_size/2
                densities.append(
                    {"density": w1.tolist(), "x_offset": x_offset, "y_offset": y_offset})

            xx = xx - xx[0, 0]
            yy = yy - yy[0, 0]
            xx = latent_vignettes_size * xx / xx[-1, -1]
            yy = latent_vignettes_size * yy / yy[-1, -1]

            return jsonify({"mds_dicts": mds_dicts, "color_options": color_options, "densities": densities, "x": xx.tolist(), "y": yy.tolist()})
        else:
            factors = [f for f in _get_job_factors(
                job_id) if name_factors_list(f) in mmd_unit]
            return jsonify({"factors": factors})

    elif type == "mmd_by_group":
        mmd_by_group_dict = {}

        tss, mmd_group, mmd_unit, mds = get_job_objects(job_id)
        if "factors" in request.form:
            # Then this is a query for a comparison matrix
            request_factors = json.loads(request.form["factors"])

            indices = []
            groups = []

            compared_groups = [g for g in mmd_group.groups if any(
                [g in [g1, g2] for g1, g2 in mmd_group.comparisons])]

            for index, group in sort_groups(mmd_group.groups, mmd_group.hierarchy):
                if set(group.keys()) == set(request_factors) and group in compared_groups:
                    # logging.warn(group)
                    indices.append(index)
                    groups.append(group)

            comp_matrix = np.ndarray((len(groups), len(groups)),
                                     dtype=int)
            comp_matrix[:, :] = -1
            comp_matrix = comp_matrix.tolist()

            for i, comparisons in enumerate(mmd_group.comparisons):
                g1, g2 = comparisons

                if g1 in groups and g2 in groups:
                    #logging.warn("Adding %s - %s" % (describe_group(g1, mmd_group.hierarchy), describe_group(g2, mmd_group.hierarchy)))
                    j, k = groups.index(g1), groups.index(g2)
                    comp_matrix[j][k] = i
                    comp_matrix[k][j] = i

            p_values = mmd_group._p_val[np.ix_(indices, indices)]
            mmd_by_group_dict["p_val"] = p_values.tolist()
            mmd_by_group_dict["group_labels"] = [
                describe_group(g, mmd_group.hierarchy) for g in groups]
            mmd_by_group_dict["group_indices"] = indices
            mmd_by_group_dict["comp_matrix"] = comp_matrix

            #del mmd_by_group_dict["witness_function_dict"]
            #del mmd_by_group_dict["densities"]
            #print([k for k in mmd_by_group_dict.keys()])

        else:
            # This is a first query
            mmd_by_group_dict["factors"] = _get_job_factors(job_id)
            xx, yy = get_latent_grid(lims=mmd_group.lims, n_points=N_LATENT_GRID_POINTS)
            mmd_by_group_dict["x"] = xx.tolist()
            mmd_by_group_dict["y"] = yy.tolist()
            mmd_by_group_dict["scale"] = mmd_group.latent_df.groupby(
                ["n", "L"])[mmd_group.coord_cols].std().mean().values[0]

        return jsonify(mmd_by_group_dict)

    elif type == "zip":

        return send_file(os.path.join(job_folder, "results_archives.zip"),
                         attachment_filename="results_archives.zip",
                         as_attachment=True)

    else:
        raise NotImplementedError(" Unknown type %s" % type)


@app.route("/api/get_witness_function/<job_id>", methods=["POST"])
def get_witness_function(job_id):
    job_id = int(job_id)
    comparison_index, i, j = json.loads(request.form["comparison_index"])
    # print(group_id)

    assert check_that_job_can_be_accessed(job_id)

    job_folder = get_job_folder(job_id)

    with open(os.path.join(job_folder, "witness_function_dict.json"), 'r') as f:
        witness_function_dict = json.load(
            f, parse_float=lambda s: np.around(float(s), 5))
    with open(os.path.join(job_folder, "stats_dict.json"), 'r') as f:
        stats_dict = json.load(
            f, parse_float=lambda s: np.around(float(s), 5))
    with open(os.path.join(job_folder, "densities_dict.json"), 'r') as f:
        densities_dict = json.load(
            f, parse_float=lambda s: np.around(float(s), 5))

    # (cond1 vs cond2) -> {density1, density2, w}
    # (i,j) -> string, "cond1 vs cond2"
    w = witness_function_dict[str(comparison_index)]
    w1 = densities_dict[str(i)]
    w2 = densities_dict[str(j)]
    stats = stats_dict[str(comparison_index)]
    witness_function = [w, w1, w2, stats]

    return jsonify(witness_function)


def get_job_objects(job_id):

    job_folder = get_job_folder(job_id)
    index_df = pd.read_csv(os.path.join(job_folder, "final_index.csv"),
                           index_col=0, keep_default_na=False)
    files = index_df["file"].tolist()
    tss = TrackSets.from_files(files, root_folder=job_folder, index_df=index_df)
    mmd_group = MMDInterGroupAnalysis.from_params(
        export_folder=job_folder,
        run_name="example",
        tss=tss,
    )

    # list folders in MDS dir
    mmd_unit = {}
    mds = {}
    for factors_dir, _, _ in os.walk(os.path.join(job_folder, "MDS")):
        factors_name = factors_dir.split(os.path.sep)[-1]
        if factors_name == "MDS":
            continue
        mmd_unit[factors_name] = MMDInterUnitAnalysis.from_params(
            export_folder=job_folder,
            run_name=factors_name,
            tss=tss
        )
        mds[factors_name] = MDSAnalysis.from_params(
            export_folder=job_folder,
            run_name=factors_name,
            tss=tss
        )
    return tss, mmd_group, mmd_unit, mds


def check_that_job_can_be_accessed(job_id):
    # Look for a job with this ID
    job = Job.query.filter_by(id=job_id, status=Job.SUCCESS).first_or_404(
        "Job with ID %s does not exist, is not over, or hasn't run well" % job_id)
    session_id = "session:%s" % session.sid
    if not job.public and not job.session_id == session_id:
        print("QUERIED JOB SESSION : ", job.session_id)
        print("CURRENT SESSION : ", session_id)
        raise BaseException("This job is not linked to this session !")
    return True


def get_trajectory_counts(job_id: int, factors_to_group_by: List):

    #print("Looking around ")
    #print(x, y)
    #print("At less than %.2f" % r)

    assert check_that_job_can_be_accessed(job_id)
    job_folder = get_job_folder(job_id=job_id)
    tss, mmd_group, mmd_unit, mds = get_job_objects(job_id)
    #file_cols = mmd_group.latent_df["file"].str.split("/", expand=True)
    mmd_group.latent_df["file"] = mmd_group.latent_df["file"].str.replace(
        job_folder+"/uploaded_data/", "")
    total_counts, detailed_counts = df_to_trajectory_counts(
        mmd_group.latent_df, factors_to_group_by)

    return total_counts, detailed_counts


def coord_cols(loc_df: pd.DataFrame) -> List[str]:
    return ["x", "y"] if "z" not in loc_df.columns else ["x", "y", "z"]


@app.route("/api/example_trajectories", methods=["POST"])
def example_trajectories():
    job_id = int(json.loads(request.form["job_id"]))
    x, y = json.loads(request.form["xy"])
    # Typical size (in um) of a trajectory
    scale = 1.5*float(json.loads(request.form["scale"]))
    # Radius in which to search around (x,y), in the latent space
    r = float(request.form["r"])

    assert check_that_job_can_be_accessed(job_id)
    tss, mmd_group, mmd_unit, mds = get_job_objects(job_id)

    trajs = mmd_group.find_closest_trajectories(
        np.array([x, y], dtype=float), n_trajs=9, max_dist=r)
    L = 3
    for i in range(len(trajs)):
        traj = trajs[i]
        traj = traj - np.mean(traj, axis=0)
        traj = PCA().fit_transform(traj)
        traj = traj @ np.array([[1.0, 1.0], [-1.0, 1.0]]) / np.sqrt(2)
        trajs[i] = traj
        trajs[i][:, 0] += (i % L)*scale
        trajs[i][:, 1] += (i // L)*scale

    return jsonify([[t[:, 0].tolist(), t[:, 1].tolist()] for t in trajs])


@ app.route('/api/get_session', methods=["GET"])
def print_session():
    """Returns the current session's ID

    Returns:
        _type_: _description_
    """
    logging.debug("get session")
    return jsonify({"sid": session.sid,
                    "id": _get_session().id,
                    })


@ app.route('/api/get_session_files', methods=["GET"])
def get_session_files():
    """
    Current version : returns the ID of one file associated to the session
    Future : list of all files associated to the session
    Returns:
        _type_: _description_
    """
    s = _get_session()
    files = s.files

    res = {"file_list": -1}
    file_list = []
    for f in files:
        file_list.append(f.path)
    res["file_list"] = file_list
    return jsonify(file_list)


@ app.route('/api/upload_file', methods=["POST"])
def upload_files():
    """
    Uploads one file in the session's storage folder
    1) actually transfer the file
    2) add file to the database, and link it to the current session

    Returns:
        _type_: JSON with information about uploaded file
    """
    # Get folder for current session
    session_folder = get_session_folder()

    file = request.files.getlist("file")[0]

    # Upload files to session folder
    # Name of the file on the user's computer, minus the first folder's name because we don't want it
    original_file_name = json.loads(request.form["destination_filename"])
    is3D = json.loads(request.form["is3D"])
    logging.critical("is3D : %s" % is3D)
    upload_info = None  # dict to return

    infos = None  # fields infered by the front from the file hieararchy
    if "info" in request.form:
        infos = json.loads(request.form["info"])

    # On passe par le temporary file pour calculer le hash
    temporary_name = "".join(np.random.choice(
        ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T"], size=10, replace=True))
    temporary_destination = os.path.join(session_folder, temporary_name)
    file.save(temporary_destination)
    hash_key = get_hash_key(temporary_destination)
    # On obtient le chemin final à partir du hash
    final_destination = os.path.join(session_folder, hash_key + ".locs")
    # Check if the file is valid
    try:
        df, DT = check_file_and_read(
            temporary_destination,
            short_version=True,
            check_min_trajs=10,
            reject_trajs_shorter_than=7,
            cut_trajs_longer_than=80,
            is3D=is3D)
        if isinstance(df, pd.DataFrame):
            # Keep client-side path for request response
            upload_info = {"filename": original_file_name, "success": True,
                           "detail": "File successfully read"}
            # We store the formatted file already
            df.to_csv(temporary_destination,index=False)
        else:
            # Keep client-side path for request response
            upload_info = {"filename": original_file_name,
                           "success": False, "detail": DT}
    except Exception as e:
        logging.error(e)
        upload_info = {"filename": original_file_name,
                       "success": False, "detail": e.__str__}

    # Add files into Files database table
    session_id = "session:%s" % session.sid

    file_size = os.stat(temporary_destination).st_size

    # Delete eventual files pre-existing with the same hash (to prevent duplicates)

    if upload_info["success"]:
        previous_files = File.query.filter_by(
            session_id=session_id,
            hash_key=hash_key,
        ).all()
        if len(previous_files) > 0:
            logging.warn("Found %d files with the same hash" % len(previous_files))
        for f in previous_files:
            db.session.delete(f)
            os.remove(os.path.join(session_folder, f.path))
        db.session.commit()

        f = File(path=final_destination,
                 session_id=session_id,
                 size=file_size,
                 hash_key=hash_key,
                 is3D=is3D,
                 name=original_file_name)

        db.session.add(f)
        db.session.commit()

        # On renome le fichier, qui a auparavant été sauvegardé sous un format facilement lisible (csv avec les bons noms de colonnes)
        os.rename(temporary_destination, final_destination)

        # S'il y a des infos à rajouter, on vérifie que le type de propriété n'existe pas déjà
        # Puis on rajoute la propriété
        if infos is None:
            infos = {}
        infos.update({"blank_column_123": "dummy_value", "DT": DT, "scale": 1.})

        for property, value in infos.items():
            p = SessionPropertyType(name=property, session_id=session_id)
            try:
                # On essaie de l'ajouter. Si elle existe déjà, on rollback
                db.session.add(p)
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
                p = SessionPropertyType.query.filter_by(
                    name=property, session_id=session_id).first_or_404()

            f = File.query.filter_by(session_id=session_id,
                                     path=final_destination).first_or_404("Could not find file with session %s and path %s" % (session_id, final_destination
                                                                                                                               ))
            file_property = FileProperty(
                file_id=f.id, property_type_id=p.id, value=value)
            try:
                db.session.add(file_property)
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

        session_properties = SessionPropertyType.query.filter_by(
            session_id=session_id).all()

        for prop_type in session_properties:
            f = File.query.filter_by(session_id=session_id,
                                     path=final_destination).first_or_404(
                                         "Could not find file with session %s and path %s" % (session_id, final_destination))
            try:
                # On essaie de l'ajouter. Si elle existe déjà, on rollback
                prop_value = FileProperty(
                    file_id=f.id, property_type_id=prop_type.id, value="")
                db.session.add(prop_value)
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
    return jsonify(upload_info)


@ app.route('/api/add_file_to_job/<file_id>', methods=["GET"])
def add_files_to_job(file_id):
    """Associates one file (designated by file_id) to the active job

    Args:
        file_id (_type_): _description_

    Returns:
        _type_: _description_
    """
    # Get the file which has this ID and is in this session
    session_id = "session:%s" % session.sid
    f = File.query.filter_by(id=file_id, session_id=session_id).first_or_404(
        description="No file with ID %s in session %s" % (file_id, session_id))
    # Get the active job
    active_job_id = get_active_job()
    active_job = Job.query.filter_by(id=active_job_id).first()
    # Add the link if it does not already exist
    if f not in active_job.files:
        active_job.files.append(f)
    db.session.commit()
    return jsonify({"file_id": f.id, "job": active_job.id, "n_files_in_job": len(active_job.files)})


@ app.route('/api/get_file_locs/<file_id>/<job_id>', methods=["GET", "POST"])
def get_file_locs(file_id, job_id):
    """
    returns the localisations of a trajectory file
    There are arguments in the request.form :
    - points: points in the latent space, so as to associate to each trajectory its distance to each of these points
    - is3D: indicates whether trajectories are in 3D or not

    Args:
        file_id (string): name of the file stored in the session folder

    Returns:
        dict: dict of x,y positions
    """
    # Get the file which has this ID and is in this session

    job_id = int(job_id)
    session_id = "session:%s" % session.sid

    if job_id == 0:
        f = File.query.filter_by(session_id=session_id, id=file_id).first_or_404(
            description="No file with id %s in session %s" % (file_id, session_id))
    else:
        f = File.query.filter_by(id=file_id).first_or_404(
            description="No file with id %s" % (file_id))

    file_name = f.name
    is3D = f.is3D
    #is3D = False if "is3D" not in request.form else json.loads(request.form["is3D"])

    if job_id == 0:
        # Si job_id == 0, il s'agit des locs d'un fichier non encore processé
        # Donc on ne regarde pas à quel job il appartient
        file_path = os.path.join(get_session_folder(), f.path)
        loc_df, DT = check_file_and_read(file_path, is3D=is3D)
        # logging.critical(loc_df.head())
        points = []

        # Keep only trajectories longer than 7 points
        trajs_length = loc_df.n.value_counts()
        good_trajs = trajs_length.loc[trajs_length >= 7].index.tolist()
        loc_df = loc_df.loc[loc_df.n.isin(good_trajs)]

        # Get scale and DT
        scale_prop_type = SessionPropertyType.query.filter_by(
            name="scale", session_id=session_id).first_or_404("Property type not found : %s with session %s" % ("scale", session_id))
        scale = FileProperty.query.filter_by(
            file_id=f.id, property_type_id=scale_prop_type.id).first()
        DT_prop_type = SessionPropertyType.query.filter_by(
            name="DT", session_id=session_id).first_or_404("Property type not found : %s with session %s" % ("DT", session_id))
        DT = FileProperty.query.filter_by(
            file_id=f.id, property_type_id=DT_prop_type.id).first()

        loc_df[coord_cols(loc_df)] *= float(scale.value)
        loc_df["t"] = loc_df["frame"] * float(DT.value)
        loc_df["point"] = 0
        loc_df = loc_df[coord_cols(loc_df) + ["n", "t", "point"]]
    else:
        # Sinon, il faut s'assurer que le fichier fait bien partie du job, et qu'on a le droit de le voir
        assert check_that_job_can_be_accessed(job_id)
        points = json.loads(request.form["points"])

        # We get the index table of the job to make the link between file id and file name in job.
        index_df = get_index_table(job_id=job_id)
        # We then recover the path of the file in the job folder from its name
        file_name = index_df.loc[index_df.file_id == f.id].file_name.iloc[0]
        file_path = get_file_path_in_job(job_id, file_name)
        assert os.path.exists(file_path)
        # From this, we select the appropriate rows
        loc_df = pd.read_csv(os.path.join(get_job_folder(
            job_id), "variables_and_trajectories.csv"), index_col=0)
        loc_df = loc_df[loc_df["file"] == file_path]
        loc_df["best_model"] = loc_df["best_model"].replace(ALL_MODELS)
        point_dict = {}
        point_colors = {}
        for i, point in enumerate(points):
            loc_df["distance_%s" % point["label"]] = np.linalg.norm(
                loc_df[["U_1", "U_2"]].values - np.array([[point["x"], point["y"]]]), axis=1)
            point_dict["distance_%s" % point["label"]] = i+1
            point_colors[i+1] = point["color"]
        point_colors[0] = "#CCCCCC"
        if len(points) > 0:
            loc_df["point"] = loc_df[["distance_%s" % p["label"]
                                      for p in points]].idxmin(axis=1).replace(point_dict)
            dist_too_long = loc_df[["distance_%s" % p["label"]
                                    for p in points]].min(axis=1) > 1.5
            loc_df.loc[dist_too_long, "point"] = 0
        else:
            loc_df["point"] = 0
        loc_df = loc_df[coord_cols(loc_df) + ["n", "t", "point"] +
                        ["log_D", "n_points", "alpha", "best_model"]]
        # logging.warn(loc_df["point"].value_counts())

    M = loc_df[coord_cols(loc_df)].max()
    m = loc_df[coord_cols(loc_df)].min()
    center = (M+m) / 2
    R = ((M-m).max() / 2)*1.02

    trajs_to_pick = np.random.choice(loc_df.n.unique().tolist(),
        size=min(loc_df.n.nunique(), MAX_TRAJECTORIES), replace=False)
    loc_df = loc_df.loc[loc_df.n.isin(trajs_to_pick)]

    colormaps = {
        "t": viridis,
        "log_D": viridis,
        "n_points": viridis,
        "alpha": coolwarm,
        "best_model": tab10}
    if job_id > 0:
        colormaps["point"] = lambda f: point_colors[int(f*(len(points)))]

    norms = {
        "t": Normalize(vmin=loc_df.t.min(), vmax=loc_df.t.max()),
        "log_D": Normalize(vmin=-2., vmax=1.),
        "alpha": TwoSlopeNorm(vmin=.1, vmax=1.9, vcenter=1.),
        "point": lambda x: x / (max([len(points), 1]))
    }

    value_range = {
        "t": np.linspace(loc_df.t.min(), loc_df.t.max(), num=100, endpoint=True),
        "log_D": np.linspace(-2, 1, num=100, endpoint=True),
        "alpha": np.linspace(0.1, 1.9, endpoint=True, num=100),
        "best_model": np.linspace(0, 5, endpoint=False, num=150),
        "point": np.linspace(0, len(points)+1, endpoint=False, num=150),
    }
    if "n_points" in loc_df.columns:
        norms["n_points"] = LogNorm(
            vmin=loc_df.n_points.min(), vmax=loc_df.n_points.max())
        value_range["n_points"] = np.linspace(
            loc_df.n_points.min(), loc_df.n_points.max(), num=100, endpoint=True)
    if "best_model" in loc_df.columns:
        norms["best_model"] = lambda x: x / 10.

    trajs_list = []
    for point, loc_df_ in loc_df.groupby("point"):
        for n, traj in loc_df_.sort_values("t", ascending=True).groupby("n"):
            traj_dict = traj[coord_cols(loc_df) + ["t"]].to_dict("list")
            traj_dict["n"] = n
            traj_dict["colors"] = {}
            for c, cmap in colormaps.items():
                if c not in traj.columns:
                    continue
                traj_value = np.nanmean(traj[c].values)
                if np.isnan(traj_value):
                    logging.warn("Value for %s is null" % c)
                #assert not np.isnan(traj_value), "Value for %s is null" % c
                rgba = cmap(norms[c](traj_value))
                hex = to_hex(rgba, keep_alpha=False)
                traj_dict["colors"][c] = hex
            # if job_id > 0:
            #    traj_dict["colors"]["point"] = point_colors[traj["point"].values[0]]
            trajs_list.append(traj_dict)

    def to_rgb_plotly(color):
        return 'rgb(%d,%d,%d)' % (color[0]*255, color[1]*255, color[2]*255)

    color_scales = {}

    for c, cmap in colormaps.items():
        if c not in traj.columns:
            continue
        color_scales[c] = {
            "cmin": value_range[c][0],
            "cmax": value_range[c][-1],
            "scale": [[i / (len(value_range[c]) - 1),
                      to_rgb_plotly(cmap(norms[c](v))) if not isinstance(cmap(norms[c](v)), str) else cmap(norms[c](v))] for i, v in enumerate(value_range[c])
                      ]
        }
    """
    if job_id > 0 and len(point) > 0:
        color_scales["point"] = {
            "cmin": value_range["point"][0],
            "cmax": value_range["point"][-1],
            "scale": [[i / (len(value_range[c]) - 1),
                      to_rgb_plotly(cmap(norms[c](v)))] for i, v in enumerate(value_range[c])
                      ]
        }
    """

    ticks = {"best_model": {
        "positions": [0.5, 1.5, 2.5, 3.5, 4.5],
        "labels": [model_name for model_name, index in ALL_MODELS.items()]
    },
        "point": {
        "positions": (np.arange(len(points) + 1) + 0.5).tolist(),
        "labels": [""] + [p["label"] for p in points]
    }}

    logging.debug("Returning %d trajs for file %s" % (len(trajs_list), f.name))

    return jsonify({"trajs": trajs_list,
                    "time_range": (loc_df.t.min(), loc_df.t.max()),
                    "color_scales": color_scales,
                    "ticks": ticks,
                    "file_name": file_name,
                    "range": (center.loc["x"] - R, center.loc["x"] + R, center.loc["y"] - R, center.loc["y"] + R)
                    # TODO: update ça pour ajouter z dans le range
                    },)


def index_columns_to_be_returned(df):
    return [c for c in df.columns if c not in ["file", "file_name", "include", "file_id", "file_path"]]


@app.before_request
def set_domain_session():
    try:
        session['domain'] = request.headers['Host']
    except KeyError:
        logging.warning("Could not set session domain")


@ app.route('/api/get_index_table/<job_id>', methods=["GET", "POST"])
def get_index_table_(job_id):
    """
    returns index table associated to the active session
    """
    # TODO: rajouter ici la 3D
    job_id = int(job_id)
    logging.critical("Getting index table")
    if job_id == 0:
        is3D = json.loads(request.form["is3D"])
        index_df = get_index_table(is3D=is3D)
    else:
        index_df = get_index_table(job_id=job_id)

    colnames = index_columns_to_be_returned(index_df)
    response = {"rowdata": list(index_df.to_dict(orient="index").values()),
                "colnames": colnames}

    # default_r = app.make_default_options_response()
    response = jsonify(response)
    return response


@ app.route('/api/update_index_table', methods=["POST"])
def update_index_table_():
    """updates index table associated to the active session

    """

    session_id = "session:%s" % session.sid

    columns_that_changed = []

    if json.loads(request.form["action"]) == "newCol":
        f = File.query.filter_by(session_id=session_id).first()
        if f is not None:
            new_prop = SessionPropertyType(
                name=json.loads(request.form["col_name"]), session_id=session_id)
            columns_that_changed.append(json.loads(request.form["col_name"]))
            db.session.add(new_prop)
            db.session.commit()

            first_prop_value = FileProperty(
                file_id=f.id, property_type_id=new_prop.id, value="")
            db.session.add(first_prop_value)
            db.session.commit()
            # index_df[request.form["col_name"]] = "condition"
        else:
            logging.critical("Adding column without file doesn't make sense")
    elif json.loads(request.form["action"]) == "editCol":
        old, new = json.loads(request.form["col_name"])
        col = SessionPropertyType.query.filter_by(
            session_id=session_id, name=old).first_or_404()
        col.name = new
        db.session.commit()
    elif json.loads(request.form["action"]) == "changeRow":
        row_values = json.loads(json.loads(request.form["row_value"]))
        # For some reason, need to be parsed twice...
        f = File.query.filter_by(session_id=session_id,
                                 id=row_values["file_id"]).first_or_404("File not found : %s with session %s" % (row_values["file_id"], session_id))
        for col_name, value in row_values.items():
            if col_name in ["file_id", "file_path"]:
                continue
            elif col_name == "file_name":
                f.name = value
            else:
                prop_type = SessionPropertyType.query.filter_by(
                    name=col_name, session_id=session_id).first_or_404("Property type not found : %s with session %s" % (col_name, session_id))
                prop = FileProperty.query.filter_by(
                    file_id=f.id, property_type_id=prop_type.id).first()
                if prop is not None:
                    if value != prop.value:
                        columns_that_changed.append(col_name)
                        prop.value = value
                else:
                    prop = FileProperty(file_id=f.id, value=value,
                                        property_type_id=prop_type.id)
                    db.session.add(prop)
                    columns_that_changed.append(col_name)
        db.session.commit()

    elif json.loads(request.form["action"]) == "delCol":
        logging.debug("Delete %s" % json.loads(request.form["col_name"]))
        prop_type = SessionPropertyType.query.filter_by(
            name=json.loads(request.form["col_name"]), session_id=session_id).first_or_404("Property %s not found in session %s" % (json.loads(request.form["col_name"]), session_id))
        logging.debug("Prop : %s" % prop_type)
        db.session.delete(prop_type)
        db.session.commit()

    elif json.loads(request.form["action"]) == "delAll":
        db.session.query(SessionPropertyType).filter_by(session_id=session_id).delete()
        files = File.query.filter_by(session_id=session_id).all()
        for f in files:
            os.remove(f.path)
            db.session.query(FileProperty).filter_by(file_id=f.id).delete()
        db.session.query(File).filter_by(session_id=session_id).delete()
        db.session.commit()
    else:
        logging.warn("Unknown action : %s" % json.loads(request.form["action"]))

    index_df = get_index_table(is3D=json.loads(request.form["is3D"]))
    colnames = index_columns_to_be_returned(index_df)
    response = {"rowdata": list(index_df.to_dict(orient="index").values()),
                "colnames": colnames, "changed_columns": columns_that_changed}
    # response = list(index_df.to_dict(orient="index").values())
    return jsonify(response)


@ app.route('/api/jobs', methods=["GET"])
def get_jobs():
    """
    Returns a JSON with information about the jobs linked to the current session

    Returns:
        _type_: _description_
    """
    jobs = get_session_jobs()
    #print("SESSION JOBS : ")
    res = {"jobs": []}
    for job in jobs:
        job_dict = {
            "id": job.id,
            "status": job.status,
            "create_time": job.create_time,
            "submit_time": job.submit_time,
            "start_time": job.start_time,
            "public": job.public,
            "name": job.name,
            "end_time": job.end_time}
        # print(job_dict)
        if job.status != -1:
            res["jobs"].append(job_dict)
    return jsonify(res["jobs"])


@ app.route('/api/submit_job', methods=["POST"])
def submit_job():
    """Submits the active job.
    Update its record in the DB.
    Send the task to celery workers.
    Returns information about the job and the task.

    Returns:
        _type_: _description_
    """
    try:
        job = Job.query.filter_by(
            id=get_active_job()).first_or_404("No active job")

        session_id = "session:%s" % session.sid
        logging.critical("Submitting job with session : %s" % session_id)
        index_df = get_index_table(is3D=bool(json.loads(request.form["is3D"])))
        selected_files = [int(f) for f in json.loads(request.form["selected_files"])]
        n_files_per_job.observe(len(selected_files))
        # logging.critical(index_df["file_name"].head())
        # logging.critical(selected_files)
        logging.debug("Selected files : %s" % selected_files)
        logging.debug(index_df)
        index_df = index_df.set_index("file_id").loc[selected_files].reset_index()
        logging.debug("Index df created")
        for _, row in index_df.iterrows():
            f = File.query.filter_by(id=row["file_id"]).first_or_404(
                "No file with id %s in session %s" % (row["file_id"], session_id))
            job.files.append(f)

        job.submit_time = datetime.datetime.utcnow()
        job.status = Job.QUEUED
        job.author = json.loads(request.form["author"])
        job.description = json.loads(request.form["description"])
        job.name = json.loads(request.form["title"])
        job.is3D = json.loads(request.form["is3D"])
        job_id = int(job.id)
        db.session.commit()

        logging.info("Starting job with %d files" % len(job.files))

        gb = index_df.groupby([index_df.file_name])
        # Adding suffixes to duplicate file names
        index_df.file_name = index_df.file_name + \
            ["(%d)" % a if a else "" for a in gb.cumcount()]
        assert index_df.file_name.nunique() == index_df.shape[0]
        job_folder = get_job_folder(job_id)
        index_df.to_csv(os.path.join(job_folder, "index.csv"), index=False)
        hierarchy = json.loads(request.form["hierarchy"])
        logging.critical("Hierarchy : %s" % hierarchy)
        json.dump(hierarchy, open(os.path.join(job_folder, "hierarchy.json"), "w"))

        is3D = json.loads(request.form["is3D"])

        INDEX_DF = index_df.copy()
        assert INDEX_DF.file_name.nunique() == INDEX_DF.shape[0]
        for i, row in INDEX_DF.iterrows():
            file_name = row["file_name"]
            path_in_session = row["file_path"]
            path_in_job = get_file_path_in_job(job_id=job_id, file_name=file_name)
            # Create the eventual subfolders
            os.makedirs(os.path.dirname(path_in_job), exist_ok=True)
            # Copy the file
            shutil.copy(path_in_session, path_in_job)
            loc_df, _ = check_file_and_read(path_in_job, is3D=is3D)
            assert loc_df is not None

            session_id = job.session_id
            f = File.query.filter_by(session_id=session_id, path=path_in_session).first_or_404(
                description="No file with path %s in session %s" % (path_in_session, session_id))
            scale_prop_type = SessionPropertyType.query.filter_by(
                name="scale", session_id=session_id).first_or_404("Property type not found : %s with session %s" % ("scale", session_id))
            scale = FileProperty.query.filter_by(
                file_id=f.id, property_type_id=scale_prop_type.id).first()
            DT_prop_type = SessionPropertyType.query.filter_by(
                name="DT", session_id=session_id).first_or_404("Property type not found : %s with session %s" % ("DT", session_id))
            DT = FileProperty.query.filter_by(
                file_id=f.id, property_type_id=DT_prop_type.id).first()

            loc_df[coord_cols(loc_df)] *= float(scale.value)
            loc_df["t"] = loc_df["frame"] * float(DT.value)

            loc_df.to_csv(path_in_job, index=False)

            INDEX_DF.loc[i, "file_name"] = path_in_job

        INDEX_DF = INDEX_DF.rename(columns={"file_name": "file"})
        INDEX_DF = INDEX_DF[[
            c for c in INDEX_DF.columns if c not in FILE_PROPERTY_COLUMNS]]

        # merge redundant columns
        INDEX_DF = remove_redundant_columns(INDEX_DF)

        INDEX_DF.to_csv(os.path.join(job_folder, "final_index.csv"))

        new_job_id = new_active_job()

        res = do_job.delay(job.id, session_folder=get_session_folder())
        result = jsonify(
            {"task_ID": res.id, "job_ID": job.id, "new_job_ID": new_job_id})
        job_status.state("submitted")
        return result

    except Exception as e:
        logging.error("Error when submitting job")
        logging.error(e)
        db.session.rollback()
        job = Job.query.filter_by(
            id=get_active_job()).first_or_404("No active job")
        new_job_id = new_active_job()
        job_status.state("failed_at_submission")
        return jsonify({"job_ID": job.id, "new_job_ID": new_job_id})


@ app.route('/api/new_active_job', methods=["GET"])
def new_active_job_():
    job_id = new_active_job()

    return jsonify({"job_ID": job_id})


@ app.route("/api/get_log/<job_id>")
def get_log(job_id):
    job_id = int(job_id)
    job = Job.query.filter_by(id=job_id).first_or_404(
        "Job with ID %s does not exist" % job_id)
    session_id = "session:%s" % session.sid
    if (not job.session_id == session_id) and (not job.public):
        raise BaseException(
            "This job is not linked to this session and isn't public !")

    job_folder = get_job_folder(job.id)

    job_log = os.path.join(job_folder, "output.log")

    log = ""
    if os.path.exists(job_log):
        f = open(job_log, mode="r")
        log = f.read()
        f.close()
    m = log

    return jsonify({"log": m})
