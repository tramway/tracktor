.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/webapp.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/webapp
    .. image:: https://readthedocs.org/projects/webapp/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://webapp.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/webapp/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/webapp
    .. image:: https://img.shields.io/pypi/v/webapp.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/webapp/
    .. image:: https://img.shields.io/conda/vn/conda-forge/webapp.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/webapp
    .. image:: https://pepy.tech/badge/webapp/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/webapp
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/webapp

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

======
webapp
======


    Tracktor is cool


.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.2.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
