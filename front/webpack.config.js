const webpack = require('webpack')
const Dotenv = require('dotenv-webpack');
// const dotenv = require("dotenv");
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// this will update the process.env with environment variables in .env file
// dotenv.config();

/* module.exports = {
    //...
    plugins: [
        new Dotenv()
    ],
    //...
    resolve: {
        fallback: { "os": false },
        fallback: { "os": false }
    }
} */



module.exports = {
    mode: "development",
    node: {
        fs: "empty"
    },
    entry: './src/index.js',
    plugins: [new MiniCssExtractPlugin(), new Dotenv()],
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'),
    },
    resolve: {
        fallback: {
            "os": require.resolve("os-browserify/browser"),
            "path": require.resolve("path-browserify"),
            "fs": false
        }
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            esModule: true,
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            esModule: true,
                            modules: {
                                mode: 'local',
                                exportLocalsConvention: 'camelCaseOnly',
                                namedExport: true,
                            },
                        },
                    },
                ]
            }
        ]
    }
};