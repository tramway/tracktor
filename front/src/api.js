import axios from 'axios'

var baseURL = process.env.REACT_APP_IS_LOCAL ? "http://dev.localhost:8080" : undefined

export default axios.create({
    withCredentials: true,
    baseURL: baseURL,
    headers: {
        //'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': 'x-requested-with'
    },
    transformResponse: [function (data) {
        // Do whatever you want to transform the data
        try {
            return JSON.parse(data.replace(/\bNaN\b/g, "null"))
        }
        catch (err) {
            try {
                return JSON.parse(data)
            }
            catch (err) {
                return data
            }
        }
    }],
});