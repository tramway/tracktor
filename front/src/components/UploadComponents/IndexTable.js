import React from "react"
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLongArrowAltDown, faLongArrowAltUp, faTimes, faBars, faTrashCan, faBinoculars, faPenToSquare } from '@fortawesome/free-solid-svg-icons'
import { help_dict } from '../TooltipValues'
import axios from "../../api"
import { Container, Button, ButtonGroup, Stack, Tooltip } from '@mui/material';
import AddCommentIcon from '@mui/icons-material/AddComment';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import PlayCircleOutlineIcon from '@mui/icons-material/PlayCircleOutline';
import JobDialog from "./JobDialog";

const FORBIDDEN_COL_NAMES = ["file", "DT", "scale", "file_name", "file_path", "file_id", "index"]

export class BtnCellRenderer extends React.Component {
  constructor(props) {
    super(props);
    this.btnClickedHandler = this.btnClickedHandler.bind(this);
  }
  btnClickedHandler() {
    this.props.clicked(this.props.data.file_id);
  }
  render() {
    return (
      <div>
        <Tooltip title={help_dict["binoculars"]}>
          <FontAwesomeIcon
            icon={faBinoculars}
            onClick={this.btnClickedHandler}
            style={{
              height: 16,
              marginRight: 4,
              "cursor": "pointer"
            }}
          />
        </Tooltip>
        {this.props.value}
      </div >
    )
  }
}

export class DeleteHeader extends React.Component {
  constructor(props) {
    super(props);
    this.onDeleteClicked = this.onDeleteClicked.bind(this)
    this.onEditClicked = this.onEditClicked.bind(this)
    this.state = {
      ascSort: 'inactive',
      descSort: 'inactive',
      noSort: 'inactive'
    };
  }

  onMenuClicked() {
    this.props.showColumnMenu(this.menuButton);
  }

  render() {
    let menu = null;
    if (this.props.enableMenu) {
      menu =
        <div ref={(menuButton) => {
          this.menuButton = menuButton;
        }}
          className="customHeaderMenuButton"
          style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
          onClick={this.onMenuClicked.bind(this)}>
          <FontAwesomeIcon icon={faBars} style={{ height: 10 }} />
        </div>;
    }

    let sort = null;
    if (this.props.enableSorting) {
      sort =
        <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
          <div onClick={this.onSortRequested.bind(this, 'asc')} onTouchEnd={this.onSortRequested.bind(this, 'asc')}
            style={{ "margin": 2 }}
            className={`customSortDownLabel ${this.state.ascSort} customHeaderButton`}>
            <FontAwesomeIcon icon={faLongArrowAltDown} />
          </div>
          <div onClick={this.onSortRequested.bind(this, 'desc')} onTouchEnd={this.onSortRequested.bind(this, 'desc')}
            style={{ "margin": 2 }}
            className={`customSortUpLabel ${this.state.descSort} customHeaderButton`}>
            <FontAwesomeIcon icon={faLongArrowAltUp} />
          </div>
          <div onClick={this.onSortRequested.bind(this, '')} onTouchEnd={this.onSortRequested.bind(this, '')}
            style={{ "margin": 2 }}
            className={`customSortRemoveLabel ${this.state.noSort} customHeaderButton`}>
            <FontAwesomeIcon icon={faTimes} />
          </div>
        </div>;
    }

    return (
      <div style={{ display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "flex-end" }}>
        <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
          {menu}
          {sort}
          <Tooltip title={help_dict["delete_col"]}>
            <FontAwesomeIcon style={{ "margin": 2 }} icon={faTrashCan} className={`customHeaderButton`} onClick={this.onDeleteClicked} />
          </Tooltip>
          <Tooltip title={help_dict["edit_col"]}>
            <FontAwesomeIcon style={{ "margin": 2 }} icon={faPenToSquare} className={`customHeaderButton`} onClick={this.onEditClicked} />
          </Tooltip>
        </div>
        <div style={{ "margin": 2 }}>
          {this.props.displayName}
        </div>
      </div>
    );
  }

  onDeleteClicked() {
    //console.log("Remove")
    //console.log(this.props.column.colDef.field)
    this.props.table.tryRemoveColumn(this.props.column.colDef.field)
    //this.props.showColumnMenu(this.menuButton);
  }

  onEditClicked() {
    this.props.table.tryEditColumn(this.props.column.colDef.field)
  }

  onSortChanged() {
    this.setState({
      ascSort: this.props.column.isSortAscending() ? 'active' : 'inactive',
      descSort: this.props.column.isSortDescending() ? 'active' : 'inactive',
      noSort: !this.props.column.isSortAscending() && !this.props.column.isSortDescending() ? 'active' : 'inactive'
    });
  }

  onSortRequested(order, event) {
    this.props.setSort(order, event.shiftKey);
  }
}

export class DisplayIndexTable extends React.Component {

  IncludeColumnDef = {
    headerName: "Include",
    field: 'include',
    width: 20,
    pinned: 'left',
    checkboxSelection: true,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    suppressMovable: true,
  }

  FileColumnDef = {
    headerName: 'File',
    field: 'file_name',
    pinned: 'left',
    resizable: true,
    tooltipField: 'file_name',
    cellRenderer: 'btnCellRenderer',
    cellRendererParams: {
      // Le deuxième argument de setState est un callback, qui est exécuté seulement une fois que le setState a effectivement eu lieu
      clicked: (file_id) => {
        if (this.onViewFile) {
          this.onViewFile(file_id)
        }
      },
      //table: this,
    },
    minWidth: 250,
    suppressMovable: true,
    editable: true,
    checkboxSelection: true,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
  }

  PropertyColumnDef = {
    type: 'rightAligned',
    sortable: true,
    editable: true,
    resizable: true,
    filter: true,
    flex: 1,
    minWidth: 150,
    maxWidth: 300,
    headerComponent: DeleteHeader,
    headerComponentParams: { enableMenu: true, enableSorting: true, menuIcon: 'fa-bars', table: this },
    cellStyle: { paddingLeft: 0, borderRight: '1px dotted' },
  }

  ForcePropertyColumnDef = {
    type: 'rightAligned',
    editable: true,
    flex: 1,
    maxWidth: 100,
    valueFormatter: (v) => {
      if (v.column.colDef.field === "DT") {
        return (String(v.value) + " s")
      }
      else if (v.column.colId === "scale") {
        return (String(v.value) + " um")
      }
    },
    onClick: (v) => {
      console.log(v)
    },
    cellStyle: { paddingLeft: 0, borderRight: '1px dotted' },
  }


  //this.IncludeColumnDef, 
  defaultColumnDefs = [this.FileColumnDef]

  constructor(props) {

    super(props);
    this.onViewFile = props.onViewFile;
    this.onFileValueChanged = props.onFileValueChanged;
    this.state = {
      openJobDialog: false,
      rowdata: [],
      colnames: this.defaultColumnDefs,
      newcolname: 'new_col_name',
      //selectedfilename: '',
      title: '',
      launch_authorized: false,
      job_launching: false,
      jobColumnNames: [],
      is3D: props.is3D
    };
    this.tableRef = React.createRef();

    this.fetchData = this.fetchData.bind(this);
    this.postData = this.postData.bind(this);

    this.newCol = this.newCol.bind(this);
    this.submitRun = this.submitRun.bind(this);
    this.deleteAll = this.deleteAll.bind(this);
    this.updateColdefsFromColNames = this.updateColdefsFromColNames.bind(this)
    this.onSelectionChanged = this.onSelectionChanged.bind(this)
  }

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.is3D !== this.props.is3D) {
      this.fetchData();
    }
  }

  submitRun(title, hierarchy) {

    const selected_rows = this.tableRef.current.api.getSelectedRows();
    if (selected_rows.length === 0) {
      alert("No files selected");
    }
    else {
      this.setState({ job_launching: true }, () => {
        const postData = new FormData();
        const selected_files = selected_rows.flatMap((row) => row["file_id"]);
        postData.append("selected_files", JSON.stringify(selected_files))
        postData.append("title", JSON.stringify(title))
        postData.append("hierarchy", JSON.stringify(hierarchy))
        postData.append("author", JSON.stringify("default_author"))
        postData.append("description", JSON.stringify("default_description"))
        postData.append("is3D", this.state.is3D)

        axios.post("/api/submit_job", postData)
          .then(response => this.props.onJobSubmitted(response.data))
          .then(this.setState({ openJobDialog: false, job_launching: false }))
          .then(() => this.tableRef.current.api.deselectAll())
      })

    }
  }

  fetchData() {

    if (this.tableRef.current.api) {
      this.tableRef.current.api.showLoadingOverlay()
    }
    var data = new FormData()
    data.append("is3D", JSON.stringify(this.props.is3D))
    axios.post("/api/get_index_table/0", data)
      .then(response => {
        const data = response.data
        this.updateColdefsFromColNames(data["colnames"])
        this.setState({ rowdata: data["rowdata"], launch_authorized: false });
        if (this.tableRef.current && this.tableRef.current.api) {
          if (data["rowdata"].length > 0) {
            this.tableRef.current.api.hideOverlay()
          }
        }
      })
  }

  updateColdefsFromColNames(colnames) {
    let new_col_defs = []
    colnames.forEach(name => {
      if ((name !== "DT") && (name !== "scale")) {
        let new_col_def = { ...this.PropertyColumnDef }
        new_col_def["field"] = name
        new_col_defs.push(new_col_def)
      }
    });

    let DT_col_def = { ...this.ForcePropertyColumnDef }
    DT_col_def["field"] = "DT"
    DT_col_def["headerName"] = "Exposure"
    DT_col_def["headerTooltip"] = "time between two consecutive frames (in seconds)"

    let scale_col_def = { ...this.ForcePropertyColumnDef }
    scale_col_def["field"] = "scale"
    scale_col_def["headerTooltip"] = "scaling factor for localizations (from file units to microns)"

    new_col_defs = [DT_col_def, scale_col_def].concat(new_col_defs)
    this.setState({ colnames: this.defaultColumnDefs.concat(new_col_defs) })
  }

  postData(data) {//action, newValue, row, column) {
    const postData = new FormData();
    postData.append("action", JSON.stringify(data["action"]));
    postData.append("col_name", JSON.stringify(data["col_name"]));
    postData.append("row_value", JSON.stringify(data["row_value"]));
    postData.append("is3D", JSON.stringify(this.props.is3D))
    //postData.append("col", data["column"]);

    if (data["action"] === "delAll") {
      this.tableRef.current.api.showLoadingOverlay()
    }

    axios.post("/api/update_index_table", postData)
      .then(response => {
        const resp_data = response.data
        this.updateColdefsFromColNames(resp_data["colnames"]);

        if ((data["action"] === "changeRow") && (resp_data["changed_columns"].some(elt => { return ["DT", "scale"].includes(elt) }))) {
          console.log("Should fetch data because DT or scale was updated")
          this.onFileValueChanged(JSON.parse(data["row_value"]).file_name)
        }

        if (data["action"] === "delAll" || data["action"] === "editCol") {
          this.fetchData()
        }
      })
  }

  tryRemoveColumn(colName) {
    if (window.confirm("Do you really want to remove the following column:\n" + colName + " ?")) {
      this.postData({ "action": "delCol", "col_name": colName })
    }
  }

  tryEditColumn(colName) {
    var newColName = window.prompt("Edit column name :", colName)
    if (newColName) {
      if (!FORBIDDEN_COL_NAMES.includes(newColName)) {
        this.postData({ "action": "editCol", "col_name": [colName, newColName] })
      }
      else {
        alert("Invalid column name: " + newColName)
      }
    }
  }

  newCol() {
    var newColName = window.prompt("New column name :", "my_column")
    if (newColName) {
      this.postData({ "action": "newCol", "col_name": newColName })
    }
  }

  onCellValueChanged = ({ data }) => {
    //console.log(JSON.stringify(data))
    this.postData({ "action": "changeRow", "row_value": JSON.stringify(data) });
  };

  deleteAll() {
    if (window.confirm("Do you really want to delete all files ?\n(this cannot be undone)")) {
      this.props.onDeleteFiles()
      this.postData({ "action": "delAll" });
      this.setState({ title: '' })
    }
  }

  onSelectionChanged(arg) {
    this.setState({ launch_authorized: arg.api.getSelectedRows().length > 0 })
  }

  render() {
    //const columnNames = this.state.colnames.map((elt) => elt.field).filter((col) => !["DT","scale","file_id","file_path","file_name"].includes(col))
    return (
      <Container maxWidth="lg">
        <Stack justifyContent="center" alignItems="stretch" spacing={1}>
          <div id="upload_index_table" className="ag-theme-alpine" style={{ height: 300, width: '100%' }}>
            <AgGridReact
              rowData={this.state.rowdata}
              columnDefs={this.state.colnames}
              rowSelection={'multiple'}
              onCellValueChanged={this.onCellValueChanged}
              onSelectionChanged={this.onSelectionChanged} //TODO: changer l'activation du bouton "run" en fonction de si la taille de la sélection est > 0
              suppressRowClickSelection={true}
              components={{ btnCellRenderer: BtnCellRenderer, deleteHeader: DeleteHeader }}
              enableBrowserTooltips={true}
              ref={this.tableRef}
              overlayNoRowsTemplate={'<span class="ag-overlay-loading-center">After you have uploaded your trajectory files, you will be able to visualise and edit them here</span>'}
              overlayLoadingTemplate={'<span class="ag-overlay-loading-center">Updating the index table ...<ClipLoader /></span>'}
            >
            </AgGridReact>
          </div>
          <Stack spacing={1}
            direction="row"
            justifyContent="center"
            alignItems="center">
            <ButtonGroup id="upload_buttons_row" size="medium">
              <Tooltip title={help_dict["add_column"]}>
                <span>
                  <Button onClick={this.newCol} startIcon={<AddCommentIcon />}> Add new Column</Button>
                </span>
              </Tooltip>
              <Tooltip title={help_dict["delete_files"]}>
                <span>
                  <Button onClick={this.deleteAll} startIcon={<DeleteForeverIcon />}> Remove all files</Button>
                </span>
              </Tooltip>
            </ButtonGroup>
            <ButtonGroup id="upload_buttons_row" size="medium" variant="contained">
              <Tooltip title={this.state.launch_authorized ? help_dict["submit"] : "First, select files to analyse."}>
                <span>
                  <Button onClick={() => {
                    const selected_rows = this.tableRef.current.api.getSelectedRows();
                    if (selected_rows.length === 0) {
                      alert("No files selected");
                    }
                    else {
                      const data = new FormData()
                      data.append("selected_rows", JSON.stringify(selected_rows))
                      data.append("colnames", JSON.stringify(this.state.colnames.map((item) => item.field)))

                      axios.post("/api/get_job_columns", data).then(
                        (response) => {
                          console.log(response.data)
                          this.setState({
                            openJobDialog: true,
                            jobColumnNames: Object(response.data["columns"])
                          })
                        }
                      )
                    }
                  }} endIcon={<PlayCircleOutlineIcon />} disabled={(this.state.launch_authorized === false) || this.state.job_launching}>Launch analysis</Button>
                </span>
              </Tooltip>
            </ButtonGroup>
          </Stack>
        </Stack >
        <JobDialog
          open={this.state.openJobDialog}
          columns={this.state.jobColumnNames}
          handleClose={() => this.setState({ openJobDialog: false })}
          submitJob={this.submitRun}
        />

      </Container>
    );
  }
}



export default DisplayIndexTable;