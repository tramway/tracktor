import React, { Component } from "react";
import "./Dropzone.css";
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import Box from '@mui/material/Box'
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Tooltip from "@mui/material/Tooltip";

class Dropzone extends Component {
  constructor(props) {
    super(props);
    this.state = { hightlight: false, file_pattern: "*", open: false };
    this.fileInputRef = React.createRef();
    this.folderInputRef = React.createRef();
    this.filePatternRef = React.createRef();

    this.openFileDialog = this.openFileDialog.bind(this);
    this.openFolderDialog = this.openFolderDialog.bind(this);
    this.onFilesAdded = this.onFilesAdded.bind(this);
    this.onFolderAdded = this.onFolderAdded.bind(this);
    this.onDragOver = this.onDragOver.bind(this);
    this.onDragLeave = this.onDragLeave.bind(this);
    this.onDrop = this.onDrop.bind(this);

    this.willSelectFolder = this.willSelectFolder.bind(this)
    this.willCloseFolderAlert = this.willCloseFolderAlert.bind(this)
    this.terminationChanged = this.terminationChanged.bind(this)
  }

  openFileDialog() {
    if (this.props.disabled) return;
    this.fileInputRef.current.click();
  }

  openFolderDialog() {
    if (this.props.disabled) return;
    this.folderInputRef.current.click();
  }

  onFilesAdded(evt) {
    if (this.props.disabled) return;
    const files = evt.target.files;
    if (this.props.onFilesAdded) {
      const array = this.fileListToArray(files);
      this.props.onFilesAdded(array);
    }
  }

  onFolderAdded(evt) {

    if (this.props.disabled) return;
    const files = evt.target.files;

    if (this.props.onFolderAdded) {
      const array = this.folderToArray(files);
      this.props.onFolderAdded(array);
    }

  }

  onDragOver(event) {
    event.preventDefault();
    if (this.props.disabed) return;
    this.setState({ hightlight: true });
  }

  onDragLeave(event) {
    this.setState({ hightlight: false });
  }

  onDrop(event) {
    event.preventDefault();
    if (this.props.disabed) return;
    const files = event.dataTransfer.files;

    for (var i = 0, f; f = files[i]; i++) { // iterate in the files dropped
      if (!f.type && (f.size % 4096 === 0)) {
        // One file is a folder
        alert('Drag & dropping folders is not supported')
        return;
      }
    }

    if (this.props.onFilesAdded) {
      const array = this.fileListToArray(files);
      this.props.onFilesAdded(array);
    }
    this.setState({ hightlight: false });
  }

  fileListToArray(list) {
    const array = [];
    for (var i = 0; i < list.length; i++) {
      array.push(list.item(i));
    }
    return array;
  }

  fileShouldBeUploaded(f) {
    var pattern_string = this.state.file_pattern
    try {
      pattern_string = pattern_string.replaceAll(".", "\\.");
      pattern_string = pattern_string + "$"
      var pattern = RegExp(pattern_string, "g");
      return pattern.test(f.webkitRelativePath)
    }
    catch {
      return true
    }

  }

  folderToArray(list) {
    const array = [];
    for (var i = 0; i < list.length; i++) {
      var file = list.item(i)
      if (this.fileShouldBeUploaded(file)) {
        array.push(file);
      }
    }
    console.log(array)
    return array;
  }

  willSelectFolder() {
    this.setState({ open: true })
  }

  willCloseFolderAlert() {
    this.setState({ open: false })
  }

  terminationChanged(event) {
    this.setState({ file_pattern: event.target.value })
  }


  render() {
    return (
      <Box
        className={`Dropzone ${this.state.hightlight ? "Highlight" : ""}`}
        onDragOver={this.onDragOver}
        onDragLeave={this.onDragLeave}
        onDrop={this.onDrop}
      >
        <Tooltip title="Click here to select the trajectory files you want to upload">
          <span>
            <Button variant="contained"
              onClick={this.openFileDialog}
            //style={{ cursor: this.props.disabled ? "default" : "pointer" }}
            >
              Upload Files
              <input
                ref={this.fileInputRef}
                className="FileInput"
                type="file"
                multiple
                onChange={this.onFilesAdded}
              />
            </Button>
          </span>
        </Tooltip>

        <img
          alt="upload"
          className="Icon"
          src="baseline-cloud_upload-24px.svg"
        />
        <Tooltip title="Click here to select a folder containing trajectory files. Information about the files will be retrieved from the folder hierarchy and the index table populated accordingly.">
          <span>
            <Button variant="contained" onClick={this.willSelectFolder}>
              Upload Folder
              <input
                ref={this.folderInputRef}
                className="FolderInput FileButton"
                type="file"
                webkitdirectory=""
                directory=""
                multiple
                onChange={this.onFolderAdded}
              />
            </Button>
          </span>
        </Tooltip>
        <Dialog open={this.state.open} onClose={this.willCloseFolderAlert}>
          <DialogTitle>Termination of trajectory files</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Enter the termination of trajectory files to upload in the selected folder (e.g. ".csv", ".mat", "trajectories.txt", ...).
              <br />
              Leave blank to upload all files in the selected folder.
              <br />
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="outlined-name"
              label="Termination of trajectory files in folder"
              defaultValue=""
              fullWidth
              onChange={this.terminationChanged}
              variant="standard"
              placeholder=".csv"
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.willCloseFolderAlert}>Cancel</Button>
            <Button variant="contained" onClick={
              () => {
                this.openFolderDialog()
                this.setState({ open: false })
              }
            }>Continue</Button>
          </DialogActions>
        </Dialog>

      </Box>
    );
  }
}

export default Dropzone;