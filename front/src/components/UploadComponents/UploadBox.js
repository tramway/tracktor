import React, { Component } from "react";
import Dropzone from "./Dropzone";
// import "../../App.css";
import axios from "../../api"
import Stack from '@mui/material/Stack';
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import CircularProgress from '@mui/material/CircularProgress';
import ListItemIcon from '@mui/material/ListItemIcon';
import Divider from '@mui/material/Divider';

function get_unique_name(file) {
  return file.webkitRelativePath ? file.webkitRelativePath.split("/").slice(1).join("/") : file.name
}

class UploadBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      files: [],
      folders: [],
      files_information: {},
      uploading: false,
      uploadProgress: {},
      successfullUploaded: false,
      errorMessages: {},
      is3D: props.is3D
    };

    this.onFilesAdded = this.onFilesAdded.bind(this);
    this.onFolderAdded = this.onFolderAdded.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
  }

  onFilesAdded(files) {
    console.log(files)
    this.setState(prevState => ({
      files: prevState.files.concat(files)
    }), this.uploadFiles);
  }

  onFolderAdded(folder) {

    function onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
    }

    function sharedStart(array) {
      array.forEach((value, i) => {
        if (value) {
          array[i] = value.toLowerCase()
        }

      })
      var A = array.concat().sort(),
        a1 = A[0], a2 = A[A.length - 1], L = a1.length, i = 0;
      while (i < L && a1.charAt(i) === a2.charAt(i)) i++;
      return a1.substring(0, i);
    }

    var folder_levels = {}

    folder.forEach(file => {

      const unique_file_name = get_unique_name(file)
      //console.log("Unique file name : " + unique_file_name)
      unique_file_name.split("/").slice(0, -1).forEach((value, i) => {
        if (!(i in folder_levels)) {
          folder_levels[i] = []
        }
        folder_levels[i] = folder_levels[i].concat(value)
      });
    });

    console.log("Folder levels :")
    console.log(folder_levels)


    var level_names = {}
    let folder_level_keys = [...Object.keys(folder_levels)]
    folder_level_keys.forEach((key) => {
      var value = folder_levels[key]
      var unique_values = value.filter(onlyUnique)
      var shared_start = sharedStart(unique_values)
      shared_start = shared_start.trim()
      shared_start = shared_start.replace(/[-_]+$/g, '');
      var level_name = "Level " + key

      if (unique_values.length === 1) {
        console.log("Folders " + key + "have only one value")
        delete folder_levels[key]
      }
      else if (shared_start.length >= 3) {
        level_name = shared_start
      }
      if (key in folder_levels) {
        level_names[key] = level_name
      }

    });
    const files_information_current = { ...this.state.files_information };
    folder.forEach((file, i) => {
      const unique_file_name = get_unique_name(file)
      files_information_current[unique_file_name] = {}
      Object.keys(folder_levels).forEach((level, index) => {
        var level_value = folder_levels[level]
        files_information_current[unique_file_name][level_names[level]] = level_value[i]
      });
    });
    console.log(files_information_current)

    this.setState({ files_information: files_information_current });
    this.onFilesAdded(folder)
  }

  async uploadFiles() {
    this.setState({ uploading: true });
    const promises = [];
    this.state.files.forEach(file => {
      promises.push(this.sendRequest(file));
    });
    try {
      await Promise.all(promises);
      if (this.state.errorMessages.length === 0) {
        this.setState({ successfullUploaded: true, files: [], uploadProgress: {} })
      }

    } catch (e) {
      console.log("Error in upload :")
      console.log(e)
    }
    finally {
      this.setState({ uploading: false });
      this.props.onUploadEnds() // -> update Table
      // Wait for onuploadEnds to be complete
    }
  }

  isimage(file) {
    var pattern = RegExp("image", "g");
    return pattern.test(file.type)
  }

  sendRequest(file) {

    let simple_error = null
    let prom = null
    if (this.isimage(file)) {
      simple_error = "Tracktor performs analysis from files containing trajectories, not from raw images. You must first use a tool to localize and track particles from microscope recordings."
    }
    else if (file.size > 50 * (10 ** 6)) {
      simple_error = "File is too large (max size : 50Mb)"
    }

    if (simple_error) {
      prom = new Promise((resolve, reject) => {
        resolve({ "data": { "success": false, "detail": simple_error } })
      })
    }
    else {
      const data = new FormData();
      const unique_file_name = get_unique_name(file)

      data.append("file", file);
      data.append("is3D", this.state.is3D);
      data.append("destination_filename", JSON.stringify(unique_file_name));
      // S'il y a une entrée dans state.files_information pour ce fichier, on l'ajoute au corps de la requête

      if (unique_file_name in this.state.files_information) {
        console.log("Adding info to data")
        data.append("info", JSON.stringify(this.state.files_information[unique_file_name]))
      }
      else {
        console.log("No info to add, this was uploaded via file upload")
      }

      prom = axios.post("/api/upload_file", data)
    }

    return (prom
      .then((response) => {
        const data = response.data
        const newProgress = { ...this.state.uploadProgress };
        const newError = { ...this.state.errorMessages };

        var file_name_to_consider = get_unique_name(file)
        // On met à jour l'icone de progression en fonction du résultat de la requête
        if (data.success === true) {
          newProgress[file_name_to_consider] = { state: "done", percentage: 100 };
        }
        else {
          console.log("Error with")
          console.log(file_name_to_consider)
          newProgress[file_name_to_consider] = { state: "error", percentage: 100, error_detail: data.detail }
          newError[file_name_to_consider] = data.detail
        }
        this.setState({ uploadProgress: newProgress, errorMessages: newError });
      }));
  }

  renderProgress(file) {

    var file_name_to_consider = get_unique_name(file)

    const uploadProgress = this.state.uploadProgress[file_name_to_consider];
    if (uploadProgress && uploadProgress.state === "done") {
      return <CheckCircleOutlineIcon />
    }
    else if (uploadProgress && uploadProgress.state === "error") {
      return <ErrorOutlineIcon />
    }
    else {
      return <CircularProgress />
    }
  }

  render() {
    //let n_files_current = this.state.uploadProgress.length ? this.state.uploadProgress.length : 0
    //let n_files_total = this.state.files.length ? this.state.files.length : 1
    let dialog_is_open = this.state.uploading || (Object.keys(this.state.errorMessages).length > 0)
    return (
      <Stack direction="row" justifycontent="space-between" spacing={1} alignItems="flex-start">
        <div>
          <Dropzone
            onFilesAdded={this.onFilesAdded}
            onFolderAdded={this.onFolderAdded}
            disabled={false}
          />
        </div>
        <Dialog
          open={dialog_is_open}
        >
          <DialogTitle>Upload in progress...</DialogTitle>
          <DialogContent>
            <List dense={false} style={{ "zindex": -1 }}>
              {this.state.files.map(file => {
                let key = get_unique_name(file)
                return (
                  <div>
                    <ListItem alignItems="flex-start" justifycontent="space-between" id={key}>
                      <ListItemText primary={key} secondary={key in this.state.errorMessages ? this.state.errorMessages[key] : undefined} />
                      <ListItemIcon>
                        {this.renderProgress(file)}
                      </ListItemIcon>
                    </ListItem>
                    <Divider variant="inset" component="li" />
                  </div>
                );
              })}
            </List>
          </DialogContent>
          <DialogActions>
            {(Object.keys(this.state.errorMessages).length > 0) &&
              <Button
                disabled={this.state.uploading}
                onClick={() => this.setState({
                  uploading: false,
                  files: [],
                  successfullUploaded: false,
                  uploadProgress: {},
                  errorMessages: {}
                })}>
                Continue
              </Button>
            }
          </DialogActions>
        </Dialog>
      </Stack>
    );
  }
}

export default UploadBox;