import {useState, useEffect, Fragment} from 'react'
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormLabel from '@mui/material/FormLabel';
import TextField from '@mui/material/TextField';
import Alert from '@mui/material/Alert';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Step from '@mui/material/Step';
import StepContent from '@mui/material/StepContent';
import Stepper from '@mui/material/Stepper';
import StepLabel from '@mui/material/StepLabel';

function HorizontalLinearStepper({steps, handleCancel, handleSubmit}) {
  const [activeStep, setActiveStep] = useState(0);

  const handleNext = () => {
    if (activeStep === steps.length - 1) {
        handleSubmit()
    }
    else {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };

  const handleBack = () => {
    if (activeStep === 0) {
        handleCancel()
    }
    else {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    }
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <Box sx={{ width: '100%' }}>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((step, index) => {
          return (
            <Step key={step["label"]} >
              <StepLabel>{step["label"]}</StepLabel>
              <StepContent key={activeStep}>
        
            <Typography sx={{ mt: 2, mb: 1 }} variant="h6">{step["indication"]}</Typography>
            {(step["detail"].length > 0) && 
                <Alert severity="info" sx={{ mt: 2, mb: 1 }}>{step["detail"]}</Alert>
            }
                {step["component"]}
            </StepContent>
            </Step>
          );
        })}
      </Stepper>
      <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
          <Button
          variant="contained" 
            color="inherit"
            onClick={handleBack}
            sx={{ mr: 1 }}
          >
            {(activeStep == 0) ? "Cancel" : "Back"}
          </Button>
          <Box sx={{ flex: '1 1 auto' }} />

            <Button onClick={handleNext} disabled={!steps[activeStep]["answerOK"]} variant="contained" >
              {activeStep === steps.length - 1 ? 'Submit job' : 'Next'}
            </Button>
          </Box>
    </Box>
  );
}

export default function JobDialog({columns, submitJob, open, handleClose}) {

    const [title, setTitle] = useState("")
    const default_hierarchy = {}
    useEffect(() => {
      columns.forEach((v,i) => {
          default_hierarchy[v] = i
      })
      Object.keys(default_hierarchy).forEach((c) => {
        if (!columns.includes(c)) {
          default_hierarchy.delete(c)
        }
      })
      setHierarchy(default_hierarchy)
    },[columns])

    const [hierarchy, setHierarchy] = useState(default_hierarchy)

    const steps = [{
        "label":"Title",
        "indication":"Enter a name for the job",
        "detail":"",
        "component":
            <span>
                <TextField
                    autoFocus
                    margin="dense"
                    id="outlined-name"
                    label="Run name"
                    defaultValue={title}
                    //type="email"
                    fullWidth
                    onChange={(e) => { setTitle(e.target.value) }}
                    variant="standard"
                    placeholder="My Job"
                    />
            </span>,
        "answerOK": title.length > 0
    }]
    if (columns.length > 1) {
        steps.push({
            "label":"Hierarchy of factors",
            "indication":"Specify the hierarchy of factors",
            "detail":"Level 1 is the most coarse grained factor. Note that several factors might be on the same hierarchical level (see the user guide for details).",
            "answerOK": true,
            "component":
                <span>
                    <Stack>
                    {columns.map((col,i) => {
                        return(
                        <FormControl>
                            <FormLabel id={col}>{col}</FormLabel>
                                <RadioGroup
                                    aria-labelledby={col}
                                    value={hierarchy[col]}
                                    name="radio-buttons-group"
                                    onChange={(e,value) => {
                                        const h = {...hierarchy}
                                        h[col] = parseInt(value)
                                        setHierarchy(h)
                                    }
                                    }
                                    row
                                >
                                    {columns.map((c, j) => (<FormControlLabel value={j} control={<Radio />} label={j+1} />))}
                                </RadioGroup>
                        </FormControl>
                    )})}
                    </Stack>
                </span>
        })
    }   

    return (
        <Dialog open={open}>
          <DialogTitle>Enter job details</DialogTitle>
          
          <DialogContent>
            <HorizontalLinearStepper 
                handleCancel={handleClose}
                handleSubmit={() => {
                    submitJob(title, hierarchy)
                }}
                steps={steps}
            />
          </DialogContent>
        </Dialog>
    )
}