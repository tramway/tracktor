// dictionnary of the app's tooltips 


export const help_dict =
{
    "add_column": "Add a column containing information about the trajectory files (e.g. regarding the condition under which they were acquired, or the biological replicate from which they originate).",
    "delete_files": "Delete all the files listed in the table. This will not affect previously submitted runs.",
    "binoculars": "View trajectories",
    "delete_col": "Delete this column",
    "edit_col": "Edit column name",
    "submit": "Run analysis with selected files and current column values"
}