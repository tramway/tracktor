import React from 'react';
import DisplayFreezedIndexTable from './ResultsIndexTable';
import DisplayLocs from './TrajectoriesPlot';
import Select from '@mui/material/Select';
import Grid from '@mui/material/Grid';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import axios from "../../api";
import RunDescription from './RunDescription';

class RunSummary extends React.Component {

  constructor(props) {

    super(props);
    this.state = {
      selected_files: [],
      selectedIndicator: 'alpha',
      info: {},
      points: props.selectedPoints,
    }
    this.index_table_ref = React.createRef();
    this.displaylocs_ref = React.createRef();
    this.addFileToView = this.addFileToView.bind(this)
    this.fetchData = this.fetchData.bind(this)
  };

  componentDidUpdate(prevProps) {
    if (prevProps.selectedPoints !== this.props.selectedPoints) {
      console.log("New point detected")
      this.setState({points:this.props.selectedPoints})
    }
  }

  fetchData() {
    if (this.props.job_id) {
      axios.get("/api/job_info/" + this.props.job_id).then(response => {
        this.setState({ info: response.data })
      })
      axios.get("/api/job_results/" + this.props.job_id + "/trad")
        .then(response => {
          this.setState({ levels: response.data["factors_list"], dataloading: false });
        })
    }
  }
  // handle the change of the dropdown menu

  componentDidMount() {
    this.fetchData();
  }

  addFileToView(f) {
    if (!this.state.selected_files.includes(f)) {
      this.setState({ selected_files: [...this.state.selected_files, f] })
    }
    else {
      this.setState({selected_files: [...this.state.selected_files.filter((file) => file !== f), f]})
    }
  }


  // render a dropdown menu with the levels
  render() {
    return (
      <Stack spacing={2} direction="column" alignItems="center" justifycontent="flex-start" mt={2}>
        <Typography variant='h4' align="center">
          {('"' + this.state.info.name + '"') + ' (run #' + this.props.job_id + ')'}
        </Typography>
        <RunDescription info={this.state.info} />
        <DisplayFreezedIndexTable ref={this.index_table_ref} onViewFile={this.addFileToView} job_id={this.props.job_id} />
        <Divider flexItem orientation="horizontal" />
        <FormControl style={{ minWidth: 130 }}>
          <InputLabel id="mds-label">Color by...</InputLabel>
          <Select
            label="Color by..."
            labelId="mds-label"
            value={this.state.selectedIndicator}
            onChange={e => this.setState({ selectedIndicator: e.target.value })}
          >
            {[
              { value: "alpha", label: "Anomalous diffusion exponent (alpha)" },
              { value: "log_D", label: "Log-diffusivity" },
              { value: "best_model", label: "Most plausible model" },
              { value: "n_points", label: "Number of localizations" },
              { value: "t", label: "Time" },
              { value: "point", label: "Closest latent point" },
            ].map(
              option => (<MenuItem value={option.value} key={option.value} disabled={(option.value === "point") && (this.state.points.length === 0)}>{option.label}</MenuItem>)
            )}
          </Select>
        </FormControl>
        {(this.state.selected_files.length > 0) ?
          <Grid container spacing={2}>
            {this.state.selected_files.map((f, i) => (
              <Grid item key={f}>
                <DisplayLocs
                  file_id={f}
                  points={this.state.points}
                  handleClose={() => {
                    let newList = [...this.state.selected_files].filter((v) => (v !== f))
                    this.setState({ selected_files: newList })
                  }}
                  selectedIndicator={this.state.selectedIndicator} job_id={this.props.job_id} />
              </Grid>
            ))}
          </Grid> : <Typography>Click on the binocular icons to visualise trajectories and analysis results of the corresponding files</Typography>
        }
      </Stack>
    );
  }
}

export default RunSummary