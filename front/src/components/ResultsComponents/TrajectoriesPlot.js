import React from 'react';
import Plot from 'react-plotly.js';
import axios from "../../api"
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import IconButton from '@mui/material/IconButton';
import CardHeader from '@mui/material/CardHeader';
import CloseIcon from '@mui/icons-material/Close';
import Skeleton from '@mui/material/Skeleton';


class DisplayLocs extends React.Component {

  constructor(props) {

    super(props);
    this.state = {
      trajs: [{ "x": [], "y": [], "z": [], "t": [], "colors": { "t": [] } }],
      ticks: {},
      range: [-.5, .5, -.5, .5],
      time_range: [0, 1],
      color_scales: {},
      loading: true,
      points: props.points,
      is3D: props.is3D ? true : false,
    };
    this.fetchData = this.fetchData.bind(this)

  }

  componentDidUpdate(prevProps) {
    if (prevProps.points !== this.props.points) {
      //console.log("New point detected")
      this.setState({ points: this.props.points }, () => this.fetchData())
    }
  }

  fetchData() {
    this.setState({ loading: true })
    //console.log("Fetch data in display_locs for file " + this.props.file_id)

    if (this.props.file_id) {
      let data = new FormData()
      data.append("points", JSON.stringify(this.state.points))
      data.append("is3D", JSON.stringify(this.state.is3D))
      axios.post("/api/get_file_locs/" + this.props.file_id + "/" + this.props.job_id, data)
        .then(response => {
          const is3D = response.data.trajs[0].z != null
          console.log("is 3D: ", is3D)
          this.setState({
            trajs: response.data.trajs,
            range: response.data.range,
            time_range: response.data.time_range,
            loading: false,
            color_scales: response.data.color_scales,
            ticks: response.data.ticks,
            file_name: response.data.file_name,
            is3D: is3D,
          });
        })
    }
  }

  // handle the change of the dropdown menu

  componentDidMount() {
    this.fetchData();
  }

  render() {
    let selectedIndicator = this.props.selectedIndicator
    let coloraxis = null
    if (this.state.color_scales[selectedIndicator]) {
      coloraxis = {
        cmin: this.state.color_scales[selectedIndicator]["cmin"],
        cmax: this.state.color_scales[selectedIndicator]["cmax"],
        colorscale: this.state.color_scales[selectedIndicator]["scale"],
        showscale: true,
      }
    }
    let ticks = null;
    if (this.state.ticks[selectedIndicator]) {
      ticks = this.state.ticks[selectedIndicator]
    }

    return (
      <Card>
        <CardHeader
          sx={{
            maxWidth: 500,
            display: "flex",
            overflow: "hidden",
            "& .MuiCardHeader-content": {
              overflow: "hidden"
            }
          }}
          action={
            <IconButton aria-label="close" onClick={() => { this.props.handleClose() }}>
              <CloseIcon />
            </IconButton>
          }
          title={this.state.file_name}
          titleTypographyProps={{ align: "center", variant: "h6", noWrap: true }}
        />
        <CardContent>

          {this.state.loading &&
            <Skeleton variant="rounded" width={500} height={500} />
          }

          {!this.state.loading &&
            <Plot
              layout={{
                width: 600,
                height: 600,
                autosize: false,
                showlegend: false,
                xaxis: { title: "x (micrometers)", range: [this.state.range[0], this.state.range[1]] },
                yaxis: { title: "y (micrometers)", range: [this.state.range[2], this.state.range[3]] },
                scene: this.state.is3D ? { aspectmode: "data" } : {}, // Fait que z a la même échelle que x et y
                coloraxis: coloraxis
              }}

              config={{
                scrollZoom: true,
                pan: true,
                modeBarButtons: [["zoom2d", "pan2d"], ["resetViews"], ["toImage"]]
              }}

              data={this.state.trajs.map(function (elt, index) {

                if (elt.x.length < 1) {
                  return {}
                }
                let customdata = {}
                if (elt.alpha) {
                  customdata["alpha"] = elt.alpha
                }
                if (elt.log_D) {
                  customdata["log_D"] = elt.log_D
                }
                if (elt.best_model) {
                  customdata["best_model"] = elt.best_model
                }
                let to_return = {
                  x: elt.x,
                  y: elt.y,
                  z: this.state.is3D ? elt.z : null,
                  type: this.state.is3D ? "scatter3d" : 'scattergl',
                  mode: 'lines',
                  customdata: customdata,
                  hovertemplate: 't: ' + elt.t[0].toFixed(1) + 's<extra>Trajectory ' + elt.n + '</extra>',
                  //hovertext: "n = " + elt.n + "<br>t = " + elt.t[0] + "</br>",
                  line: {
                    color: elt["colors"][selectedIndicator]
                  },
                };


                if (coloraxis) {

                  if (index === 0) {
                    to_return["mode"] = "markers+lines"
                    to_return["marker"] = {
                      autocolorscale: false,
                      color: elt[selectedIndicator],
                      size: 1,
                      colorscale: coloraxis.colorscale,
                      cmin: coloraxis.cmin,
                      cmax: coloraxis.cmax,
                      colorbar: {
                        xanchor: "center",
                        title: selectedIndicator,
                      },
                      showscale: true
                    }
                    if (ticks) {
                      to_return.marker.colorbar["tickmode"] = "array"
                      to_return.marker.colorbar["tickvals"] = ticks["positions"]
                      to_return.marker.colorbar["ticktext"] = ticks["labels"]
                    }
                  }
                }
                return to_return;
              }.bind(this))}
            />
          }
        </CardContent>
      </Card>
    );
  }
}

export default DisplayLocs;