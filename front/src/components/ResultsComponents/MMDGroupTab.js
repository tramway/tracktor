import React from 'react';
import Plot from 'react-plotly.js';
import Stack from '@mui/material/Stack';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import IconButton from '@mui/material/IconButton'
import Divider from '@mui/material/Divider'
import CloseIcon from '@mui/icons-material/Close';
import CardContent from '@mui/material/CardContent'
import Alert from '@mui/material/Alert';
import Skeleton from '@mui/material/Skeleton';
import CircularProgress from '@mui/material/CircularProgress';
import axios from "../../api"
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import FactorSelect from './FactorSelect';
import { makeDataFromPoint, getColorCode } from './utils';


//const argFact = (compareFn) => (array) => array.map((el, idx) => [el, idx]).reduce(compareFn)[1]

//const argMax = argFact((min, el) => (el[0] > min[0] ? el : min))
//const argMin = argFact((max, el) => (el[0] < max[0] ? el : max))

const trajectory_colors = ["0203e2","#fe6700","#00bffe","#be03fd","#00ff00","#f2013f","#19a700","#ffa812","#fe01b1"]


const plot_to_show_to_index = (plot) => {
  // mapping between plot labels and indices of witness_function_dict entries
  plot = String(plot)
  if (plot === "witness") {
    return 0
  }
  else if (plot === "A") {
    return 1
  }
  else if (plot === "B") {
    return 2
  }
  else {
    return 3
  }
}

const plot_config = {
  modeBarButtons: [["toImage"]],
  toImageButtonOptions: {
    format: 'jpeg', // one of png, svg, jpeg, webp
    filename: 'plot',
    height: 700,
    width: 700,
    scale: 1. // Multiply title/legend/axis/canvas sizes by this factor
  },
  displaylogo: false,
  responsive: true,
}

const colorMaps = {
  "A": "Reds",
  "B": "Blues",
  "witness": "RdBu",
  "stats": "Greens",
}

class DisplayMMDByGroup extends React.Component {

  constructor(props) {

    super(props);
    this.state = {
      levels: [],
      selectedGroupPair: null, // ID of the pair (string)
      selectedA: null, // ID of the first group (string)
      selectedB: null, // ID of the second group (string)
      needReverse: false, // whether to reverse colorscale in witness function
      dataloading: true,
      selectedLevel: 'unit',
      factors:[],
      selectedFactors:[],
      mmd: [],
      scale: 0.5,
      isHMZoneSelected: false,
      maxlabel: 0,
      selectedPoints: props.selectedPoints,
      plotToShow: "witness",
      witness_function: null,
      selectedCoordinates: [],
    };
    this.getTitle = this.getTitle.bind(this)
    this.fetchData = this.fetchData.bind(this)
    this.fetchExampleTrajs = this.fetchExampleTrajs.bind(this)
    this.fetchWitnessFunction = this.fetchWitnessFunction.bind(this)

  }

  getTitle(p) {
    if (p === "A") {
      return 'Latent space occupation density<br />' + this.state.selectedA
    }
    else if (p === 'B') {
      return 'Latent space occupation density<br />' + this.state.selectedB
    }
    else {
      return 'Witness function between<br /> ' + this.state.selectedA + ' and ' + this.state.selectedB + ''
    }
  }

  fetchData() {

    this.setState({dataloading:true}, 
      () => {
        const data = new FormData()
        const isDataRequest = this.state.selectedFactors.length > 0
        if (isDataRequest) {
          data.append("factors",JSON.stringify(this.state.selectedFactors))
        }
        axios.post("/api/job_results/" + this.props.job_id + "/mmd_by_group", data, { "responseType": "text" })
          .then(response => {
              if (isDataRequest) {
                this.setState({
                  p_val: response.data["p_val"],
                  group_labels: response.data.group_labels,
                  group_indices: response.data.group_indices,
                  comp_matrix: response.data.comp_matrix,
                  dataloading: false,
                });
              }
              else {
                
                const factors = response.data.factors.filter((f) => {
                  console.log(f)
                  return f[0] !== "file"
                })
                console.log("Setting factors", factors, response.data.factors)
                this.setState({
                  factors: factors,
                  dataloading: this.state.selectedFactors.length > 0,
                  x: response.data.x,
                  y: response.data.y,
                  scale: response.data.scale,
                })
              }

          })
    })
    
  }

  fetchWitnessFunction(comparison_index, i, j) {
    let data = new FormData()
    data.append("comparison_index", JSON.stringify([comparison_index, i, j]))
    axios.post("/api/get_witness_function/" + this.props.job_id, data, { "responseType": "text" })
      .then(response => {
        try {
          this.setState({
            witness_function: response.data
          });
        }
        catch (e) {
          console.log(response.data)
        }

      })
  }

  fetchExampleTrajs(x, y) {

    let data = new FormData()
    data.append("xy", JSON.stringify([x, y]))
    data.append("scale", JSON.stringify(this.state.scale))
    data.append("r", JSON.stringify(3 * (this.state.x[0][1] - this.state.x[0][0]) / 2))
    data.append("job_id", JSON.stringify(this.props.job_id))
    axios.post("/api/example_trajectories", data).then(response => {
      try {
        try {
          try {
            response.data = JSON.parse(response.data.replace(/\bNaN\b/g, "null"))
          }
          catch (e) {
            response.data = JSON.parse(response.data)
          }
        }
        catch (e) {
          //console.log(response.data)
        }

      }
      catch (e) {
        //console.log(response.data)
      }

      let color = getColorCode()
      let label = this.state.maxlabel + 1

      this.setState({
        maxlabel: label,
        traj_plot_data: [...this.state.traj_plot_data, {
          "label": label,
          "color": color,
          "coords": {
            "x": x,
            "y": y
          },
          "plotly_data": response.data.map((traj, i) => {
            return {
              "x": traj[0],
              "y": traj[1],
              "hoverinfo": "none",
              "marker": {
                "color": trajectory_colors[i],
              },
              "mode": "lines"
            }
          })
        }]
      })

    })


  }

  // handle the change of the dropdown menu

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedPoints !== this.props.selectedPoints) {
      console.log("New point detected")
      this.setState({selectedPoints:this.props.selectedPoints})
    }
  }

  flatten2dArray(a) {
    return (a.concat.apply([], a))
  }



  // render a dropdown menu with the levels
  render() {

    let log_p = null
    if (this.state.p_val) {
      log_p = this.state.p_val.map((p_row) => (p_row.map((p) => Math.max(0., -Math.log10(p)))))
    }

    let selectedBox = {}
    if (this.state.selectedCoordinates.length === 2) {
      let i = this.state.selectedCoordinates[0]
      let j = this.state.selectedCoordinates[1]
      selectedBox = {
        "y": [i-0.5,i+0.5,i+0.5,i-0.5,i-0.5],
        "x": [j-0.5,j-0.5,j+0.5,j+0.5,j-0.5],
        "mode": "lines",
        "marker": {"color":"black"},
        "line":{"width":3},
        "hoverinfo":"none"
      }
    }

    console.log("Group labels", this.state.group_labels)

    return (
      <Stack mt={2} spacing={2} alignItems="center" justifycontent="flex-start">
        <FactorSelect 
          factors={this.state.factors}
          onChange={(v) => {
            this.setState({
              witness_function: null,
              selectedFactors:v,
              dataloading:true,
              selectedCoordinates: [],
              selectedGroupPair: null,
              selectedA: null,
              selectedB: null
            },() => this.fetchData())
        }}/>
        {this.state.dataloading &&
          <CircularProgress />
        }
        {
          !this.state.dataloading && (this.state.factors.length == 0) &&
          <Alert severity="warning">
            There are no groups to compare. Add columns to the index table to attach files to given groups.
          </Alert>
        }
        {!this.state.dataloading && (this.state.factors.length > 0) && (this.state.group_labels) &&
          <Stack direction="column" alignItems="center">
            <Stack direction="row" alignItems="flex-end" mb={1} spacing={1}>
              <Stack direction="column" alignItems="center" spacing={1}>
                <Typography align="center">Click on a cell to dislay the corresponding comparison</Typography>
                <Plot className="result_plot"
                  data={
                    [{
                      z: log_p,
                      x: this.state.group_labels.map((elt, i) => i),
                      y: this.state.group_labels.map((elt, i) => i),
                      text: this.state.p_val,
                      texttemplate: "%{text:.1e}",
                      hovertemplate: "%{x} VS %{y}<br />P=%{text:.2e}<extra></extra>",
                      type: 'heatmap',
                      zmin: -1,
                      zmax: -Math.log10(0.00025),
                      colorscale: "Portland",
                      //colorscale: "YlGnBu",
                      //reversescale: true,
                      showscale: false,
                    },
                  ].concat(selectedBox)}

                  /// sets selectedGroupPair to the string "selected group A vs selected group B"
                  onClick=
                  {e => {
                    console.log(e.points[0])
                    let i = e.points[0].pointIndex[0]
                    let j = e.points[0].pointIndex[1]
                    let comparison_index = parseInt(this.state.comp_matrix[i][j])
                    console.log(i,j)
                    if (comparison_index >= 0) {
                      console.log("Fetching comparison", comparison_index)
                      console.log("group_indices", this.state.group_indices)
                      this.fetchWitnessFunction(
                        this.state.comp_matrix[i][j],
                        this.state.group_indices[i],
                        this.state.group_indices[j])
                      this.setState({
                        selectedGroupPair: this.state.comp_matrix[i][j],
                        selectedA: this.state.group_labels[i],
                        selectedB: this.state.group_labels[j],
                        needReverse: i > j,
                        selectedCoordinates: [i,j]
                      });
                    }
                  }}
                  config={plot_config}
                  layout={{
                    width: 600,
                    height: 600,
                    yaxis: { 
                      scaleanchor: "x", 
                      automargin:true, 
                      ticktext: this.state.group_labels,
                      tickvals: this.state.group_labels.map((elt, i) => i),
                      range: [-0.5,this.state.group_labels.length-0.5],
                      showgrid: false,
                      zeroline: false,
                    },
                    xaxis: { 
                      automargin:true,
                      ticktext: this.state.group_labels,
                      tickvals: this.state.group_labels.map((elt, i) => i),
                      range: [-0.5,this.state.group_labels.length-0.5],
                      showgrid: false,
                      zeroline: false,
                    }
                    //legend: { x: 1, y: 1, yanchor: "bottom", xanchor: "right" },
                    //title: 'MMD p-value between pairs of sets',
                  }}
                />
              </Stack>
              <Divider orientation="vertical" flexItem />
              {this.state.witness_function ?
                <Stack direction="column" spacing={1} alignItems="center">
                  <ToggleButtonGroup
                    color="primary"
                    value={this.state.plotToShow}
                    exclusive
                    onChange={(v) => {
                      console.log("Showing " + v.target.value)
                      this.setState({ plotToShow: v.target.value })
                    }}
                  >
                    <ToggleButton value="A">
                      {this.state.selectedA}
                    </ToggleButton>
                    <ToggleButton value="B">
                      {this.state.selectedB}
                    </ToggleButton>
                    <ToggleButton value="witness">
                      Comparison
                    </ToggleButton>
                    <ToggleButton value="stats">
                      Criticality
                    </ToggleButton>
                  </ToggleButtonGroup>
                  <Plot className="result_plot"
                    style={{ minWidth: 500 }}
                    data={[
                      {
                        x: this.flatten2dArray(this.state.x),
                        y: this.flatten2dArray(this.state.y),
                        // if pairs are flipped (compared to what's stored in the witness_function_dict), we inverse the values of the witness function
                        z: this.flatten2dArray(this.state.witness_function[plot_to_show_to_index(this.state.plotToShow)]).map(elt => (this.state.plotToShow === "witness" && this.state.needReverse) ? -elt : elt),
                        type: 'contour',
                        zmin: this.state.plotToShow === "witness" ? -0.1 : 0.,
                        zmax: this.state.plotToShow === "witness" ? 0.1 : null,
                        colorscale: colorMaps[this.state.plotToShow],
                        showscale: true,
                        reversescale: (this.state.plotToShow === "B"),
                        ncontours: 51,
                        hoverinfo: "none",
                        colorbar: (this.state.plotToShow === "witness") ? {
                          "tickvals": [-0.07, 0.07],
                          "ticktext": [this.state.selectedB, this.state.selectedA]
                        } : null
                      },

                    ].concat(this.state.selectedPoints.map((elt) => makeDataFromPoint(elt)))}

                    layout=
                    {{
                      height: 500,
                      legend: { x: 1, y: 1, yanchor: "bottom", xanchor: "right" },
                      title: this.getTitle(this.state.plotToShow),
                      xaxis: { "title": "UMAP 1" },
                      yaxis: { "title": "UMAP 2", "scaleanchor": "x" }
                    }}
                    config={plot_config}
                    onClick={e => {
                      this.props.addNewPoint(e.points[0].x,e.points[0].y)
                      //this.fetchExampleTrajs(
                      //  e.points[0].x,
                      //  e.points[0].y)
                    }
                    }
                  />
                </Stack> :
                <Stack direction="column" spacing={1} alignItems="center">
                  <Skeleton variant="rounded">
                    <ToggleButtonGroup>
                      <ToggleButton>
                        A
                      </ToggleButton>
                      <ToggleButton>
                        B
                      </ToggleButton>
                      <ToggleButton>
                        Comparison
                      </ToggleButton>
                      <ToggleButton>
                        Criticality
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </Skeleton>
                  <Skeleton variane="rounded">
                    <Plot className="result_plot"
                      style={{ minWidth: 500 }} />
                  </Skeleton>
                </Stack>}
            </Stack>
            
          </Stack>
        }
      </Stack>
    );
  }
}

export default DisplayMMDByGroup;

/*

<Divider orientation="horizontal" flexItem />
            {(this.state.selectedPoints.length > 0) ?
              <Grid container spacing={1} mt={1}>
                {this.state.selectedPoints.map((point, i) => {
                  return (
                    <Grid item key={i}>
                      <Card>
                        <CardHeader
                          action={
                            <IconButton
                              aria-label="close"
                              size="large"
                              variant="contained"
                              onClick={() => {
                                this.props.deletePoint(point.index)
                              }}
                            >
                              <CloseIcon />
                            </IconButton>
                          }
                          title={"Latent point " + point.label}
                          titleTypographyProps={{ "align": "center"}}
                          subheaderTypographyProps={{ "align": "center" }}
                          subheader={"(UMAP 1, UMAP 2) = (" + point.x.toFixed(1) + ", " + point.y.toFixed(1) + ")"}
                        >
                        </CardHeader>
                        <CardContent>
                          <Plot
                            data={
                              this.state.traj_plot_data[i]
                            }
                            layout=
                            {{
                              showlegend: false,
                              width: 500,
                              height: 500,
                              xaxis: { title: "x (um)", range: [-.75* this.state.scale,3.75*this.state.scale]},
                              yaxis: { "scaleanchor": 'x', title: "y (um)", range: [-.75* this.state.scale,3.75*this.state.scale], scaleratio:1},
                            }}
                            config={{ scrollZoom: true, pan: true, modeBarButtons: [["zoom2d", "pan2d"], ["resetViews"], ["toImage"]] }}
                          />
                        </CardContent>
                      </Card>
                    </Grid>);
                })
                }
              </Grid> : <Typography mt={1} align="center">{this.state.selectedGroupPair ? "Click on coordinates of the latent space to see trajectories located nearby" : ""}</Typography>}

            */