import * as React from 'react';
import { useRef, useEffect, useCallback } from "react";
import { useTheme } from '@mui/material/styles';
import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';
import axios from "../../api"
import Plot from 'react-plotly.js';
import { make_data_object, describe_factors } from './utils';

const plot_config = {
  modeBarButtons: [["toImage"]],
  toImageButtonOptions: {
    format: 'jpeg', // one of png, svg, jpeg, webp
    filename: 'plot',
    height: 700,
    width: 1000,
    scale: 1. // Multiply title/legend/axis/canvas sizes by this factor
  },
  displaylogo: false,
}

function get_colors(points) {
  return points.map(point => point.color)
}

export default function PointsOfInterest({ points, job_id, is3D }) {
  const [metrics, setMetrics] = React.useState({});
  const [dataLoading, setDataLoading] = React.useState(true);
  const [exampleTrajs, setExampleTrajs] = React.useState([]);
  const [scale, setScale] = React.useState(1.);
  const [jobIs3D, setJobIs3D] = React.useState(is3D)

  useEffect(() => {
    setDataLoading(points.length > 0)
    setJobIs3D(is3D) // Si le job_id change, peut-être que le is3D change aussi
  }, [job_id])



  // fetch data
  useEffect(() => {
    if (points.length > 0) {
      let data = new FormData()
      data.append("points", JSON.stringify(points))
      axios.post("/api/points_data/" + job_id, data).then((response) => {
        setMetrics(response.data.metrics_dicts)
        setExampleTrajs(response.data.trajectories)
        setScale(response.data.scale)
        setDataLoading(false)
      })
    }
  }, [points])
  useEffect(() => {
    setDataLoading(points.length > 0)
  }, [points])

  return (
    <Stack mt={2} spacing={2} alignItems="center" justifyContent="flex-start">
      {(points.length > 0) ?
        <Typography variant="h4">
          Metrics of latent space regions
        </Typography> :
        <Alert severity="warning">
          Go to the Comparison or Vignette tabs,
          and click on the locations of the latent space whose details you want to display.
        </Alert>}
      {(!dataLoading && points.length > 0) ?
        <Grid container spacing={2} alignItems="center" justifycontent="center">
          <Grid item md={12} lg={6} xl={4}>
            <Card>
              <CardContent>
                <Container class="card-body d-flex align-items-center justify-content-center">
                  <Plot className="result_plot"
                    config={plot_config}
                    data={make_data_object(metrics, "log_D", get_colors(points))}
                    layout={{
                      autosize: true,
                      showlegend: false,
                      title: 'Log-diffusion',
                      yaxis: { title: "log_10 (D) (um^2/s)" },
                      xaxis: { title: "Latent space locations" },
                      //width: 900,
                      //height: 600 
                    }} />
                </Container>
              </CardContent>
            </Card>
          </Grid>
          <Grid item md={12} lg={6} xl={4}>
            <Card>
              <CardContent>
                <Container class="card-body d-flex align-items-center justify-content-center">
                  <Plot className="result_plot"
                    data={make_data_object(metrics, "alpha", get_colors(points))}
                    config={plot_config}
                    layout={{
                      autosize: true,
                      showlegend: false,
                      title: 'Anomalous diffusion exponent',
                      yaxis: { title: "Alpha", range: [0., 2.] },
                      xaxis: { title: "Latent space locations" },
                      //width: 900,
                      //height: 600 
                    }} />
                </Container>
              </CardContent>
            </Card>
          </Grid>
          <Grid item md={12} lg={6} xl={4}>
            <Card>
              <CardContent>
                <Container class="card-body d-flex align-items-center justify-content-center">
                  <Plot className="result_plot"
                    data={metrics["values"]["best_model"].map((elt, i) => {
                      return {
                        x: elt,
                        name: metrics["label"][i],
                        type: "histogram",
                        histnorm: "percent",
                        hoverinfo: "name",
                        marker: {
                          color: points[i].color,
                        }
                      }
                    })}
                    config={plot_config}
                    layout={{
                      autosize: true,
                      showlegend: false,
                      title: 'Predicted random walk model', yaxis: {
                        title: "Percentage of trajectories"
                      },
                      //width: 900,
                      //height: 600
                    }} />
                </Container>
              </CardContent>
            </Card>
          </Grid>
        </Grid> :
        dataLoading ?
          <CircularProgress /> : <></>
      }
      {(!dataLoading && points.length > 0) ?
        <>
          <Typography variant="h4">
            Example trajectories
          </Typography>
          <Grid container spacing={2} alignItems="center" justifycontent="center">
            {points.map((point, i) => {
              return (
                <Grid item key={i}>
                  <Card>
                    <CardHeader
                      title={point.longlabel}
                      titleTypographyProps={{ "align": "center" }}
                      subheaderTypographyProps={{ "align": "center" }}
                      subheader={"(UMAP 1, UMAP 2) = (" + point.x.toFixed(1) + ", " + point.y.toFixed(1) + ")"}
                    >
                    </CardHeader>
                    <CardContent>
                      <Plot
                        data={
                          exampleTrajs["trajectories"][i].map((elt) => {
                            var data = {
                              "x": elt[0],
                              "y": elt[1],
                              "mode": "lines",
                              "hoverinfo": "none",
                              "type": jobIs3D ? "scatter3d" : 'scattergl'
                            }
                            if (jobIs3D) {
                              data["z"] = elt[2]
                            }
                            return (data)
                          })
                        }
                        layout=
                        {{
                          showlegend: false,
                          width: 500,
                          height: 500,
                          scene: jobIs3D ? { aspectmode: "data" } : {}, // Fait que z a la même échelle que x et y
                          xaxis: { title: "x (um)", range: [-.75 * scale, 3.75 * scale] },
                          yaxis: { "scaleanchor": 'x', title: "y (um)", range: [-.75 * scale, 3.75 * scale], scaleratio: 1 },
                        }}
                        config={{ scrollZoom: true, pan: true, modeBarButtons: [["zoom2d", "pan2d"], ["resetViews"], ["toImage"]] }}
                      />
                    </CardContent>
                  </Card>
                </Grid>);
            })
            }
          </Grid>
        </> : <></>
      }
    </Stack>
  );
}