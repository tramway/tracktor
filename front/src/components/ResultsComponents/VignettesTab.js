import React from 'react';
import Plot from 'react-plotly.js';
import Stack from '@mui/material/Stack';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import Divider from '@mui/material/Divider';
//import Chip from '@mui/material/Chip';
import CardHeader from '@mui/material/CardHeader';
import Alert from '@mui/material/Alert';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import axios from "../../api"
import FactorSelect from './FactorSelect';
import { makeDataFromPoint } from './utils';

const plot_config = {
  modeBarButtons: [["toImage"]],
  toImageButtonOptions: {
    format: 'jpeg', // one of png, svg, jpeg, webp
    filename: 'plot',
    height: 700,
    width: 700,
    scale: 1. // Multiply title/legend/axis/canvas sizes by this factor
  },
  displaylogo: false,
  //staticPlot: true,
}

class Vignettes extends React.Component {

  constructor(props) {

    super(props);
    this.state = {
      factors: [],
      selectedFactors: [],
      dataloading: false,
      densities: [],
      x: [],
      y: [],
      selectedPoints: props.selectedPoints,
    };

  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedPoints !== this.props.selectedPoints) {
      console.log("New point detected")
      this.setState({selectedPoints:this.props.selectedPoints})
    }
  }

  fetchData() {

    this.setState({dataloading:true})
    const data = new FormData()
    data.append("job_id",JSON.stringify(this.props.job_id))
    const isDataRequest = this.state.selectedFactors.length > 0
    if (isDataRequest) {
      data.append("factors",JSON.stringify(this.state.selectedFactors))
    }
    axios.post("/api/get_densities",data).then(
      response => {
        const data = response.data
        if (!isDataRequest) {
          this.setState({
            factors: data.factors, 
            dataloading:this.state.selectedFactors.length > 0})
        }
        else{
          this.setState({
            dataloading: false,
            densities:data.densities,
            x:data.x,
            y:data.y})
        }
        
      }
    )
    
  }

  // TODO: finir la sélection des niveaux ici

  // handle the change of the dropdown menu

  componentDidMount() {
    this.fetchData();
  }

  flatten2dArray(a) {
    return (a.concat.apply([], a))
  }

  // render a dropdown menu with the levels
  // <Typography align="center">One point = one trajectory</Typography>
  render() {

    const zmax = Math.max(...Object.entries(this.state.densities).map((entry) => Math.max(...this.flatten2dArray(entry[1]["density"]))))

    return (
      <Stack mt={2} spacing={2} alignItems="center" justifycontent="flex-start">
        <FactorSelect 
          factors={this.state.factors} 
          onChange={(v) => {
            this.setState({selectedFactors:v,dataloading: true},() => this.fetchData())
            }}/>

        {false && !this.state.dataloading &&

          <Plot
            data={this.state.latent_dict[this.state.selectedLevel]}
            layout={{
              width: 500,
              height: 500,
              autosize: false,
              xaxis: { title: "UMAP 1" },
              yaxis: { title: "UMAP 2", scaleanchor: "x" },
              legend: { x: 1, y: 0, yanchor: "top", xanchor: "right" }
            }}
          />
        }
        {this.state.dataloading ? <CircularProgress /> : (this.state.selectedFactors.length > 0) ? 
        <Grid container spacing={2} alignItems="center" justifyContent="center">
          
        {Object.entries(this.state.densities).map((entry) => {
          //console.log(entry)
          const group_name = entry[0]
          const density = entry[1]["density"]
          //const level = entry[1]["level"]
          //const tags = entry[1]["tags"]
          //const f_name = entry[1]["file"]
          const x = this.flatten2dArray(this.state.x)
          const y = this.flatten2dArray(this.state.y)
          const z = this.flatten2dArray(density)

          return (<Grid item xs={12} sm={6} md={4} lg={3} key={group_name}> 
            <Card>
              <CardHeader
                //title={tags.map((elt) => <Chip
                //  key={elt} label={elt}
                ///>)}
                //subheader={f_name}
                title={group_name}
                titleTypographyProps={{ align: "center", variant: "h6", noWrap: true }}
                />
                <CardContent>
                <Stack >
                  <Box class="card-body d-flex align-items-center justify-content-center">
                <Plot
                  class="align-items-center"
                  data={[{
                    x: x,
                    y: y,
                    // if pairs are flipped (compared to what's stored in the sitness_function_dict), we inverse the values of the witness function
                    z: z,
                    type: 'contour',
                    zmin:0.,
                    zmax: zmax,
                    showscale: false,
                    ncontours: 25,
                    hoverinfo: "none",
                    colorscale: "Portland",
                    //reverse: true,
                    colorbar: null,
                    visible: true,
                  }].concat(
                    this.state.selectedPoints.map((point) => makeDataFromPoint(point))
                  )}
                  layout={{
                    height: 300,
                    width: 300,
                    //autosize: true,
                    margin: {
                      b: 0,
                      l: 0,
                      r: 0,
                      t: 0,
                    },
                    xaxis: { 
                      //"title": "UMAP 1", 
                      "range":[Math.min(x), Math.max(x)] },
                    yaxis: { 
                      //"title": "UMAP 2", 
                      "range":[Math.min(y), Math.max(y)] }
                  }}
                  onClick={e => {
                    this.props.addNewPoint(
                      e.points[0].x,
                      e.points[0].y)
                    }
                  }
                  config={plot_config}
                />
                </Box>
                </Stack>
            </CardContent>
            </Card>
          </Grid>)
        }
          
        )}
        </Grid> :
        <Alert severity="warning">
          Please select one or several factors according to which results should be grouped in the visualizations.
        </Alert>
        }
      </Stack>
    );
  }
}

export default Vignettes;