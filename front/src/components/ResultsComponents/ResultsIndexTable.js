import React from "react"
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLongArrowAltDown, faLongArrowAltUp, faTimes, faBars } from '@fortawesome/free-solid-svg-icons'
import { BtnCellRenderer } from '../UploadComponents/IndexTable'
import axios from "../../api";
import { Container } from '@mui/material';

class NoDeleteHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ascSort: 'inactive',
      descSort: 'inactive',
      noSort: 'inactive'
    };
  }

  onMenuClicked() {
    this.props.showColumnMenu(this.menuButton);
  }

  render() {
    let menu = null;

    if (this.props.enableMenu) {
      menu =
        <div ref={(menuButton) => {
          this.menuButton = menuButton;
        }}
          className="customHeaderMenuButton"
          style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
          onClick={this.onMenuClicked.bind(this)}>
          <FontAwesomeIcon icon={faBars} style={{ height: 10 }} />
        </div>;
    }

    let sort = null;
    if (this.props.enableSorting) {
      sort =
        <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
          <div onClick={this.onSortRequested.bind(this, 'asc')} onTouchEnd={this.onSortRequested.bind(this, 'asc')}
            style={{ "margin": 2 }}
            className={`customSortDownLabel ${this.state.ascSort} customHeaderButton`}>
            <FontAwesomeIcon icon={faLongArrowAltDown} />
          </div>
          <div onClick={this.onSortRequested.bind(this, 'desc')} onTouchEnd={this.onSortRequested.bind(this, 'desc')}
            style={{ "margin": 2 }}
            className={`customSortUpLabel ${this.state.descSort} customHeaderButton`}>
            <FontAwesomeIcon icon={faLongArrowAltUp} />
          </div>
          <div onClick={this.onSortRequested.bind(this, '')} onTouchEnd={this.onSortRequested.bind(this, '')}
            style={{ "margin": 2 }}
            className={`customSortRemoveLabel ${this.state.noSort} customHeaderButton`}>
            <FontAwesomeIcon icon={faTimes} />
          </div>
        </div>;
    }

    return (
      <div style={{ display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "flex-end" }}>
        <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
          {menu}
          {sort}
        </div>
        <div style={{ "margin": 2 }}>
          {this.props.displayName}
        </div>
      </div>
    );
  }

  onSortChanged() {
    this.setState({
      ascSort: this.props.column.isSortAscending() ? 'active' : 'inactive',
      descSort: this.props.column.isSortDescending() ? 'active' : 'inactive',
      noSort: !this.props.column.isSortAscending() && !this.props.column.isSortDescending() ? 'active' : 'inactive'
    });
  }

  onSortRequested(order, event) {
    this.props.setSort(order, event.shiftKey);
  }
}


class DisplayFreezedIndexTable extends React.Component {

  FileColumnDef = {
    headerName: 'File',
    field: 'file_name',
    pinned: 'left',
    resizable: true,
    tooltipField: 'file_name',
    cellRenderer: 'btnCellRenderer',
    cellRendererParams: {
      // Le deuxième argument de setState est un callback, qui est exécuté seulement une fois que le setState a effectivement eu lieu
      clicked: (file_id) => {
        if (this.onViewFile) {
          this.onViewFile(file_id)
        }
      },
      table: this,
    },
    minWidth: 250,
    suppressMovable: true,
  }

  PropertyColumnDef = {
    type: 'rightAligned',
    sortable: true,
    editable: false,
    filter: true,
    resizable: true,
    flex: 1,
    minWidth: 150,
    maxWidth: 300,
    headerComponent: NoDeleteHeader,
    headerComponentParams: { enableMenu: true, enableSorting: true, menuIcon: 'fa-bars', table: this },
    cellStyle: { paddingLeft: 0, borderRight: '1px dotted' },
  }

  ForcePropertyColumnDef = {
    type: 'rightAligned',
    editable: false,
    flex: 1,
    maxWidth: 100,
    valueFormatter: (v) => {
      if (v.column.colDef.field === "DT") {
        return (String(v.value) + " s")
      }
      else if (v.column.colId === "scale") {
        return (String(v.value) + " um")
      }
    },
    cellStyle: { paddingLeft: 0, borderRight: '1px dotted' },
  }

  //this.IncludeColumnDef, 
  defaultColumnDefs = [this.FileColumnDef]

  constructor(props) {

    super(props);
    this.onViewFile = props.onViewFile;
    this.state = {
      rowdata: [],
      colnames: this.defaultColumnDefs,
      newcolname: 'new_col_name',
    };
    this.tableRef = React.createRef();
    this.fetchData = this.fetchData.bind(this);
  }

  fetchData() {

    if (this.tableRef.current.api) {
      this.tableRef.current.api.showLoadingOverlay()
    }
    axios.get("/api/get_index_table/" + this.props.job_id)
      .then(response => {
        const data = response.data
        this.updateColdefsFromColNames(data["colnames"])
        this.setState({ rowdata: data["rowdata"] });
        if (this.tableRef.current && this.tableRef.current.api) {
          if (data["rowdata"].length > 0) {
            this.tableRef.current.api.hideOverlay()
          }

        }
      })
  }

  componentDidMount() {
    this.fetchData();
  }

  updateColdefsFromColNames(colnames) {
    let new_col_defs = []
    colnames.forEach(name => {
      if (name !== "DT" && name !== "scale") {
        let new_col_def = { ...this.PropertyColumnDef }
        new_col_def["field"] = name
        new_col_defs.push(new_col_def)
      }
    });

    let DT_col_def = { ...this.ForcePropertyColumnDef }
    DT_col_def["field"] = "DT"
    DT_col_def["headerName"] = "Exposure"
    DT_col_def["headerTooltip"] = "time between two consecutive frames (in seconds)"

    let scale_col_def = { ...this.ForcePropertyColumnDef }
    scale_col_def["field"] = "scale"
    scale_col_def["headerTooltip"] = "sceling factor for localizations (from file units to microns)"

    new_col_defs = [DT_col_def, scale_col_def].concat(new_col_defs)
    this.setState({ colnames: this.defaultColumnDefs.concat(new_col_defs) })
  }

  render() {
    return (
      <Container maxWidth="lg">
        <div id="upload_index_table" className="ag-theme-alpine" style={{ height: 300, width: "100%" }}>
          <AgGridReact
            rowData={this.state.rowdata}
            columnDefs={this.state.colnames}
            rowSelection={'multiple'}
            onCellValueChanged={this.onCellValueChanged}
            suppressRowClickSelection={true}
            components={{ btnCellRenderer: BtnCellRenderer, deleteHeader: NoDeleteHeader }}
            enableBrowserTooltips={true}
            ref={this.tableRef}
            overlayNoRowsTemplate={'<span class="ag-overlay-loading-center">No trajectory files to show</span>'}
            overlayLoadingTemplate={'<span class="ag-overlay-loading-center">Update of the index table ...</span>'}
          >
          </AgGridReact>
        </div>


      </Container>

    );
  }
}



export default DisplayFreezedIndexTable;