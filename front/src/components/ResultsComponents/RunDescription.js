import React from 'react'
import Typography from '@mui/material/Typography'

const RunDescription = (props) => {
    return (
        <Typography align="center">
            {props.info.submit ? ("Submitted on " + props.info.submit) : "Not submitted"}
            <br />
            {props.info.started ? ("Started on " + props.info.started) : "Not yet started"}
            <br />
            {props.info.end_time ? ("Ended on " + props.info.end_time) : "Not yet finished"}
        </Typography>
    )
}

export default RunDescription