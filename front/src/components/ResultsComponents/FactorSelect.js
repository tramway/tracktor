import * as React from 'react';
import { useRef, useEffect } from "react";
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import CancelIcon from "@mui/icons-material/Cancel";
import Chip from '@mui/material/Chip';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(factor, factors, theme) {
  return {
    fontWeight:
      factors.indexOf(factor) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

export default function FactorSelect({factors, onChange, label = "Group by..."}) {
  const theme = useTheme();
  const [selectedFactors, setSelectedFactors] = React.useState([]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setSelectedFactorsFromValue(event.target.value)
    
  };

  const setSelectedFactorsFromValue = (value) => {
    let unsorted = typeof value === 'string' ? value.split(',') : value
    let sorted = unsorted.sort((a,b) => (a === 'file') ? (b === 'file' ? 0 : 1) : (b === 'file' ? -1 : 0))
    setSelectedFactors(
      // On autofill we get a stringified value.
      // We ensure that "file is the last element"
      sorted
    )
  }

  useEffect(() => {
    onChange(selectedFactors)
  }, [selectedFactors]);

  useEffect(() => {
    if (!(selectedFactors in factors)) {
      if (factors.length > 0) {
        setSelectedFactorsFromValue(factors[0])
      }
    }
  }, [factors]);

  /*
  deleteIcon={
                        <CancelIcon
                          onMouseDown={(event) => event.stopPropagation()}
                        />
                      }
                    onDelete={
                    (e) => {
                        e.preventDefault()
                        console.log("Clicked delete")
                        setSelectedFactors(selectedFactors.filter(factor => factor !== value))
                    }
                }
  */

  return (
    <div>
      <FormControl sx={{ m: 1, width: 350 }}>
        <InputLabel id="demo-multiple-chip-label">{label}</InputLabel>
        <Select
          labelId="demo-multiple-chip-label"
          id="demo-multiple-chip"
          value={selectedFactors}
          onChange={handleChange}
          input={<OutlinedInput id="select-multiple-chip" label={label} />}
          renderValue={(selected) => {
            return(
            <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
              {(selected.length > 0) && selected.map((value) => (
                <Chip 
                    key={value} 
                    label={value} 
                />
              ))}
            </Box>
          )}}
          MenuProps={MenuProps}
        >
          {factors.map((factors_set) => (
            <MenuItem
              key={factors_set}
              value={factors_set}
              style={getStyles(factors_set, factors, theme)}
            >
              {factors_set.join(" / ")}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}