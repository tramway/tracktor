import React from 'react';
import Plot from 'react-plotly.js';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import Alert from '@mui/material/Alert';
import CardContent from '@mui/material/CardContent';
import Container from '@mui/material/Container';
import CircularProgress from '@mui/material/CircularProgress';
import axios from "../../api"
import FactorSelect from './FactorSelect';
import { make_data_object, describe_factors } from './utils';

const asc = arr => arr.sort((a, b) => a - b);

const quantile = (arr, q) => {
  const sorted = asc(arr);
  const pos = (sorted.length - 1) * q;
  const base = Math.floor(pos);
  const rest = pos - base;
  if (sorted[base + 1] !== undefined) {
      return sorted[base] + rest * (sorted[base + 1] - sorted[base]);
  } else {
      return sorted[base];
  }
};

/*function capitalize_first_letter(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}*/

const plot_config = {
  modeBarButtons: [["toImage"]],
  toImageButtonOptions: {
    format: 'jpeg', // one of png, svg, jpeg, webp
    filename: 'plot',
    height: 700,
    width: 1000,
    scale: 1. // Multiply title/legend/axis/canvas sizes by this factor
  },
  displaylogo: false,
}



class MetricsPanel extends React.Component {

  constructor(props) {

    super(props);
    this.state = {
      factors: [],
      dataloading: true,
      factor_dict: {},
      selectedFactors: [],
      points: props.selectedPoints,
      proportions: {},
      total_counts: [],
      detailed_counts: []
    };

  }

  updateState(dict, callback=null) {
    const state = {
      factors: this.state.factors,
      dataloading: this.state.dataloading,
      factor_dict: this.state.factor_dict,
      selectedFactors: this.state.selectedFactors,
      points: this.state.points,
      proportions: this.state.proportions,
      total_counts: this.state.total_counts,
      detailed_counts: this.state.detailed_counts
    }
    for (let [key, value] of Object.entries(dict)) {
      if (value !== null) {
        state[key] = value;
      }
    }
    if (callback === null) {
      this.setState(state)
    } else {
      this.setState(state, callback)
    }
  }

  fetchData() {
    if (this.props.job_id) {
        this.updateState({dataloading:true},
          () => {
            const data = new FormData()
            const isDataRequest = this.state.selectedFactors.length > 0// The first request only queries the factors, the second one queries the data
            if (isDataRequest) {
              data.append("factors", JSON.stringify(this.state.selectedFactors))
            }
            axios.post("/api/job_results/" + this.props.job_id + "/trad",data)
              .then(response => {
                if (isDataRequest) {
                  this.updateState({
                    factor_dict: response.data["factor_dict"],
                    total_counts: response.data["total_counts"],
                    detailed_counts: response.data["detailed_counts"],
                    dataloading: false
                  });
                }
                else {
                  this.updateState({
                    factors: response.data["factors"],
                    dataloading: this.state.selectedFactors.length > 0
                  })
                }
              })
            if ((this.state.points.length > 0) & isDataRequest) {
              const data_points = new FormData()
              data_points.append("points",JSON.stringify(this.state.points))
              data_points.append("factors", JSON.stringify(this.state.selectedFactors))
              axios.post("/api/factors_points_data/" + this.props.job_id, data_points).then(response => {
                console.log(response.data.region_proportion)
                this.updateState({
                  proportions: response.data.region_proportion,
                })
              })
            }
          })

    }
  }

  componentDidMount() {
    this.fetchData()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedPoints !== this.props.selectedPoints) {
      console.log("New point detected")
      this.updateState({points:this.props.selectedPoints}, () => {this.fetchData()})
    }
  }

  // render a dropdown menu with the levels
  render() {
    return (
      <Stack mt={2} spacing={2} alignItems="center" justifycontent="flex-start">
        <FactorSelect
          factors={this.state.factors}
          onChange={(v) => {
            this.updateState({selectedFactors:v, dataloading: true},() => this.fetchData())
            }}/>
        {this.state.dataloading ? <CircularProgress /> :
        (this.state.selectedFactors.length > 0) ?
          <Grid container spacing={2} alignItems="center" justifyContent="center">
            <Grid item md={12} lg={6}>
              <Card >
                <CardContent>
                  <Container class="card-body d-flex align-items-center justify-content-center">
                <Plot
                  layout={{
                    updatemenus:[{
                      buttons: [
                        {
                            args: [{'visible': this.state.total_counts.map((e) => true).concat(this.state.total_counts.map((e) => false)) }],
                            label: 'Raw',
                            method: 'update'
                        },
                        {
                            args: [{'visible': this.state.total_counts.map((e) => false).concat(this.state.total_counts.map((e) => true)) }],
                            label:'Per 100 frames',
                            method:'update'
                        }
                      ],
                      direction: 'left',
                      pad: {'r': 10, 't': 10},
                      showactive: true,
                      type: 'buttons',
                      x: 0.1,
                      xanchor: 'left',
                      y: 1.1,
                      yanchor: 'top'
                    }],
                    title: "Number of trajectories per group<br />(total : " +
                      this.state.total_counts.reduce((partialSum, a) => partialSum + parseInt(a.y), 0) + " trajectories)",
                    yaxis: {
                      title: "Total per group",
                      rangemode: "tozero",
                      //range: [0., 1.2 * Math.max(this.state.total_counts["y"])],
                      //side: 'right',
                      //showgrid: false,
                      showticklabels: true,
                      fixedrange: true,
                    },
                    xaxis: {title:describe_factors(this.state.selectedFactors)},
                    barmode: "stack",
                    showlegend: false,
                    //width: 900,
                    //height: 600
                  }}
                  config={plot_config}
                  data={this.state.total_counts.map((elt) => {
                        return {
                          x: elt.x, //.map((s) => s.replace(/(.{12})/g,"$&" + "<br />")), // adds line breaks every n characters
                          y: elt.y,
                          text: elt.y.map((elt_, i) => {
                            let start = elt_ + " trajectories"
                            if (elt.n_files) {
                              start = start + "<br />" + elt.n_files[i] + " file" + (elt.n_files > 1 ? "s" : "")
                            }
                            return start;
                          }),
                          width: .5,
                          type: "bar",
                          textposition: "auto",
                          name: elt.x[0],
                          hoverinfo: "x",
                        }
                      }).concat(
                      this.state.total_counts.map((elt) => {
                        return {
                          x: elt.x,
                          y: elt.y_per_hundred_frames,
                          text: elt.y_per_hundred_frames.map((elt_, i) => {
                            let start = elt_.toFixed(2) + " trajs / 100 frames"
                            if (elt.n_files) {
                              start = start + "<br />" + elt.n_files[i] + " file" + (elt.n_files > 1 ? "s" : "")
                            }
                            return start;
                          }),
                          width: .5,
                          type: "bar",
                          textposition: "auto",
                          name: elt.x[0],
                          hoverinfo: "x",
                          visible: false,
                        }
                      }))
                  }
                />
                </Container>
                </CardContent>
              </Card>
            </Grid>
            <Grid item md={12} lg={6}>
              <Card>
              <CardContent>
                  <Container class="card-body d-flex align-items-center justify-content-center">
                <Plot
                  layout={{
                    title: "Number of trajectories per file",
                    showlegend: false,
                    yaxis: {
                      title: "# trajectories per file",
                      rangemode: "tozero",
                      //range: [0., 1.2 * Math.max(this.state.total_counts["y"])],
                      //side: 'right',
                      //showgrid: false,
                      showticklabels: true,
                      fixedrange: true,
                    },
                    xaxis: {title:describe_factors(this.state.selectedFactors)},
                    updatemenus:[{
                      buttons: [
                        {
                            args: [{'visible': this.state.detailed_counts.map((e) => true).concat(this.state.detailed_counts.map((e) => false)) }],
                            label: 'Raw',
                            method: 'update'
                        },
                        {
                            args: [{'visible': this.state.detailed_counts.map((e) => false).concat(this.state.detailed_counts.map((e) => true)) }],
                            label:'Per 100 frames',
                            method:'update'
                        }
                      ],
                      direction: 'left',
                      pad: {'r': 10, 't': 10},
                      showactive: true,
                      type: 'buttons',
                      x: 0.1,
                      xanchor: 'left',
                      y: 1.1,
                      yanchor: 'top'
                    }],
                    //width: 900,
                    //height: 600
                  }}
                  config={plot_config}
                  data={this.state.detailed_counts.map((elt) => {
                    return {
                      type: "violin",
                      spanmode: "hard",
                      x: elt.x,
                      y: elt.y,
                      name: elt.x[0],
                      text: elt.text,
                      //points: "suspectedoutliers",
                      points: "all",
                      jitter: .5,
                      box: { visible: true },
                      hoveron: "points",
                      hoverinfo: "text",
                    }
                  }).concat(this.state.detailed_counts.map((elt) => {
                    return {
                      type: "violin",
                      spanmode: "hard",
                      x: elt.x,
                      y: elt.y_per_hundred_frames,
                      name: elt.x[0],
                      text: elt.text,
                      //points: "suspectedoutliers",
                      points: "all",
                      jitter: .5,
                      box: { visible: true },
                      hoveron: "points",
                      hoverinfo: "text",
                      visible: false,
                    }
                  }))
                  }
                />
                </Container>
                </CardContent>
              </Card>
            </Grid>
            <Grid item md={12} lg={6}>
              <Card>
              <CardContent>
                  <Container class="card-body d-flex align-items-center justify-content-center">
                <Plot className="result_plot"
                  data={make_data_object(this.state.factor_dict,"n_points")}
                  config={plot_config}
                  layout={{
                    showlegend: false,
                    title: 'Length of trajectories',
                    yaxis: {
                      type: 'log',
                      autorange: false,
                      range: [Math.log10(7),Math.log10(2*quantile(this.state.factor_dict["values"]["n_points"].map((values) => quantile(values,.95)),.95))],
                      title: 'Trajectory length'
                    },
                    xaxis: {title:describe_factors(this.state.selectedFactors)},
                    //width: 900,
                    //height: 600
                  }} />
                  </Container>
                </CardContent>
              </Card>
            </Grid>

            <Grid item md={12} lg={6}>
              <Card>
                <CardContent>
                  <Container class="card-body d-flex align-items-center justify-content-center">
                  <Plot className="result_plot"
                    config={plot_config}
                    data={make_data_object(this.state.factor_dict,"log_D")}
                    layout={{
                      showlegend: false,
                      title: 'Log-diffusion',
                      yaxis: { title: "log_10 (D) (um^2/s)" },
                      xaxis: {title:describe_factors(this.state.selectedFactors)},
                      //width: 900,
                      //height: 600
                      }} />
                      </Container>
                </CardContent>
              </Card>
            </Grid>
            <Grid item md={12} lg={6}>
              <Card>
                <CardContent>
                  <Container class="card-body d-flex align-items-center justify-content-center">
                <Plot className="result_plot"
                  data={make_data_object(this.state.factor_dict,"alpha")}
                  config={plot_config}
                  layout={{
                    showlegend: false,
                    title: 'Anomalous diffusion exponent',
                    yaxis: { title: "Alpha", range: [0., 2.] },
                    xaxis: {title:describe_factors(this.state.selectedFactors)},
                    //width: 900,
                    //height: 600
                    }} />
                    </Container>
                    </CardContent>
              </Card>
            </Grid>
            <Grid item md={12} lg={6}>
              <Card>
              <CardContent>
                  <Container class="card-body d-flex align-items-center justify-content-center">
                <Plot className="result_plot"
                  data={this.state.factor_dict["values"]["best_model"].map((elt,i) => {
                    return {
                      x: elt,
                      name: this.state.factor_dict["label"][i],
                      type: "histogram",
                      histnorm: "percent",
                      hoverinfo: "name",
                    }
                  })}
                  config={plot_config}
                  layout={{
                    showlegend: false,
                    title: 'Predicted random walk model', yaxis: {
                      title: "Percentage of trajectories"
                    },
                    //width: 900,
                    //height: 600
                  }} />
                  </Container>
                    </CardContent>
              </Card>
            </Grid>
            <Grid item md={12} lg={6}>
            <Card>
              <CardContent>
                  <Container class="card-body d-flex align-items-center justify-content-center">
                {(this.state.points.length > 0) ? (Object.keys(this.state.proportions).length > 0) ? <Plot className="result_plot"
                  data={Object.keys(this.state.proportions).map((point_label, i) => {
                    let elt = this.state.proportions[point_label]
                    return {
                      x: elt.group,
                      y: elt.proportion,
                      name: point_label,
                      hoverinfo:"name",
                      type: "bar",
                      marker: {
                        color: this.state.points[i].color,
                      }
                    }
                  })}
                  config={plot_config}
                  layout={{
                    autosize: true,
                    showlegend: false,
                    title: 'Fraction of trajectories in selected regions of the latent space',
                    xaxis: {title:describe_factors(this.state.selectedFactors)},
                    yaxis: {
                      title: "Percentage of trajectories"
                    },
                    //width: 900,
                    //height: 600
                  }} /> : <CircularProgress /> :
                  <Alert severity="warning">
                    Go to the Comparison or Vignette tabs,
                    and click on the locations of the latent space whose details you want to display.
                  </Alert>
                  }
                  </Container>
                    </CardContent>
              </Card>
            </Grid>
          </Grid> :
          <Alert severity="warning">
            Please select one or several factors according to which results should be grouped in the visualizations.
          </Alert>
        }
      </Stack>
    );
  }
}

export default MetricsPanel
