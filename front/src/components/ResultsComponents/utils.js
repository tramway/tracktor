export function makeDataFromPoint(point) {
    return {
        "x": [point.x],
        "y": [point.y],
        "text": [point.label],
        "textposition": "top center",
        "textfont": { "size": 16 },
        "type": "scatter",
        "mode": "markers+text",
        "hoverinfo": "none",
        "marker": {
            "size":12,
            "symbol":"circle-open",
            "color":point.color,
            "line":{
                "width":3,
                "color":point.color
            }
        },
        "showlegend": false,
    }
} 

export function getColorCode() {
    var makeColorCode = '0123456789ABCDEF';
    var code = '#';
    for (var count = 0; count < 6; count++) {
      code = code + makeColorCode[Math.floor(Math.random() * 16)];
    }
    return code;
  }

function make_violin(elt) {

    let dict = {
      x: elt.values.map((elt_) => { return elt.label }),
      y: elt.values,
      name: elt.label,
      type: "violin",
      spanmode: "hard",
      box: {
        visible: true,
        hoveron: "none",
        fillcolor: elt.color,
      },
      marker: {
        color: elt.color,
      },
      hoverinfo: "none",
    }
    return dict
}

export function make_data_object(factor_dict,column, colors) {
    const x = factor_dict.label
    const y = factor_dict.values[column]
    return x.map((group, group_index) => 
      ({
        label:group,
        values:y[group_index],
        color:colors ? colors[group_index]: undefined,
      })
    ).map(make_violin)
}

export function describe_factors(factors) {
    return factors.join(" - ")
}