import React from 'react';
import Plot from 'react-plotly.js';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import Alert from '@mui/material/Alert';
import axios from "../../api"
import CircularProgress from '@mui/material/CircularProgress';
import FactorSelect from './FactorSelect';
import Switch from '@mui/material/Switch';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';

function shift2DArray(array,shift) {
  for (let i = 0; i < array.length; i++) {
    for (let j = 0; j < array[i].length; j++) {
      array[i,j]  = array[i,j] + shift
    }
  }
  return array
}

function flatten2dArray(a) {
  return (a.concat.apply([], a))
}

class DisplayMDSFiles extends React.Component {


  constructor(props) {

    super(props);
    this.state = {
      mds_dicts: [],
      densities: [],
      dataloading: true,
      factors: [],
      x: undefined, // for latent vignettes
      y: undefined, // for latent vignettes
      selectedFactors: [],
      colorOptions: [],
      selectedColors: undefined,
      showDensities: false,
      
    };

  }

  fetchData() {

    this.setState({dataloading:true}, () => {
      const isDataRequest = this.state.selectedFactors.length > 0
      const data = new FormData()
      if (isDataRequest) {
        data.append("factors",JSON.stringify(this.state.selectedFactors))
      }
      axios.post("/api/job_results/" + this.props.job_id + "/mds_conditions",data)
        .then(response => {
          if (isDataRequest) {
            this.setState({
              mds_dicts: response.data.mds_dicts,
              densities: response.data.densities,
              colorOptions: response.data.color_options,
              x: response.data.x,
              y: response.data.y,
              dataloading: false
            })
          }
          else {
            this.setState({
              factors: response.data.factors,
              dataloading: this.state.selectedFactors.length > 0
            });
          }
        })
    })
    
  }



  // handle the change of the dropdown menu
  componentDidMount() {
    this.fetchData();
  }



  // render a dropdown menu with the levels
  render() {

    let mds_data = undefined
    let zmax = 0.
    if (!this.state.dataloading && (this.state.selectedColors >= 0)) {
      zmax = Math.max(this.state.densities.map((group) => Math.max(...flatten2dArray(group["density"]))))
      mds_data = Object.keys(this.state.mds_dicts[this.state.selectedColors]).map((group) => {
        const dict_lengths = Object.values(this.state.mds_dicts[this.state.selectedColors]).map((v) => v.length)
        const showlegend = (Math.max(...dict_lengths) > 1) || (this.state.colorOptions[this.state.selectedColors].filter((v,i) => this.state.selectedFactors[i] != v).length === 0)
        console.log(this.state.colorOptions[this.state.selectedColors], this.state.selectedFactors)
        const table = this.state.mds_dicts[this.state.selectedColors][group]
        return({
          "type":"scatter",
          "mode":"markers",
          "text": table.map((record) => record.name),
          "name": group,
          "visible": !this.state.showDensities,
          "hoverinfo": "text",
          "x": table.map((record) => record.X_1),
          "y": table.map((record) => record.X_2),
          "showlegend": showlegend,
        })
      })

      if (this.state.showDensities) {
        mds_data = this.state.densities.map((group) => {
          return (
            {
              "type":"heatmap",
              "x":flatten2dArray(this.state.x).map(a => a + group.x_offset),//shift2DArray(this.state.x,group.x_offset),
              "y":flatten2dArray(this.state.y).map(a => a + group.y_offset),//shift2DArray(this.state.y,group.y_offset),
              "z":flatten2dArray(group.density),
              "showlegend":false,
              "showscale":false,
              "colorscale":"Portland",
              "zmin":0,
              "visible":this.state.showDensities,
              "hoverinfo":"none",
              "zmax":zmax,
            }
          )
        }).concat(mds_data)
      } 
    }

    return (
      <Stack mt={2} spacing={2} alignItems="center" justifyContent="flex-start">
        <Stack direction="row" alignItems="center">
        <FactorSelect 
          factors={this.state.factors}
          label="One point per..."
          onChange={(v) => {
            this.setState({
              dataloading: true,
              selectedFactors: v
            }, () => this.fetchData())
          }}
        />
        {(this.state.mds_dicts.length > 0) && 
          <FactorSelect
            factors={this.state.colorOptions}
            label="Color by..."
            onChange={(v) => {
              this.setState({selectedColors:this.state.colorOptions.indexOf(v)})
            }}
          />
        }
        <FormGroup>
          <FormControlLabel control={<Switch  onChange={
            (v) => {
              this.setState({showDensities:v.target.checked})
            }
          }/>} label="Show vignettes" />
        </FormGroup>
        </Stack>
        {this.state.dataloading && <CircularProgress />}
        {!this.state.dataloading && (this.state.selectedColors >= 0) &&
          <Plot 
            data={
              mds_data
            }
            layout={{ 
               height: 500,
               //legend: { x: 1, y: 0, yanchor: "top", xanchor: "right" },
               yaxis:{
                zeroline:false,
                dtick:0.04,
                linewidth:1,
                mirror:"all",
               },
               xaxis:{
                scaleanchor:"y",
                zeroline: false,
                dtick:0.04,
                mirror:"all",
                linewidth:1,
              } }} 
            config={{
              modeBarButtons: [["toImage"]],
              toImageButtonOptions: {
                format: 'jpeg', // one of png, svg, jpeg, webp
                filename: 'plot',
                height: 700,
                width: 700,
                scale: 1. // Multiply title/legend/axis/canvas sizes by this factor
              },
              displaylogo: false,
            }}
            />
        }
        {!this.state.dataloading && this.state.only_one_file &&
          <Alert severity="warning">
            This feature is only available when at least three files are analyzed simulatneously.
          </Alert>
        }
      </Stack>


    );
  }
}
export default DisplayMDSFiles;