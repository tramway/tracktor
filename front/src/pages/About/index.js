import React, { useEffect, useState } from 'react'
import { useIntl } from 'react-intl'
import Page from 'material-ui-shell/lib/containers/Page'
import Scrollbar from 'material-ui-shell/lib/components/Scrollbar'
import Container from '@mui/material/Container'
import Stack from '@mui/material/Stack'
import ReactMarkdown from 'react-markdown'
import remarkSlug from 'remark-slug'
import remarkGfm from 'remark-gfm'
import remarkToc from 'remark-toc'

const About = () => {
  const [source, setSource] = useState(null)
  const intl = useIntl()

  const loadData = async () => {
    const data = await fetch(
      //'https://raw.githubusercontent.com/TarikHuber/react-most-wanted/master/README.md'
      require('./howto.md')
    )
    const text = await data.text()
    setSource(text)
  }

  useEffect(() => {
    loadData()
  }, [])

  return (
    <Page
      pageTitle={intl.formatMessage({ id: 'about', defaultMessage: 'About' })}
    >
      <Scrollbar>
        <Stack mt={2} alignItems="center">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/psGGzaNJMI8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen>
        </iframe>
        </Stack>
        <div style={{ padding: 12 }}>
          {source && (
            <ReactMarkdown className="markdown-body" children={source} remarkPlugins={[[remarkSlug],[remarkGfm],[remarkToc,{tight:true}]]}/>
          )}
        </div>
      </Scrollbar>
    </Page>
  )
}
export default About
