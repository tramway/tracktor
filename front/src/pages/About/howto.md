# Table of contents

# Overview

Tracktor provides means to analyze the dynamics of particles set in motion by random fluctuations originating from the environment in which they evolve and modulated by their interactions with it.

## Main Steps

- **Step 1:** Traktor computes optimal descriptive statistics of your trajectories.

- **Step 2:** Tracktor does statistical tests between so-obtained sets of features vectors. It highlights differences in the underlying generative processes and guides the interpretation with visualizations.

## Why Tracktor ?

In heterogeneous environments such as cells, biomolecules are likely to be subject to a mixture of interactions, varying both spatially and temporally. Hence, their dynamics can hardly be matched to canonical random walk models (which usually account for one physical phenomena at a time, so as to preserve some analytical tractability). The method on which Tracktor relies benefits from previous works on the theory of random walks, but does not require assumptions that observed trajectories are realizations of one or another canonical random walk model.

Tracktor's originality is indeed to rely on a _hypothesis-free_ characterization of random walks. For each trajectory, it computes a vector of features describing the motion. This vector notably relates to the scale of the trajectory (its diffusion coefficient), but it does account as well for more subtle differences such as the sub- or super-difusive character of the trajectory, as well as characteristics of the underlying model. Such quantities are known to be difficult to estimate _in general_, without prior knowledge of the generative model. Here, the features are computed by a neural network trained to infer relevant physical quantities from simulated random walks of a variety of models.

In a second time, Tracktor performs pairwise comparisons between sets of trajectories. Sets can correspond to files uploaded by the user or to groups of files, according to specified information (e.g. the condition in which trajectories were obtained, an ID for the biological replicate, a region of the cell...).
Tracktor assesses the statistical significance of the difference observed between each pair of sets of trajectories and indicates the trajectories to which this difference is due.

# Files upload & run configuration

## General requirements

- Tracktor handles 2D and 3D trajectories from a variety of file formats.
- Files should not exceed 50 Mb and should contain more than 10 trajectories each.
- Trajectories within a same uploaded file will always be treated together: upload them in separate files if you want them to be in distinct groups.
- Only trajectories with more than 7 localizations are considered, and only the first 80 localizations of each trajectory are processed, so as to match the characteristics of trajectories used to train the neural network.
- The time interval between two frames should not be lower than 3 ms nor larger than 2.5 s.
- Tracktor tries to detect the format of the files you upload and reads them accordingly.

## Files in CSV-like formats

Accepted separators : comma, semicolumn, tab, space, vertical bar.
One row per localization, with at least :
- coordinates (1 column per dimension)
- time, or frame number
- trajectory identifier

Preferred column names are `x`, `y`, `z` (if your trajectories are 3-dimensional), `t`, `frame`, `n` (trajectory ID).
If your columns are named differently, Tracktor will try to infer what each column contains.
You will be able to check that this has been done correctly once the upload is completed, by visualizing trajectories. See below the beginning of a valid file.

```
x,y,n,t,frame
1.23,0.45,1,0.000,0
1.27,0.46,1,0.050,1
0.89,2.23,2,0.000,0
0.87,2.27,2,0.050,1
0.81,2.24,2,0.100,2
```

Tracktor uses `pandas.read_csv()` to read CSV files : if it fails, and if you know Python, you can try to find the origin of the issue by opening your files using the same function.

## Others file formats

### Matlab

Tracktor handles Matlab files (one row per localization). It tries to identify columns as it does for CSV-like files. If a file contains several matrices, Tracktor will try each of them until one is valid.

### Trackmate

Tracktor also handles Trackmate's output files.

## Files or folders ?

Click on the "Upload files" button to upload the selection of files of your choice. You can also drag and drop files in the dedicated area.

If your files are organized in folders, corresponding, for instance, to different biological replicates or to various conditions, Tracktor can associate attributes to each file, based on the folder hierarchy. To use this feature, click on the "Upload folder" button and choose the top-level folder. You will be prompted to enter the ending of the trajectory files, so as not to upload irrelevant data which might be contained in the same folders (images, un-tracked localizations, microscope settings...).

## Specifying information to group files

To add information about each file's condition, add columns and enter values in the table shown on the "Upload" page (the _index_ table). If you upload your files by folders, Tracktor will try to infer relevant column values from the folder hierarchy and add them to the index table. In the following, we refer to columns of the index table as "factors".

| ![index table](index-table.png) |
|:---:|
| An example index table |

**Do not forget to check the pixel size ("scale", in micrometers) and the exposure (in seconds) for each file**. Then, run the analysis by clicking on the "run analysis" button.

Prior to starting the computation, Tracktor will ask you to specify the hierarchy of the factors that you specified. Thusn Tracktor will know which comparisons are relevant to make.
The most coarse factors have hierarchy one, and the level increases with the granularity. Groups will be defined by subsets of factors of increasing hierarchy, and comparisons between groups will be performed if only the factor with the highest level of hierarchy differs between the two groups. If needed, two factors can have an equivalent level of hierarchy.

For instance, if your data is organized by day of acquisition / treatment given to cells / field of view, and you want to assess the effect of the treatment, you might give the following hierarchy:
- day of acquisition : 2
- treatment of cells : 1
- field of view : 3

In this case, groups will look like (Treatment A), (Treatment B), (Treatment C), (Treatment A, day 1), (Treatment A, day 2), ..., (Treatment C, day 5, cell 4).

| ![hierarchy](hierarchy.png) |
|:-----:|
| A dialog asks to input the desired hierarchy |


# Analysis results

To access analysis results, click on "See results", in the side menu, and then on the row corresponding to the desired analysis. Several tabs correspond to various aspects of each analysis. Here, we explore the results of an example analysis and see what each tab shows. The tables on which the visualizaions presented here rely can be downloaded by clicking on the "Download" button in the top-right corner.

## Summary

Here, you see the index table containing the information about the files included in the analysis. You can visualise trajectories by clicking on the binocular icons and color them according to your preferred criteria.

| ![summary tab](summary.png) |
|:-----:|
| The summary tab |

| ![summary visualizations](summary-plots.png) |
|:-----:|
| Visualisations of trajectories from a few files, with trajectories coloured according to their log-diffusivity |

| ![summary visualizations colouring according to latent space](summary-points.png) |
|:-----:|
| Here, trajectories are coloured according to the latent point to which they are the closest (among the set of selected points, see below) |

## Metrics

In this panel, random walk metrics are displayed: the log-diffusivity, the anomalous diffusion exponent (estimated by the network), and the most plausible random walk model associated to each trajectory by the network. Results can be grouped according to the factors entered in the index table.

| ![standard metrics](metrics-levels.png) |
|:-----:|
| Display metrics according to your desired level of granularity |

The random walk models used during the amortized inference phase are the following ones :
- OU: Ornstein-Uhlenbeck process (confined motion)
- CTRW: Continuous time random walk (subdiffusive), with heavy-tailed waiting time distribution
- LW: Levy Walk (superdiffusive)
- fBM: fractional Brownian Motion, both sub- and superdiffusive depending on alpha.
- sBM: scaled Brownian Motion, decelerating (subdiffusive) or accelerating (superdiffusive) motion, depending on alpha.

The last graph shows, for each group, the fraction of trajectories located close to each selected point of the latent space. To select points of the latent space, click on one of its visualizations, in the "Comparisons" or "Vignettes" tab. Click again on a selected point to remove it from the list.

| ![metrics corresponding to latent space](metrics-points.png) |
|:-----:|
| For each group and each latent space region, the bottom plot shows the fraction of trajectories found in the vicinity of selected points |

## Comparisons

In this panel you first see on the left a matrix showing the p-values of all comparisons done by Tracktor. P-values displayed in the matrix are obtained using a maximum mean discrepancy (MMD) test (see reference 2.). For each comparison, trajectories are sampled in a way that equalizes the length histograms, so as not to introduce biases due to different length distributions.

| ![comparison matrix](comparison-matrix.png) |
|:-----:|
| Each cell corresponds to a comparison and is coloured according to the _p_-value |

The null hypothesis of the test done to compare two groups A and B is: "Trajectories from group A and group B have the same latent space distribution, _i.e._ are generated by the same process". Hence, a low P-value means that the underlying generative processes differ to some extent. To understand where this eventual difference stems from, you might want to compare the groups latent densities, or see which regions of the latent space are the most discriminative between the two groups.

If you click on one comparison's cell, you will see on the right of the screen plots showing
- the latent space occupation density of each of the two compared groups,
- the witness function, which is the difference of these densities,
- the criticality heatmaps, which shows how discriminative each location in the latent space is.

| ![comparison matrix and density](comparison-density.png) |
|:-----:|
| The witness function (i.e. difference of densities) for streptavidin trajectories observed with and without septins (see the clicked cell on the left) |

If you click on a position in the latent space, this will select the point and you will be able to use it in other visualizations. For instance, it can be interesting to select points located in local maxima of criticality, as in the example below.

| ![comparison matrix and criticality, with selected points](comparison-points.png) |
|:-----:|
| The criticality map, with selected points at its maxima |

## Vignettes

Here, you see side by side the latent space densities of each group. Use the box at the top to select factors according to which  vignettes should be grouped.

| ![vignettes](vignettes.png) |
|:-----:|
| Vignettes showing densities for various conditions |

## Points

Here, you first see metrics of the trajectories located close to each selected point of the latent space. Below, a set of trajectories located in these regions are displayes, to convey a visual intuition of their specificity.

| ![selected points metrics](points-metrics.png) |
|:-----:|
| Metrics describing trajectories located in the vicinity of each selected point |

| ![selected points example trajectories](points-examples.png) |
|:-----:|
| Example of trajectories found in the vicinity of each selected point |

## Maps

Here, you see two-dimensional maps where each point corresponds to a group (defined according to the factors of your choice). On these maps, groups whose latent distributions of trajectories are very different are far from each other. Conversely, those whose latent distributions are similar are close. The maps are obtained using a modified version of the MDS algorithm (see reference 2).
Toggle the "See vignettes" button to display the latent densities of each group.

| ![maps](maps-files.png) |
|:-----:|
| Here, each dot represents a file. |

| ![files map with densities](maps-densities.png) |
|:-----:|
| Toggle the "Show vignettes" switch to visualize the latent densities corresponding to each point. We see well that files with similar densities are located close to each other. |

# Miscellaneous

## Appearance

We recommend using Google Chrome, Safari or Mozilla Firefox to use Tracktor. If you have the feeling that components are too wide, try zooming out a little (on Mac : cmd + "-").

## Note on confidentiality

Once uploaded, your data **cannot be accessed by other users**. It is stored at the Institut Pasteur and erased after two days of inactivity. Users are identified by a cookie stored on their computer.

# Authors

Tracktor was developed at the [Decision & Bayesian Computation lab](https://research.pasteur.fr/en/team/decision-and-bayesian-computation/) :
- Hippolyte Verdier ([Pasteur](https://research.pasteur.fr/en/member/hippolyte-verdier/), [Twitter](https://twitter.com/Hippover))
- Tidiane Camaret N'Dir ([Pasteur](https://research.pasteur.fr/en/member/tidiane-camaret-ndir/))

Maintenance is taken over by François Laurent.

**Contact** : flaurent\[at\]pasteur.fr

# References

1. **About the statistical test:** Verdier, H., Laurent, F., Cassé, A., Vestergaard, C. L., Specht, C. G., & Masson, J. B. (2023). Simulation-based inference for non-parametric statistical comparison of biomolecule dynamics. PLOS Computational Biology, 19(2), e1010088. <https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1010088>

2. **About the graph neural network architecture:** Verdier, H., Duval, M., Laurent, F., Cassé, A., Vestergaard, C.L. and Masson, J.B., 2021. Learning physical properties of anomalous random walks using graph neural networks. Journal of Physics A: Mathematical and Theoretical, 54(23), p.234001. <https://iopscience.iop.org/article/10.1088/1751-8121/abfa45/meta>

3. **About the performance of the graph neural network:** Verdier, H., Laurent, F., Cassé, A., Vestergaard, C. L., & Masson, J. B. (2022). Variational inference of fractional Brownian motion with linear computational complexity. Physical Review E, 106(5), 055311. <https://link.aps.org/pdf/10.1103/PhysRevE.106.055311>
