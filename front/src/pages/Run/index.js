import { AppBar, Tab, Tabs, Container, Typography, Alert, AlertTitle, Button, IconButton, Tooltip, CircularProgress, Stack } from '@mui/material'
import Page from 'material-ui-shell/lib/containers/Page'
import React, { lazy, useState, useEffect, useRef, useCallback } from 'react'
import { useParams } from 'react-router-dom'
import { useIntl } from 'react-intl'
import DownloadIcon from '@mui/icons-material/Download';
import TerminalIcon from '@mui/icons-material/Terminal';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import axios from "../../api"
import { getColorCode } from '../../components/ResultsComponents/utils'
import RunDescription from '../../components/ResultsComponents/RunDescription'


let Vignettes = lazy(() => import('../../components/ResultsComponents/VignettesTab'))
let DisplayMDS = lazy(() => import('../../components/ResultsComponents/MDSTab'))
let MetricsPanel = lazy(() => import('../../components/ResultsComponents/MetricsTab'))
let RunSummary = lazy(() => import('../../components/ResultsComponents/SummaryTab'))
let DisplayMMDByGroup = lazy(() => import('../../components/ResultsComponents/MMDGroupTab'))
let PointsOfInterest = lazy(() => import('../../components/ResultsComponents/LatentProperties'))

function LogViewer({ job_id }) {
    const [log, setLog] = useState('');
    const logRef = useRef(null);

    useEffect(() => {
        const intervalId = setInterval(() => {
            axios.get("/api/get_log/" + job_id)
                .then((data) => {
                    setLog(data.data.log)
                }
                );
        }, 5000);

        return () => clearInterval(intervalId);
    }, []);

    /*useEffect(() => {
      logRef.current.scrollTop = logRef.current.scrollHeight;
    }, [log]);*/

    return (
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
            >
                <Typography>Log</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <pre ref={logRef} style={{ overflowY: "scroll", whiteSpace: "pre-wrap", height: "15em" }} >{log.length > 0 ? log : "No log yet..."}</pre>
            </AccordionDetails>
        </Accordion>

    );
}

const RunResults = (props) => {
    const { job_id } = useParams();

    const [tab, setTab] = useState('summary')
    const intl = useIntl()

    const [availableTabs, setAvailableTabs] = useState({})
    const [jobStatus, setJobStatus] = useState("") // Coarse grain
    const [isLoading, setIsLoading] = useState(true)
    const [jobInfo, setJobInfo] = useState("") // Percentage of progress, or info about the error
    const [job, setJob] = useState({})
    const [getJobTries, setGetJobTries] = useState(0)

    const [selectedPoints, setSelectedPoints] = useState([])

    const getJob = () => {
        axios.get("/api/job_info/" + job_id).then((result) => { setGetJobTries(getJobTries + 1); setJob(result.data) })
    }

    if (getJobTries === 0) {
        getJob()
    }

    const addNewPoint = useCallback((point_x, point_y) => {
        const existing_copies = selectedPoints.filter((point) => ((point.x == point_x) && (point.y == point_y)))
        if (existing_copies.length > 0) {
            existing_copies.forEach(point => {
                deletePoint(point.index)
            });
        }
        else {
            console.log("No copies")
            console.log(selectedPoints)
            let new_index = (selectedPoints.length > 0) ? Math.max(...selectedPoints.map(point => point.index)) + 1 : 1
            console.log("New index", new_index)
            let point = {
                x: point_x,
                y: point_y,
                label: "#" + new_index,
                longlabel: "Point #" + new_index,
                index: new_index,
                color: getColorCode(),
            }
            console.log(point)
            let newPoints = [...selectedPoints].concat(point)
            setSelectedPoints(newPoints)
        }
    }, [selectedPoints.length])

    const deletePoint = useCallback((point_index) => {
        setSelectedPoints([...selectedPoints].filter((value) => (value.index != point_index)))
    }, [selectedPoints.length])

    useEffect(() => {
        let interval = null;
        if (job && (job.status === 2)) {
            // Not doing anything
        }
        else {
            interval = setInterval(() => getJob(), 20 * (10 ** 3));
        }
        return () => clearInterval(interval)
    }, [getJob])

    useEffect(() => {
        if ((job.status === 2) && (jobStatus !== "done")) {
            setJobStatus("done")
            //clearInterval(getLog)
        }
        else if ((job.status === 1) && (jobStatus !== "running")) {
            setJobStatus("running")
        }
        else if ((job.status === 0) && (jobStatus !== "queued")) {
            setJobStatus("queued")
        }
        else if ((job.status >= 3) && (jobStatus !== "failed")) {
            setJobStatus("failed")
            //clearInterval(getLog)
        }
    }, [job])

    useEffect(() => {
        if (jobStatus === "done") {
            setAvailableTabs({
                "mmd": <DisplayMMDByGroup job_id={job_id} selectedPoints={selectedPoints} addNewPoint={addNewPoint} deletePoint={deletePoint} />,
                "summary": <RunSummary job_id={job_id} selectedPoints={selectedPoints} is3D={job.is3D} />,
                "mds": <DisplayMDS job_id={job_id} />,
                "metrics": <MetricsPanel job_id={job_id} selectedPoints={selectedPoints} addNewPoint={addNewPoint} deletePoint={deletePoint} />,
                "poi": <PointsOfInterest job_id={job_id} points={selectedPoints} is3D={job.is3D} />,
                "latent": <Vignettes job_id={job_id} selectedPoints={selectedPoints} addNewPoint={addNewPoint} deletePoint={deletePoint} />
            })
        }
    }, [jobStatus, job_id, selectedPoints])

    useEffect(() => {
        if ((jobStatus === "done") && (availableTabs.length === 0)) {
            setIsLoading(true)
        }
        else {
            if (jobStatus.length > 0) {
                setIsLoading(false)
            }
            else {
                setIsLoading(true)
            }
        }

    }, [availableTabs, jobStatus])

    useEffect(() => {
        if (!isLoading) {
            setJobStatus("")
            setIsLoading(true)
            setJobInfo("")
            setGetJobTries(0)
            setAvailableTabs({})
        }
    }, [job_id])

    const downloadArchive = (job) => {
        axios.get("/api/job_results/" + job + "/zip", { responseType: 'blob' })
            .then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data], { "type": "application/zip" }));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', 'run_' + job + '_results.zip'); //or any other extension
                document.body.appendChild(link);
                link.click();
            });
    }

    return (
        <Page
            pageTitle={intl.formatMessage({
                id: 'run',
                defaultMessage: 'Results of ',
            }) + '"' + (job.name ? job.name : ("Job " + job_id)) + '"'}
            tabs={(jobStatus === "done") ?
                <AppBar position="static">
                    <Tabs
                        value={tab}
                        onChange={(e, t) => setTab(t)}
                        aria-label="results panels"
                        centered
                        textColor="inherit"
                        indicatorColor="secondary"
                    >
                        <Tab label="Summary" value="summary" disabled={jobStatus !== "done"} />
                        <Tab label="Metrics" value="metrics" disabled={jobStatus !== "done"} />
                        <Tab label="Comparisons" value="mmd" disabled={jobStatus !== "done"} />
                        <Tab label="Vignettes" value="latent" disabled={jobStatus !== "done"} />
                        <Tab label="Points" value="poi" disabled={jobStatus !== "done"} />
                        <Tab label="Maps" value="mds" disabled={jobStatus !== "done"} />
                    </Tabs>
                </AppBar> : null
            }
            appBarContent={
                <>
                    <Tooltip title="Download analysis results">
                        <span>
                            <Button
                                disabled={(isLoading || (jobStatus !== "done"))}
                                size="large"
                                variant="outlined"
                                color="inherit"
                                endIcon={<DownloadIcon />}
                                onClick={() => {
                                    downloadArchive(job_id)
                                }}
                            >
                                Download
                            </Button>
                        </span>
                    </Tooltip>
                </>
            }
        >
            <Container maxWidth="large">
                <>
                    {isLoading &&
                        <Stack mt={2} spacing={2} justifycontent="center" alignItems="center">
                            <Typography variant="h4" align="center">Content loading...</Typography>
                            <CircularProgress />
                        </Stack>}
                    {(!isLoading && (jobStatus === "done")) && availableTabs[tab]}
                    {(!isLoading && (jobStatus === "queued")) &&
                        <Stack mt={2} spacing={2} justifycontent="center" alignItems="center">
                            <Typography variant="h4" align="center">Analysis pending...</Typography>
                            <RunDescription info={job} />
                            <CircularProgress />
                        </Stack>
                    }
                    {(!isLoading && (jobStatus === "running")) &&
                        <Stack mt={2} spacing={2} justifycontent="center" alignItems="center">
                            <Typography variant="h4" align="center">Analysis running...</Typography>
                            <Typography align="center">Analyses typically last for ~10 minutes, up to a few hours when dozens of sets are compared.</Typography>
                            <RunDescription info={job} />
                            <CircularProgress />
                            <LogViewer job_id={job_id} />
                        </Stack>
                    }
                    {(!isLoading && (jobStatus === "failed")) &&
                        <Stack mt={2} spacing={2} justifycontent="center" alignItems="center">
                            <Alert severity="error">
                                <AlertTitle>Tracktor encountered an error when running the analysis...</AlertTitle>
                                {jobInfo}
                            </Alert>
                            <RunDescription info={job} />
                            <LogViewer job_id={job_id} />
                        </Stack>
                    }
                </>
            </Container>

        </Page>
    )
}
export default RunResults
