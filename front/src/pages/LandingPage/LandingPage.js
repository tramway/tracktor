import React from 'react'
import { Link } from 'react-router-dom'
//import { ReactComponent as Logo } from "../../logo.svg";
//import SvgIcon from '@mui/material/SvgIcon';

const LandingPage = () => {
  /*
  <SvgIcon>
   <Logo />
  </SvgIcon>
  */
  return (
    <div
      style={{
        position: 'absolute',
        left: 0,
        top: 0,
        bottom: 0,
        right: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >

      <h2>Welcome to Tracktor, a trajectory analysis tool for SPT experiments</h2>
      <div>Please report bugs at flaurent [at] pasteur.fr</div>
      <div>A bug tracker is also available at <a href="https://gitlab.pasteur.fr/tramway/tracktor/-/issues">gitlab.pasteur.fr/tramway/tracktor</a></div>
      <div>
        <Link to="/upload">Get started (2D)</Link>
      </div>
      <div>
        <Link to="/upload3d">Get started (3D)</Link>
      </div>
      <div>
        <Link to="/about">User guide</Link>
      </div>
    </div>
  )
}
export default LandingPage
