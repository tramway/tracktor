import React, { useState, useRef, useEffect } from 'react'
import DisplayIndexTable from '../../components/UploadComponents/IndexTable';
import DisplayLocs from '../../components/ResultsComponents/TrajectoriesPlot';
import UploadBox from '../../components/UploadComponents/UploadBox';
import Page from 'material-ui-shell/lib/containers/Page';
import { useIntl } from 'react-intl';
import { Divider, Grid } from '@mui/material';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import { SnackbarProvider, useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom'



const UploadTab = ({ is3D }) => {

    const intl = useIntl()
    const navigate = useNavigate();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const [selected_files, setSelectedFiles] = useState([]);
    const indexTable = useRef();
    const filesRef = useRef();
    filesRef.current = selected_files
    const [elRefs, setElRefs] = useState([]);
    const elRefsRefs = useRef();
    elRefsRefs.current = elRefs

    //console.log("Loaded with is3D = ", is3D)

    useEffect(() => {
        let newRefs = selected_files.map((elt, i) => (i < elRefsRefs.current.length ? elRefsRefs.current[i] : React.createRef()))
        setElRefs(newRefs)
    }, [selected_files])

    useEffect(() => {
        closeSeenFiles()
    }, [is3D])


    const addFileToSee = (file_id, files) => {
        //console.log("Add file " + file_id + ", currently " + files.length + " files")
        const newList = [...files, file_id]
        setSelectedFiles(newList)

    }

    const closeSeenFiles = () => {
        setSelectedFiles([])
    }

    const updateTable = () => {
        if (indexTable.current) {
            indexTable.current.fetchData()
        }
    }

    const handleSnackbarClick = (key) => {
        console.log("Clicked on key : " + key)
        console.log(key)
        navigate("/run/" + key);
        // close the snackbar after navigating 
        closeSnackbar(key)
    }

    const updateViewedLocs = (file_name, files) => {
        if (files.includes(file_name)) {
            let i = files.indexOf(file_name)
            elRefsRefs.current[i].current.fetchData()
        }
    }

    return (
        <SnackbarProvider>
            <Page pageTitle={intl.formatMessage({
                id: is3D ? 'upload3d' : 'upload',
                defaultMessage: is3D ? 'Upload files (3D)' : 'Upload files (2D)'
            })}>
                <Stack spacing={2} alignItems="stretch" justifyContent="flex-start" mt={2} ml={2}>
                    <Typography variant='h2' align="center">
                        Upload files {is3D ? "(3D)" : "(2D)"}
                    </Typography>
                    <Typography align="center">
                        1. Upload files containing trajectories in {is3D ? "3D" : "2D"}. See accepted file formats <Link to="/about">here</Link>. <br />
                        2. We advise you to visualize your uploaded trajectories by clicking on the binocular icons to check that they were properly read. <br />
                        3. Add columns specifying the conditions in which they were observed, files will be grouped accordingly in subsqquent comparisons.
                    </Typography>
                    <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2} alignItems="center" justifyContent="center" mt={2} ml={2}>
                        <UploadBox is3D={is3D} onUploadEnds={updateTable} flexItem />
                        <Divider flexItem orientation='vertical' />
                        <DisplayIndexTable flexItem
                            ref={indexTable}
                            is3D={is3D}
                            onViewFile={(value) => {
                                addFileToSee(value, filesRef.current)
                            }}
                            onDeleteFiles={() => {
                                closeSeenFiles()
                            }}
                            onJobSubmitted={(job_data) => {
                                let job_id = job_data.job_ID
                                if (job_data.task_ID) {
                                    enqueueSnackbar('Run ' + job_id + ' was successfully submitted !',
                                        {
                                            key: job_id,
                                            action: (key) => (
                                                <button onClick={() => handleSnackbarClick(key)}>See analysis...</button>
                                            ),
                                            variant: 'info',
                                            anchorOrigin: {
                                                vertical: 'top',
                                                horizontal: 'center'
                                            }
                                        })
                                }
                                else {
                                    enqueueSnackbar('Run ' + job_id + ' could not be submitted, sorry... Try again later', {
                                        variant: 'error',
                                        anchorOrigin: {
                                            vertical: 'top',
                                            horizontal: 'center'
                                        }
                                    })
                                }

                            }}
                            onFileValueChanged={(file_name) => {
                                updateViewedLocs(file_name, filesRef.current)
                            }} />
                    </Stack>

                    <Divider flexItem orientation="horizontal" />
                    {(selected_files.length > 0) ?
                        <Grid container spacing={2} mb={2}>
                            {selected_files.map((f_id, i) => (
                                <Grid item key={f_id}>
                                    <DisplayLocs
                                        file_id={f_id}
                                        ref={elRefs[i]}
                                        handleClose={() => {
                                            let newList = [...filesRef.current].filter((v) => (v !== f_id))
                                            setSelectedFiles(newList)
                                        }}
                                        selectedIndicator="t"
                                        is3D={is3D}
                                        job_id={0} />
                                </Grid>
                            ))}
                        </Grid> : <Typography align="center">Click on the binocular icons to visualise trajectories of the corresponding files</Typography>
                    }


                </Stack>
            </Page>
        </SnackbarProvider>
    )
}

export default UploadTab