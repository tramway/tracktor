import React from 'react'
import {
  Insights,
  QueryStats,
  Upload,
  InfoOutlined,
  Language as LanguageIcon,
} from '@mui/icons-material'
import PendingIcon from '@mui/icons-material/Pending';
import HourglassBottomIcon from '@mui/icons-material/HourglassBottom';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import StarIcon from '@mui/icons-material/Star';

import allLocales from './locales'

const getMenuItems = (props) => {
  const {
    intl,
    updateLocale,
    locale,
    menuContext,
    themeContext,
    a2HSContext,
    jobs,
    auth: authData,
  } = props

  // const { toggleThis, isDesktop, isAuthMenuOpen, isMiniSwitchVisibility } =
  //  menuContext
  // const { themeID, setThemeID, isRTL, toggleThisTheme } = themeContext
  //
  // const { auth, setAuth } = authData
  // const { isAppInstallable, isAppInstalled, deferredPrompt } = a2HSContext

  const localeItems = allLocales.map((l) => {
    return {
      value: undefined,
      visible: true,
      primaryText: intl.formatMessage({ id: l.locale }),
      onClick: () => {
        updateLocale(l.locale)
      },
      leftIcon: <LanguageIcon />,
    }
  })

  const my_jobs = jobs.filter((job) => (job.public === false))
  const demo_jobs = jobs.filter((job) => (job.public === true))

  //const isAuthorised = auth.isAuthenticated

  const isAuthorised = true
  let myJobsItem = {
    primaryText: intl.formatMessage({ id: 'my_results', defaultMessage: 'See results' }),
    leftIcon: <Insights />,
  }
  if (my_jobs.length > 0) {
    myJobsItem["primaryTogglesNestedList"] = true
    myJobsItem["nestedItems"] = my_jobs.map((job) => {
      let icon = <QueryStats />
      if (job.status === 1) {
        icon = <HourglassBottomIcon />
      }
      else if (job.status === 0) {
        icon = <PendingIcon />
      }
      else if (job.status >= 3) {
        icon = <ErrorOutlineIcon />
      }
      return {
        value: '/run/' + job.id,
        visible: true,
        primaryText: job.name,
        leftIcon: icon,
      }
    })
  }
  let demoJobsItem = {
    primaryText: intl.formatMessage({ id: 'demo_results', defaultMessage: 'Demo results' }),
    leftIcon: <StarIcon />,
  }
  if (demo_jobs.length > 0) {
    demoJobsItem["primaryTogglesNestedList"] = true
    demoJobsItem["nestedItems"] = demo_jobs.map((job) => {
      let icon = <QueryStats />
      if (job.status === 1) {
        icon = <HourglassBottomIcon />
      }
      else if (job.status === 0) {
        icon = <PendingIcon />
      }
      else if (job.status >= 3) {
        icon = <ErrorOutlineIcon />
      }
      return {
        value: '/run/' + job.id,
        visible: true,
        primaryText: job.name,
        leftIcon: icon,
      }
    })
  }

  return [
    {
      value: '/upload',
      primaryText: intl.formatMessage({
        id: 'upload',
        defaultMessage: 'Upload files (2D)'
      }),
      leftIcon: <Upload />,
    },
    {
      value: '/upload3d',
      primaryText: intl.formatMessage({
        id: 'upload3d',
        defaultMessage: 'Upload files (3D)'
      }),
      leftIcon: <Upload />,
    },
    myJobsItem,
    demoJobsItem,
    {
      value: '/about',
      visible: true,
      primaryText: intl.formatMessage({ id: 'about' }),
      leftIcon: <InfoOutlined />,
    },
  ]
}
export default getMenuItems

/*
{ divider: true },
    {
      primaryText: intl.formatMessage({ id: 'language' }),
      secondaryText: intl.formatMessage({ id: locale }),
      primaryTogglesNestedList: true,
      leftIcon: <LanguageIcon />,
      nestedItems: localeItems,
    }
*/
