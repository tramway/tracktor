import pytest
from back.app.app import app


@pytest.fixture
def client():
    client = app.test_client()
    yield client
